package com.mellowelephant.saucer.thread;

import android.util.Log;

import com.mellowelephant.saucer.data.SQLData;
import com.mellowelephant.saucer.dto.BeerDTO;
import com.mellowelephant.saucer.dto.SaucerLocationDTO;
import com.mellowelephant.saucer.service.BeerNetworkQueueService;
import com.mellowelephant.saucer.util.AppState;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class RemovePendingThread implements Runnable
{
	private BeerNetworkQueueService bnqs;
	private Long beerId;
	
	public RemovePendingThread(Long beerId, BeerNetworkQueueService bnqs)
	{
		this.beerId = beerId;
		this.bnqs = bnqs;
	}
	
	@Override
	public void run()
	{
		Log.d(AppState.logTag, "RemovePendingThread running");
		
		SQLData sqlData = new SQLData(bnqs);
		
		//Write to the queue
		if(!sqlData.removeBeerFromPendingQueue(beerId))	//Try to remove this beer from the pending queue (in case we added as pending but haven't sent it to the server yet. Just prevent it from being sent)
			sqlData.addBeerToRemoveQueue(beerId);		//If that fails, it must have already gone to the server. We need to send the removal to the server
		
		//Update all the cached locations
		Map<SaucerLocationDTO, List<BeerDTO>> caches = sqlData.getValidCachedBeerLists();
		Set<SaucerLocationDTO> cacheLocations = caches.keySet();
		for(SaucerLocationDTO cacheLocation : cacheLocations)
		{
			List<BeerDTO> cache = caches.get(cacheLocation);
			
			boolean foundBeer = false;
			for(BeerDTO cacheBeer : cache)
			{
				if(cacheBeer.getId().longValue() == beerId.longValue())
				{
					cacheBeer.setPendingLocation(null);
					cacheBeer.setDateConsumed(null);
					cacheBeer.setRating(null);
					cacheBeer.setComments(null);
					
					foundBeer = true;
					break;
				}
			}
			
			if(foundBeer)
				sqlData.updateBeerListInCache(cacheLocation, cache);
		}
	}
}
