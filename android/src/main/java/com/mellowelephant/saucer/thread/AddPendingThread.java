package com.mellowelephant.saucer.thread;

import android.util.Log;

import com.mellowelephant.saucer.data.SQLData;
import com.mellowelephant.saucer.dto.BeerDTO;
import com.mellowelephant.saucer.dto.SaucerLocationDTO;
import com.mellowelephant.saucer.service.BeerNetworkQueueService;
import com.mellowelephant.saucer.util.AppState;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class AddPendingThread implements Runnable
{
	private BeerNetworkQueueService bnqs;
	private BeerDTO beer;
	private SaucerLocationDTO location;
	
	public AddPendingThread(BeerDTO beer, SaucerLocationDTO location, BeerNetworkQueueService bnqs)
	{
		this.beer = beer;
		this.bnqs = bnqs;
		this.location = location;
	}
	
	@Override
	public void run()
	{
		Log.d(AppState.logTag, "AddPendingThread running for BeerID " + beer.getId());
		
		SQLData sqlData = new SQLData(bnqs);
		
		//Write to the queue
		sqlData.addBeerToPendingQueue(beer, location);
		
		//Update all the cached locations
		Map<SaucerLocationDTO, List<BeerDTO>> caches = sqlData.getValidCachedBeerLists();
		Set<SaucerLocationDTO> cacheLocations = caches.keySet();
		for(SaucerLocationDTO cacheLocation : cacheLocations)
		{
			List<BeerDTO> cache = caches.get(cacheLocation);
			
			boolean foundBeer = false;
			for(BeerDTO cacheBeer : cache)
			{
				if(cacheBeer.getId().longValue() == beer.getId().longValue())
				{
					cacheBeer.setPendingLocation(beer.getPendingLocation());
					cacheBeer.setDateConsumed(beer.getDateConsumed());
					cacheBeer.setRating(beer.getRating());
					cacheBeer.setComments(beer.getComments());
					
					foundBeer = true;
					break;
				}
			}
			
			if(!foundBeer)
				cache.add(beer);
			
			sqlData.updateBeerListInCache(cacheLocation, cache);
		}
	}
}
