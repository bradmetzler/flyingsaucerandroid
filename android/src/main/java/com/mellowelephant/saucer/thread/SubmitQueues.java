package com.mellowelephant.saucer.thread;

import android.util.Log;

import com.mellowelephant.saucer.BackendAPI;
import com.mellowelephant.saucer.data.SQLData;
import com.mellowelephant.saucer.dto.BeerRatingDTO;
import com.mellowelephant.saucer.dto.SaucerLocationDTO;
import com.mellowelephant.saucer.util.AppState;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class SubmitQueues
{
	private BackendAPI api;
	private SQLData sqlData;
	private Long memberId;
	private String deviceId;
	
	public SubmitQueues(BackendAPI api, SQLData sqlData, Long memberId, String deviceId)
	{
		this.api = api;
		this.sqlData = sqlData;
		this.memberId = memberId;
		this.deviceId = deviceId;
	}
	
	public boolean submit()
	{
		boolean failedSends = false;
		
		//Submit and clear pending queue
		Map<SaucerLocationDTO, List<BeerRatingDTO>> pendingQueue = sqlData.getPendingQueue();
		if(pendingQueue == null || pendingQueue.isEmpty())
			Log.d(AppState.logTag, "BNQS: No pending beers to send.");
		else
		{
			Set<SaucerLocationDTO> pendingLocations = pendingQueue.keySet();
			for(SaucerLocationDTO pendingLocation : pendingLocations)
			{
				List<BeerRatingDTO> pendingBeers = pendingQueue.get(pendingLocation);
				
				try
				{
					Log.d(AppState.logTag, "BNQS: Sending pending beer queue (" + pendingBeers.size() + ") for " + pendingLocation.displayName + "...");
					api.addPendingBeers(memberId, deviceId, pendingBeers, pendingLocation);
					Log.d(AppState.logTag, "BNQS: Clearing pending beer queue for " + pendingLocation.displayName + "...");
					sqlData.clearPendingQueueForLocation(pendingLocation);
				}
				catch(Exception e)
				{
					Log.d(AppState.logTag, "BNQS: Failed to send pending beers for " + pendingLocation.displayName + "!");
					failedSends = true;
				}
			}
		}
		
		//Submit remove queue
		List<BeerRatingDTO> removeQueue = sqlData.getRemoveQueue();
		if(removeQueue == null || removeQueue.isEmpty())
			Log.d(AppState.logTag, "BNQS: No remove pendings to send.");
		else
		{
			for(BeerRatingDTO beerToRemove : removeQueue)
			{
				try
				{
					Log.d(AppState.logTag, "BNQS: Sending remove pending queue (" + removeQueue.size() + ")...");
					api.removePendingBeers(memberId, deviceId, beerToRemove.getBeerId());
					sqlData.removeFromRemoveQueue(beerToRemove);
				}
				catch(Exception e)
				{
					Log.d(AppState.logTag, "BNQS: Failed to send the remove pending queue!");
					failedSends = true;
				}
			}
		}
		
		//Submit comment/rating queue
		List<BeerRatingDTO> commentRatingQueue = sqlData.getCommentRatingQueue();
		if(commentRatingQueue == null || commentRatingQueue.isEmpty())
			Log.d(AppState.logTag, "BNQS: No comment/ratings to send.");
		else
		{
			//Make batches of 20
			List<List<BeerRatingDTO>> batches = new ArrayList<List<BeerRatingDTO>>();
			for(BeerRatingDTO commentRating : commentRatingQueue)
			{
				if(!batches.isEmpty() && batches.get(batches.size() - 1).size() < 20)
					batches.get(batches.size() - 1).add(commentRating);
				else
					batches.add(new ArrayList<BeerRatingDTO>(Arrays.asList(commentRating)));
			}
			
			//Send the batches
			for(List<BeerRatingDTO> batch : batches)
			{
				try
				{
					Log.d(AppState.logTag, "BNQS Sending comments/ratings queue (" + batch.size() + ")...");
					api.rateAndCommentBeers(memberId, deviceId, batch);
					sqlData.removeFromCommentRatingQueue(batch);
				}
				catch(Exception e)
				{
					Log.d(AppState.logTag, "BNQS failed to send the comment/rating queue!");
					failedSends = true;
				}
			}
		}
		
		return failedSends;
	}
}
