package com.mellowelephant.saucer.thread;

import android.util.Log;

import com.mellowelephant.saucer.BackendAPI;
import com.mellowelephant.saucer.data.AppData;
import com.mellowelephant.saucer.data.SQLData;
import com.mellowelephant.saucer.service.BeerNetworkQueueService;
import com.mellowelephant.saucer.util.AppState;

public class SubmitQueuesThread implements Runnable
{
	private BeerNetworkQueueService bnqs;
	
	public SubmitQueuesThread(BeerNetworkQueueService bnqs)
	{
		this.bnqs = bnqs;
	}
	
	@Override
	public void run()
	{
		Log.d(AppState.logTag, "SubmitQueuesThread running");
		BackendAPI api = new BackendAPI();
		SQLData sqlData = new SQLData(bnqs);
		
		if(new SubmitQueues(api, sqlData, AppData.getLoggedInUser(bnqs).getId(), AppData.getDeviceId(bnqs)).submit())	//Try to send anything that's in the queue.
			bnqs.retryTimer();
		else
			bnqs.stopSelf();
	}
}
