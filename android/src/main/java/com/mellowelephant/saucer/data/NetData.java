package com.mellowelephant.saucer.data;

import android.content.Context;
import android.os.AsyncTask.Status;
import android.util.Log;

import com.mellowelephant.saucer.async.InviteBuddyTask;
import com.mellowelephant.saucer.async.RegisterForNotificationsTask;
import com.mellowelephant.saucer.async.RemoveBuddyTask;
import com.mellowelephant.saucer.async.RetrieveBeerBuddyBeerListTask;
import com.mellowelephant.saucer.async.RetrieveBeerBuddyListTask;
import com.mellowelephant.saucer.async.RetrieveBeerListTask;
import com.mellowelephant.saucer.async.RetrieveFoodMenuTask;
import com.mellowelephant.saucer.async.SignInTask;
import com.mellowelephant.saucer.async.UnRegisterForNotificationsTask;
import com.mellowelephant.saucer.callback.BeerBuddyActionListener;
import com.mellowelephant.saucer.callback.BeerBuddyListListener;
import com.mellowelephant.saucer.callback.BeerDetailsListener;
import com.mellowelephant.saucer.callback.BeerListListener;
import com.mellowelephant.saucer.callback.FoodMenuListener;
import com.mellowelephant.saucer.callback.SignInListener;
import com.mellowelephant.saucer.dto.SaucerLocationDTO;
import com.mellowelephant.saucer.dto.UserDTO;
import com.mellowelephant.saucer.location.LocationAware;
import com.mellowelephant.saucer.util.AppState;

public class NetData implements LocationAware
{
	private RetrieveBeerListTask retrieveBeerListTask = null;
	private RetrieveBeerBuddyListTask retrieveBeerBuddyListTask = null;
	private RetrieveBeerBuddyBeerListTask retrieveBeerBuddyBeerListTask = null;
	private InviteBuddyTask inviteBuddyTask = null;
	private RemoveBuddyTask removeBuddyTask = null;
	private RetrieveFoodMenuTask retrieveFoodMenuTask = null;
	private SignInTask signInTask = null;
	
	public NetData()
	{
		AppState.registerAsLocationAware("NetData", this);
	}
	
	public void stopLoginSensitiveTasks()
	{
		if(retrieveBeerListTask != null && retrieveBeerListTask.getStatus() == Status.RUNNING)
			retrieveBeerListTask.cancel(true);
	}
	
	public void registerForNotifications(boolean isEnabled)
	{
		AppData.setNotificationsBusy(true);	//Notifications checkbox will be disabled when this flag is set
		
		if(isEnabled)
			new RegisterForNotificationsTask().execute();
		else
			new UnRegisterForNotificationsTask().execute();
	}
	
	public void retrieveBeerList(Context context, SaucerLocationDTO location, UserDTO user, BeerListListener callback)
	{
		if(retrieveBeerListTask == null || retrieveBeerListTask.getStatus() != Status.RUNNING)
		{
			Log.d(AppState.logTag, "Starting a new RetrieveBeerListTask.");
			retrieveBeerListTask = new RetrieveBeerListTask(context, location, user);	//Task hasn't been initialized or a previous one is already done. Start up a new one
			retrieveBeerListTask.execute(callback);
		}
		else
		{
			Log.d(AppState.logTag, "Found a running RetrieveBeerListTask. Changing the callback.");
			retrieveBeerListTask.callback = callback;	//Task was previously started on a different page and is currently running. Don't start over, just change the callback
		}
	}
	
	public void retrieveBeerBuddyList(Context context, UserDTO user, BeerBuddyListListener callback)
	{
		if(retrieveBeerBuddyListTask == null || retrieveBeerBuddyListTask.getStatus() != Status.RUNNING)
		{
			Log.d(AppState.logTag, "Starting a new RetrieveBeerBuddyListTask.");
			retrieveBeerBuddyListTask = new RetrieveBeerBuddyListTask(context, user);	//Task hasn't been initialized or a previous one is already done. Start up a new one
			retrieveBeerBuddyListTask.execute(callback);
		}
		else
		{
			Log.d(AppState.logTag, "Found a running RetrieveBeerBuddyListTask. Changing the callback.");
			retrieveBeerBuddyListTask.callback = callback;	//Task was previously started on a different page and is currently running. Don't start over, just change the callback
		}
	}
	
	public void retrieveBeerBuddyBeerList(Context context, SaucerLocationDTO location, UserDTO user, BeerListListener callback)
	{
		if(retrieveBeerBuddyBeerListTask == null || retrieveBeerBuddyBeerListTask.getStatus() != Status.RUNNING)
		{
			Log.d(AppState.logTag, "Starting a new RetrieveBeerBuddyBeerListTask.");
			retrieveBeerBuddyBeerListTask = new RetrieveBeerBuddyBeerListTask(context, location, user);	//Task hasn't been initialized or a previous one is already done. Start up a new one
			retrieveBeerBuddyBeerListTask.execute(callback);
		}
		else
		{
			Log.d(AppState.logTag, "Found a running RetrieveBeerBuddyBeerListTask. Changing the callback.");
			retrieveBeerBuddyBeerListTask.callback = callback;	//Task was previously started on a different page and is currently running. Don't start over, just change the callback
		}
	}
	
	public void inviteBuddy(String cardNumber, String dob, BeerBuddyActionListener callback)
	{
		if(inviteBuddyTask == null || inviteBuddyTask.getStatus() != Status.RUNNING)
		{
			Log.d(AppState.logTag, "Starting a new InviteBuddyTask.");
			inviteBuddyTask = new InviteBuddyTask(AppState.user, cardNumber, dob);	//Task hasn't been initialized or a previous one is already done. Start up a new one
			inviteBuddyTask.execute(callback);
		}
		else
		{
			Log.d(AppState.logTag, "Found a running InviteBuddyTask. Changing the callback.");
			inviteBuddyTask.callback = callback;	//Task was previously started on a different page and is currently running. Don't start over, just change the callback
		}
	}
	
	public void removeBuddy(Long removeMemberId, BeerBuddyActionListener callback)
	{
		if(removeBuddyTask == null || removeBuddyTask.getStatus() != Status.RUNNING)
		{
			Log.d(AppState.logTag, "Starting a new RemoveBuddyTask.");
			removeBuddyTask = new RemoveBuddyTask(AppState.user, removeMemberId);	//Task hasn't been initialized or a previous one is already done. Start up a new one
			removeBuddyTask.execute(callback);
		}
		else
		{
			Log.d(AppState.logTag, "Found a running RemoveBuddyTask. Changing the callback.");
			removeBuddyTask.callback = callback;	//Task was previously started on a different page and is currently running. Don't start over, just change the callback
		}
	}
	
	public void retrieveFoodMenu(FoodMenuListener callback)
	{
		if(retrieveFoodMenuTask == null || retrieveFoodMenuTask.getStatus() != Status.RUNNING)
		{
			Log.d(AppState.logTag, "Starting a new RetrieveFoodMenuTask.");
			retrieveFoodMenuTask = new RetrieveFoodMenuTask(AppState.currentLocation);	//Task hasn't been initialized or a previous one is already done. Start up a new one
			retrieveFoodMenuTask.execute(callback);
		}
		else
		{
			Log.d(AppState.logTag, "Found a running RetrieveFoodMenuTask. Changing the callback.");
			retrieveFoodMenuTask.callback = callback;	//Task was previously started on a different page and is currently running. Don't start over, just change the callback
		}
	}
	
	public void loginUser(String ufoNumber, String password, SaucerLocationDTO location, boolean isMou, SignInListener callback)
	{
		if(signInTask != null && signInTask.getStatus() != Status.FINISHED)
		{
			Log.d(AppState.logTag, "Canceling a running SignInTask!");
			signInTask.cancel(true);
		}
		
		Log.d(AppState.logTag, "Starting a new SignInTask.");
		signInTask = new SignInTask(ufoNumber, password, location, isMou);	//Task hasn't been initialized or a previous one is already done. Start up a new one
		signInTask.execute(callback);
	}
	
	@Override
	public void onNewLocation(SaucerLocationDTO location, boolean wasSetAutomatically)
	{
		//Cancel any currently-running location-dependent asyncs and start new ones
		
		if(retrieveBeerListTask != null && retrieveBeerListTask.getLocation() != location && retrieveBeerListTask.getStatus() == Status.RUNNING)
		{
			Log.d(AppState.logTag, "Aborting in-progress RetrieveBeerListTask because of a location change!");
			BeerListListener callback = retrieveBeerListTask.callback;
			
			retrieveBeerListTask.cancel(true);
			retrieveBeerListTask = null;
			
			if(location != null)
				retrieveBeerList(AppState.context, location, AppState.user, callback);
		}
		
		if(retrieveFoodMenuTask != null && retrieveFoodMenuTask.getLocation() != location && retrieveFoodMenuTask.getStatus() == Status.RUNNING)
		{
			Log.d(AppState.logTag, "Aborting in-progress RetrieveFoodMenuTask because of a location change!");
			FoodMenuListener callback = retrieveFoodMenuTask.callback;
			
			retrieveBeerListTask.cancel(true);
			retrieveBeerListTask = null;
			
			if(location != null)
				retrieveFoodMenu(callback);
		}
	}
}
