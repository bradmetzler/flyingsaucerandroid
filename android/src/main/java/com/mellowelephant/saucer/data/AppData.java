package com.mellowelephant.saucer.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.provider.Settings;
import android.util.Log;

import com.mellowelephant.saucer.MEG;
import com.mellowelephant.saucer.callback.BeerBuddyActionListener;
import com.mellowelephant.saucer.callback.BeerBuddyListListener;
import com.mellowelephant.saucer.callback.BeerListListener;
import com.mellowelephant.saucer.callback.FoodMenuListener;
import com.mellowelephant.saucer.callback.SignInListener;
import com.mellowelephant.saucer.dto.BeerDTO;
import com.mellowelephant.saucer.dto.BeerRatingDTO;
import com.mellowelephant.saucer.dto.BuddyDTO;
import com.mellowelephant.saucer.dto.ContainerDTO;
import com.mellowelephant.saucer.dto.SaucerLocationDTO;
import com.mellowelephant.saucer.dto.TTLResponse;
import com.mellowelephant.saucer.dto.UserDTO;
import com.mellowelephant.saucer.util.AppState;
import com.mellowelephant.saucer.util.BeerFilter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class AppData
{
	//Note: Some of these methods take a Context as a parameter. This is because if the activity is not running (i.e. if you're calling any of these from a service), the AppState context can be null.
	
	private static final String PREFERENCESFILE = "FlyingSaucerPrefs";
	
	//Location keys
	private static final String PREF_LOCATION = "Location_Selection";								//"Auto" or one of the locations
	private static final String PREF_LOCATION_LASTKNOWN = "Location_LastKnownAuto";					//Last known automatic location
	private static final String PREF_LOCATION_LASTKNOWNTIME = "Location_LastKnownAuto_TimeStamp";	//Timestamp of last known automatic location
	private static final String AUTO_LOCATION = "Auto";
	private static final long LOCATION_CACHE_LENGTH = 3600000;	//1 hour
	
	//Login keys
	private static final String PREF_LOGGEDINUSER = "LoggedInUser";
	
	//Misc Feature keys
	private static final String PREF_BEERMEINSTRUCTIONSVIEWED = "BMIV";
	
	//Notification keys
	private static final String PREF_NOTIFICATIONSENABLED = "NOTEON";
	private static final String PREF_NOTIFICATIONSKEY = "NOTEKEY";
	private static final String PREF_NOTIFICATIONSMBRID = "NOTEMBRID";
	private static final String PREF_NOTIFICATIONSBUSY = "NOTEBUSY";
	
	//BeerFilter keys
	private static final String PREF_FILTERHIDECONSUMED = "FilterHideConsumed";
	private static final String PREF_FILTERBEERNAME = "FilterBeerName";
	private static final String PREF_FILTERSTYLES = "FilterStyles";
	private static final String PREF_FILTERBREWERIES = "FilterBreweries";
	private static final String PREF_FILTERCONTAINER = "FilterContainer";
	private static final String PREF_FILTERLANDED = "FilterLanded";
	
	//Drinking Buddies keys
	private static final String PREF_MANUALREFRESHES = "ManualRefreshes";
	
	//Network data handler
	private static NetData netData = null;
	
	//SQL data handler
	private static SQLData sqlData = null;
	
	public static String getDeviceId()
	{
		return getDeviceId(AppState.context);
	}
	public static String getDeviceId(Context ctx)
	{
		return Settings.Secure.getString(ctx.getContentResolver(), Settings.Secure.ANDROID_ID);
	}
	public static boolean isNotificationsEnabled()
	{
		SharedPreferences prefs = AppState.context.getSharedPreferences(PREFERENCESFILE, 0);
		
		if(prefs.contains(PREF_NOTIFICATIONSENABLED))
			return prefs.getBoolean(PREF_NOTIFICATIONSENABLED, true);
		else
			return AppState.isGooglePlayAvailable(false);	//We've never registered for GCM. Default to "on" if Google Play Services is available
	}
	public static void setNotificationsEnabled(boolean isEnabled)
	{
		AppState.context.getSharedPreferences(PREFERENCESFILE, 0).edit().putBoolean(PREF_NOTIFICATIONSENABLED, isEnabled).commit();
		initializeNotifications();
	}
	public static String getNotificationsKey()
	{
		return AppState.context.getSharedPreferences(PREFERENCESFILE, 0).getString(PREF_NOTIFICATIONSKEY, null);
	}
	public static Long getNotificationsMbrId()
	{
		return AppState.context.getSharedPreferences(PREFERENCESFILE, 0).getLong(PREF_NOTIFICATIONSMBRID, 0);
	}
	public static void setNotificationsKey(String key, Long mbrId)
	{
		Editor prefs = AppState.context.getSharedPreferences(PREFERENCESFILE, 0).edit();
		
		prefs.putBoolean(PREF_NOTIFICATIONSENABLED, (key != null));
		
		if(key == null)
		{
			prefs.remove(PREF_NOTIFICATIONSKEY);
			prefs.remove(PREF_NOTIFICATIONSMBRID);
		}
		else
		{
			prefs.putString(PREF_NOTIFICATIONSKEY, key);
			prefs.putLong(PREF_NOTIFICATIONSMBRID, mbrId);
		}
		
		prefs.commit();
	}
	public static boolean isNotificationsBusy()
	{
		return AppState.context.getSharedPreferences(PREFERENCESFILE, 0).getBoolean(PREF_NOTIFICATIONSBUSY, false);
	}
	public static void setNotificationsBusy(boolean isBusy)
	{
		if(isBusy)
			AppState.context.getSharedPreferences(PREFERENCESFILE, 0).edit().putBoolean(PREF_NOTIFICATIONSBUSY, true).commit();
		else
			AppState.context.getSharedPreferences(PREFERENCESFILE, 0).edit().remove(PREF_NOTIFICATIONSBUSY).commit();
	}
	public static UserDTO getLoggedInUser()
	{
		return getLoggedInUser(AppState.context);
	}
	public static boolean beerMeInstructionsViewed()
	{
		return AppState.context.getSharedPreferences(PREFERENCESFILE, 0).getBoolean(PREF_BEERMEINSTRUCTIONSVIEWED, false);
	}
	public static void setBeerMeInstructionsViewed(boolean viewed)
	{
		AppState.context.getSharedPreferences(PREFERENCESFILE, 0).edit().putBoolean(PREF_BEERMEINSTRUCTIONSVIEWED, viewed).commit();
	}
	public static UserDTO getLoggedInUser(Context ctx)
	{
		return MEG.fromJson(ctx.getSharedPreferences(PREFERENCESFILE, 0).getString(PREF_LOGGEDINUSER, null), UserDTO.class);
	}
	public static void setLoggedInUser(UserDTO loggedInUser)
	{
		getNetData().stopLoginSensitiveTasks();
		
		SharedPreferences.Editor prefs = AppState.context.getSharedPreferences(PREFERENCESFILE, 0).edit();
		if(loggedInUser == null)
		{
			prefs.remove(PREF_LOGGEDINUSER);
			prefs.remove(PREF_BEERMEINSTRUCTIONSVIEWED);
			setNotificationsEnabled(false);
		}
		else
		{
			prefs.putString(PREF_LOGGEDINUSER, MEG.toJson(loggedInUser));
			setNotificationsEnabled(true);
		}
		
		prefs.commit();
	}
	
	public static SaucerLocationDTO getSavedLocation(){return getSavedLocation(false);}
	public static SaucerLocationDTO getSavedLocation(boolean evenIfItsOld)	//Null signals the app to retrieve a new location via Fused Location API
	{
		SharedPreferences prefs = AppState.context.getSharedPreferences(PREFERENCESFILE, 0);
		
		String location = prefs.getString(PREF_LOCATION, AUTO_LOCATION);
		if(location.equals(AUTO_LOCATION))
		{
			String lastKnown = prefs.getString(PREF_LOCATION_LASTKNOWN, null);
			long lastKnownTimeStamp = prefs.getLong(PREF_LOCATION_LASTKNOWNTIME, 0);
			
			if(System.currentTimeMillis() <= lastKnownTimeStamp + LOCATION_CACHE_LENGTH || evenIfItsOld)
			{
				Log.d(AppState.logTag, "Returning previously-retrieved automatic location: " + lastKnown);
				return SaucerLocationDTO.getByDisplayName(lastKnown);
			}
			else
			{
				Log.d(AppState.logTag, "Previously-retrieved automatic location is too old. Fetch a new one");
				return null;
			}
		}
		else
		{
			Log.d(AppState.logTag, "Returning manually-set location preference: " + location);
			return SaucerLocationDTO.getByDisplayName(location);
		}
	}
	public static boolean isSavedLocationAutomatic()
	{
		return AppState.context.getSharedPreferences(PREFERENCESFILE, 0).getString(PREF_LOCATION, AUTO_LOCATION).equals(AUTO_LOCATION);
	}
	public static boolean isSavedLocationCurrent()
	{
		if(!isSavedLocationAutomatic())
			return true;
		else
			return System.currentTimeMillis() <= (AppState.context.getSharedPreferences(PREFERENCESFILE, 0).getLong(PREF_LOCATION_LASTKNOWNTIME, 0) + LOCATION_CACHE_LENGTH);
	}
	
	public static void setSavedLocation(SaucerLocationDTO newLocation, boolean wasSetAutomatically)
	{
		SharedPreferences.Editor prefs = AppState.context.getSharedPreferences(PREFERENCESFILE, 0).edit();

		if(wasSetAutomatically)
		{
			prefs.putString(PREF_LOCATION, AUTO_LOCATION);
			
			prefs.putString(PREF_LOCATION_LASTKNOWN, newLocation.displayName);
			prefs.putLong(PREF_LOCATION_LASTKNOWNTIME, System.currentTimeMillis());
		}
		else
		{
			prefs.putString(PREF_LOCATION, newLocation.displayName);
			
			prefs.remove(PREF_LOCATION_LASTKNOWN);
			prefs.remove(PREF_LOCATION_LASTKNOWNTIME);
		}
		
		prefs.commit();
	}
	
	private static NetData getNetData()
	{
		if(netData == null)
			netData = new NetData();
		
		return netData;
	}
	public static SQLData getSQLData()
	{
		if(sqlData == null)
			sqlData = new SQLData(AppState.context);
		
		return sqlData;
	}
	
	public static void initializeNotifications()	//Called when app starts and whenever push notifications are enabled/disabled.
	{
		if(getLoggedInUser() != null && isNotificationsEnabled() && getNotificationsKey() == null)
		{
			//Notifications are enabled, but we aren't registered
			Log.d(AppState.logTag, "Need to register for GCM");
			getNetData().registerForNotifications(true);
		}
		else if(!isNotificationsEnabled() && getNotificationsKey() != null)
		{
			//Notifications are disabled, but we are registered
			Log.d(AppState.logTag, "Need to unregister for GCM");
			getNetData().registerForNotifications(false);
		}
	}
	public static void retrieveBeerList(BeerListListener callback)
	{
		getNetData().retrieveBeerList(AppState.context, AppState.currentLocation, AppState.user, callback);
	}
	
	public static Map<SaucerLocationDTO, List<BeerDTO>> getValidCachedBeerLists()
	{
		return getSQLData().getValidCachedBeerLists();
	}
	
	public static void updateBeerListInCache(SaucerLocationDTO location, List<BeerDTO> updatedBeerList)
	{
		getSQLData().updateBeerListInCache(location, updatedBeerList);
	}
	
	public static void saveBeerListToCache(SaucerLocationDTO location, TTLResponse<List<BeerDTO>> beerList)
	{
		getSQLData().saveBeerListToCache(location, beerList);
	}
	
	public static void clearBeerListCache()
	{
		getSQLData().clearBeerListCache();
	}
	
	public static List<BuddyDTO> getValidCachedBeerBuddyList()
	{
		return getSQLData().getCachedBeerBuddyList();
	}
	
	public static void retrieveBeerBuddyList(BeerBuddyListListener callback)
	{
		getNetData().retrieveBeerBuddyList(AppState.context, AppState.user, callback);
	}

	public static void updateBeerBuddyListInCache(List<BuddyDTO> updatedBuddyList)
	{
		getSQLData().updateBeerBuddyListInCache(updatedBuddyList);
	}
	
	public static void saveBeerBuddyListToCache(TTLResponse<List<BuddyDTO>> buddyList)
	{
		getSQLData().saveBeerBuddyListToCache(buddyList);
	}
	
	public static void clearBeerBuddyListCache()
	{
		getSQLData().clearBeerBuddyListCache();
	}
	
	public static void retrieveBeerBuddyBeerList(BeerListListener callback)
	{
		getNetData().retrieveBeerBuddyBeerList(AppState.context, AppState.currentLocation, AppState.user, callback);
	}
	
	public static void inviteBuddy(String cardNumber, String dob, BeerBuddyActionListener callback)
	{
		getNetData().inviteBuddy(cardNumber, dob, callback);
	}
	
	public static void removeBuddy(Long removeMemberId, BeerBuddyActionListener callback)
	{
		getNetData().removeBuddy(removeMemberId, callback);
	}
	
	public static void addBeerToPendingQueue(BeerDTO beer, SaucerLocationDTO location)
	{
		getSQLData().addBeerToPendingQueue(beer, location);
	}
	
	public static boolean removeBeerFromPendingQueue(Long beerId)
	{
		return getSQLData().removeBeerFromPendingQueue(beerId);
	}

	public static boolean addCommentRatingToPendingQueue(BeerDTO beer)
	{
		return getSQLData().addCommentRatingToPendingQueue(beer);
	}
	
	public static void addBeerToRemoveQueue(Long beerId)
	{
		getSQLData().addBeerToRemoveQueue(beerId);
	}
	
	public static void addBeerToCommentRatingQueue(BeerDTO beer)
	{
		getSQLData().addBeerToCommentRatingQueue(beer);
	}
	
	public static Map<SaucerLocationDTO, List<BeerRatingDTO>> getPendingQueue()
	{
		return getSQLData().getPendingQueue();
	}
	public static void clearPendingQueueForLocation(SaucerLocationDTO location)
	{
		getSQLData().clearPendingQueueForLocation(location);
	}
	
	public static List<BeerRatingDTO> getRemoveQueue()
	{
		return getSQLData().getRemoveQueue();
	}
	public static void clearRemoveQueue()
	{
		getSQLData().clearRemoveQueue();
	}

	public static List<BeerRatingDTO> getCommentRatingQueue()
	{
		return getSQLData().getCommentRatingQueue();
	}
	public static void clearCommentRatingQueue()
	{
		getSQLData().clearCommentRatingQueue();
	}
	
	public static void retrieveFoodMenu(FoodMenuListener callback)
	{
		getNetData().retrieveFoodMenu(callback);
	}
	
	public static void loginUser(String ufoNumber, String password, SaucerLocationDTO location, boolean isMou, SignInListener callback)
	{
		getNetData().loginUser(ufoNumber, password, location, isMou, callback);
	}
	
	public static int getNumberOfManualBuddyListRefreshes()
	{
		return AppState.context.getSharedPreferences(PREFERENCESFILE, Context.MODE_MULTI_PROCESS).getInt(PREF_MANUALREFRESHES, 0);	//MODE_MULTI_PROCESS because we don't want this value cached
	}
	
	public static void setNumberOfManualBuddyListRefreshes(int number)
	{
		setNumberOfManualBuddyListRefreshes(AppState.context, number);
	}
	
	public static void setNumberOfManualBuddyListRefreshes(Context ctx, int number)
	{
		ctx.getSharedPreferences(PREFERENCESFILE, 0).edit().putInt(PREF_MANUALREFRESHES, number).commit();
	}
	
	public static void saveBeerFilter(BeerFilter filter)
	{
		Editor prefs = AppState.context.getSharedPreferences(PREFERENCESFILE, Context.MODE_MULTI_PROCESS).edit();
		
		prefs.putBoolean(PREF_FILTERHIDECONSUMED, filter.hideBeersIveHad);
		
		if(filter.selectedNameFilter != null)
			prefs.putString(PREF_FILTERBEERNAME, filter.selectedNameFilter);
		else
			prefs.remove(PREF_FILTERBEERNAME);
		
		if(filter.styleFilter != null)
			prefs.putString(PREF_FILTERSTYLES, listToString(filter.styleFilter));
		else
			prefs.remove(PREF_FILTERSTYLES);
		
		if(filter.breweryFilter != null)
			prefs.putString(PREF_FILTERBREWERIES, listToString(filter.breweryFilter));
		else
			prefs.remove(PREF_FILTERBREWERIES);
		
		if(filter.containerFilter != null)
			prefs.putString(PREF_FILTERCONTAINER, filter.containerFilter.displayName);
		else
			prefs.remove(PREF_FILTERCONTAINER);
		
		if(filter.landedFilter != null)
			prefs.putInt(PREF_FILTERLANDED, filter.landedFilter);
		else
			prefs.remove(PREF_FILTERLANDED);
		
		prefs.commit();
	}
	
	public static BeerFilter loadBeerFilter()
	{
		return loadBeerFilter(AppState.context);
	}
	public static BeerFilter loadBeerFilter(Context context)
	{
		SharedPreferences prefs = context.getSharedPreferences(PREFERENCESFILE, Context.MODE_MULTI_PROCESS);
		
		BeerFilter filter = new BeerFilter();
		filter.hideBeersIveHad = prefs.getBoolean(PREF_FILTERHIDECONSUMED, false);
		filter.selectedNameFilter = prefs.getString(PREF_FILTERBEERNAME, null);
		filter.styleFilter = prefs.contains(PREF_FILTERSTYLES) ? new ArrayList<String>(Arrays.asList(prefs.getString(PREF_FILTERSTYLES, "").split(","))) : null;
		filter.breweryFilter = prefs.contains(PREF_FILTERBREWERIES) ? new ArrayList<String>(Arrays.asList(prefs.getString(PREF_FILTERBREWERIES, "").split(","))) : null;
		filter.containerFilter = prefs.contains(PREF_FILTERCONTAINER) ? ContainerDTO.fromDisplayName(prefs.getString(PREF_FILTERCONTAINER, null)) : null;
		filter.landedFilter = prefs.contains(PREF_FILTERLANDED) ? prefs.getInt(PREF_FILTERLANDED, 0) : null;
		
		return filter;
	}
	
	private static String listToString(List<String> list)
	{
		StringBuilder string = new StringBuilder();
		
		for(String item : list)
			string.append(item + ",");
		
		string.deleteCharAt(string.length() - 1);
		return string.toString();
	}
}
