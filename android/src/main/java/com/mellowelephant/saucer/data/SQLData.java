package com.mellowelephant.saucer.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.google.gson.reflect.TypeToken;
import com.mellowelephant.saucer.MEG;
import com.mellowelephant.saucer.dto.BeerDTO;
import com.mellowelephant.saucer.dto.BeerRatingDTO;
import com.mellowelephant.saucer.dto.BuddyBeersDTO;
import com.mellowelephant.saucer.dto.BuddyDTO;
import com.mellowelephant.saucer.dto.BuddyStatusDTO;
import com.mellowelephant.saucer.dto.SaucerLocationDTO;
import com.mellowelephant.saucer.dto.TTLResponse;
import com.mellowelephant.saucer.util.AppState;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SQLData extends SQLiteOpenHelper
{
	private static final int DATABASE_VERSION = 2;
	private static final String DATABASE_NAME = "FlyingSaucerSQL";
	
	//BeerList cache table
	private static final String TABLE_BEERLISTCACHE = "BeerListCache";
	private static final String COL_BEERLISTCACHE_LOC = "Location";
	private static final String COL_BEERLISTCACHE_EXPIRY = "Expiry";
	private static final String COL_BEERLISTCACHE_DATA = "Data";
	
	//Drinking Buddies cache table
	private static final String TABLE_BUDDYCACHE = "DrinkingBuddiesCache";
	private static final String COL_BUDDYCACHE_KEY = "Key";
	private static final String COL_BUDDYCACHE_EXPIRY = "Expiry";
	private static final String COL_BUDDYCACHE_DATA = "Data";
	private static final String KEY_BUDDYCACHE_BUDDYLIST = "BuddyList";
	private static final String KEY_BUDDYCACHE_BUDDYBEERS = "BuddyBeers";
	
	//Pending beers queue (doesn't hold pending beers. Holds beers that we need to send to the server as pending beers)
	private static final String TABLE_PENDINGQUEUE = "PendingBeerQueue";
	private static final String COL_PENDINGQUEUE_BEERID = "BeerID";
	private static final String COL_PENDINGQUEUE_COMMENT = "Comment";
	private static final String COL_PENDINGQUEUE_RATING = "Rating";
	private static final String COL_PENDINGQUEUE_REVIEWID = "ReviewID";
	private static final String COL_PENDINGQUEUE_LOC = "Location";
	
	//Remove beers queue
	private static final String TABLE_REMOVEQUEUE = "RemoveQueue";
	private static final String COL_REMOVEQUEUE_BEERID = "BeerID";
	
	//Comment+Rating queue
	private static final String TABLE_COMMENTRATINGQUEUE = "CommentRatingQueue";
	private static final String COL_COMMENTRATINGQUEUE_BEERID = "BeerID";
	private static final String COL_COMMENTRATINGQUEUE_COMMENT = "Comment";
	private static final String COL_COMMENTRATINGQUEUE_RATING = "Rating";
	private static final String COL_COMMENTRATINGQUEUE_REVIEWID = "ReviewID";
	
	private Context ctx;
	
	public SQLData(Context context)
	{
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		ctx = context;
	}

	@Override
	public void onCreate(SQLiteDatabase db)
	{
		Log.d(AppState.logTag, "SQLite onCreate()");
		db.execSQL("CREATE TABLE " + TABLE_BEERLISTCACHE + " (" + COL_BEERLISTCACHE_LOC + " TEXT, " + COL_BEERLISTCACHE_EXPIRY + " INTEGER, " + COL_BEERLISTCACHE_DATA + " TEXT)");
		db.execSQL("CREATE TABLE " + TABLE_BUDDYCACHE + " (" + COL_BUDDYCACHE_KEY + " TEXT, " + COL_BUDDYCACHE_EXPIRY + " INTEGER, " + COL_BUDDYCACHE_DATA + " TEXT)");
		db.execSQL("CREATE TABLE " + TABLE_PENDINGQUEUE + " (" + COL_PENDINGQUEUE_BEERID + " INTEGER, " + COL_PENDINGQUEUE_COMMENT + " TEXT, " + COL_PENDINGQUEUE_RATING + " INTEGER, " + COL_PENDINGQUEUE_REVIEWID + " TEXT, " + COL_PENDINGQUEUE_LOC + " TEXT)");
		db.execSQL("CREATE TABLE " + TABLE_REMOVEQUEUE + " (" + COL_REMOVEQUEUE_BEERID + " INTEGER)");
		db.execSQL("CREATE TABLE " + TABLE_COMMENTRATINGQUEUE + " (" + COL_COMMENTRATINGQUEUE_BEERID + " INTEGER, " + COL_COMMENTRATINGQUEUE_COMMENT + " TEXT, " + COL_COMMENTRATINGQUEUE_RATING + " INTEGER, " + COL_COMMENTRATINGQUEUE_REVIEWID + " TEXT)");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
	{
		Log.d(AppState.logTag, "SQLite onUpgrade called. Old Version: " + oldVersion + ", New Version: " + newVersion);
		
		if(oldVersion < 2)	//Buddy cache implemented in DB version 2
			db.execSQL("CREATE TABLE " + TABLE_BUDDYCACHE + " (" + COL_BUDDYCACHE_KEY + " TEXT, " + COL_BUDDYCACHE_EXPIRY + " INTEGER, " + COL_BUDDYCACHE_DATA + " TEXT)");
	}
	
	public Map<SaucerLocationDTO, List<BeerDTO>> getValidCachedBeerLists()
	{
		Map<SaucerLocationDTO, List<BeerDTO>> cacheMap = new HashMap<SaucerLocationDTO, List<BeerDTO>>();
		
		SQLiteDatabase db = getReadableDatabase();
		if(db == null)
			return null;
		
		Cursor c = db.rawQuery("SELECT * FROM " + TABLE_BEERLISTCACHE, null);
		int expiryColumn = c.getColumnIndex(COL_BEERLISTCACHE_EXPIRY);
		int dataColumn = c.getColumnIndex(COL_BEERLISTCACHE_DATA);
		int locColumn = c.getColumnIndex(COL_BEERLISTCACHE_LOC);
		while(c.moveToNext())
		{
			SaucerLocationDTO location = SaucerLocationDTO.getByDisplayName(c.getString(locColumn));
			if(System.currentTimeMillis() <= c.getLong(expiryColumn))
			{
				Log.d(AppState.logTag, "Found a cached beer list for " + location.displayName);
				
				List<BeerDTO> beers = MEG.fromJson(c.getString(dataColumn), new TypeToken<List<BeerDTO>>(){}.getType());
				cacheMap.put(location, beers);
			}
			else
				Log.d(AppState.logTag, "Found a cached beer list for " + location.displayName + ", but it has expired");
		}
		
		Log.d(AppState.logTag, "Found " + cacheMap.size() + " valid cached beer lists.");
		
		if(c != null)
			c.close();
		if(db != null)
			db.close();
		
		return cacheMap;
	}
	
	public List<BeerDTO> getCachedBeerList(SaucerLocationDTO location)
	{
		if(location == null)
			return null;
		
		List<BeerDTO> beers = null;
		
		SQLiteDatabase db = getReadableDatabase();
		if(db == null)
			return null;
		
		Cursor c = db.rawQuery("SELECT * FROM " + TABLE_BEERLISTCACHE + " WHERE " + COL_BEERLISTCACHE_LOC + " = ?", new String[]{location.displayName});
		if(c.moveToNext())
		{
			int expiryColumn = c.getColumnIndex(COL_BEERLISTCACHE_EXPIRY);
			int dataColumn = c.getColumnIndex(COL_BEERLISTCACHE_DATA);
			
			if(System.currentTimeMillis() <= c.getLong(expiryColumn))
			{
				Log.d(AppState.logTag, "Found a cached beer list for " + location.displayName);
				beers = MEG.fromJson(c.getString(dataColumn), new TypeToken<List<BeerDTO>>(){}.getType());
			}
			else
				Log.d(AppState.logTag, "Found a cached beer list for " + location.displayName + ", but it has expired.");
		}
		else
			Log.d(AppState.logTag, "Didn't find a cached beer list for " + location.displayName);
		
		if(c != null)
			c.close();
		if(db != null)
			db.close();
		
		return beers;
	}
	
	public void saveBeerListToCache(SaucerLocationDTO location, TTLResponse<List<BeerDTO>> beerList)
	{
		if(beerList != null && beerList.getPayload() != null)
		{
			Log.d(AppState.logTag, "Saving " + location.displayName + " beer list to cache with TTL: " + beerList.getTimeToLive());
			
			SQLiteDatabase db = getWritableDatabase();
			db.execSQL("DELETE FROM " + TABLE_BEERLISTCACHE + " WHERE " + COL_BEERLISTCACHE_LOC + " = ?", new String[]{location.displayName});
			db.execSQL("INSERT INTO " + TABLE_BEERLISTCACHE + " (" + COL_BEERLISTCACHE_LOC + ", " + COL_BEERLISTCACHE_EXPIRY + ", " + COL_BEERLISTCACHE_DATA + ") VALUES (?, ?, ?)", new Object[]{location.displayName, Long.valueOf(System.currentTimeMillis() + beerList.getTimeToLive()), MEG.toJson(beerList.getPayload())});
			db.close();
		}
		else
			Log.w(AppState.logTag, "Received a beerList to save to SQL but the list or the payload were null!");
	}
	
	public void updateBeerListInCache(SaucerLocationDTO location, List<BeerDTO> updatedBeerList)
	{
		if(updatedBeerList != null)
		{
			Log.d(AppState.logTag, "Updating " + location.displayName + " beer list to cache.");
			
			SQLiteDatabase db = getWritableDatabase();
			ContentValues x = new ContentValues();
			x.put(COL_BEERLISTCACHE_DATA, MEG.toJson(updatedBeerList));
			db.update(TABLE_BEERLISTCACHE, x, COL_BEERLISTCACHE_LOC + " = ?", new String[]{location.displayName});
			db.close();
		}
		else
			Log.w(AppState.logTag, "Received an updatedBeerList to save to SQL but the list was null!");
	}
	
	public void clearBeerListCache()
	{
		Log.d(AppState.logTag, "Clearing beer list cache.");
		
		SQLiteDatabase db = getWritableDatabase();
		db.execSQL("DELETE FROM " + TABLE_BEERLISTCACHE);
		db.close();
	}
	
	public List<BuddyDTO> getCachedBeerBuddyList()
	{
		List<BuddyDTO> buddyList = null;

		SQLiteDatabase db = getReadableDatabase();
		if(db == null)
			return null;

		Cursor c = db.rawQuery("SELECT * FROM " + TABLE_BUDDYCACHE + " WHERE " + COL_BUDDYCACHE_KEY + " = ?", new String[]{KEY_BUDDYCACHE_BUDDYLIST});
		if(c.moveToNext())
		{
			int expiryColumn = c.getColumnIndex(COL_BUDDYCACHE_EXPIRY);
			int dataColumn = c.getColumnIndex(COL_BUDDYCACHE_DATA);

			if(System.currentTimeMillis() <= c.getLong(expiryColumn))
			{
				Log.d(AppState.logTag, "Found a cached buddy list");
				buddyList = MEG.fromJson(c.getString(dataColumn), new TypeToken<List<BuddyDTO>>(){}.getType());
			}
			else
			{
				Log.d(AppState.logTag, "Found a cached buddy list, but it has expired. Clearing manual refresh count.");
				AppData.setNumberOfManualBuddyListRefreshes(ctx, 0);
			}
		}
		else
			Log.d(AppState.logTag, "Didn't find a cached buddy list");

		if(c != null)
			c.close();
		if(db != null)
			db.close();

		return buddyList;
	}
	
	public void saveBeerBuddyListToCache(TTLResponse<List<BuddyDTO>> buddyList)
	{
		if(buddyList != null && buddyList.getPayload() != null)
		{
			Log.d(AppState.logTag, "Saving buddy list to cache with TTL: " + buddyList.getTimeToLive());

			SQLiteDatabase db = getWritableDatabase();
			db.execSQL("DELETE FROM " + TABLE_BUDDYCACHE + " WHERE " + COL_BUDDYCACHE_KEY + " = ?", new String[]{KEY_BUDDYCACHE_BUDDYLIST});
			db.execSQL("INSERT INTO " + TABLE_BUDDYCACHE + " (" + COL_BUDDYCACHE_KEY + ", " + COL_BUDDYCACHE_EXPIRY + ", " + COL_BUDDYCACHE_DATA + ") VALUES (?, ?, ?)", new Object[]{KEY_BUDDYCACHE_BUDDYLIST, Long.valueOf(System.currentTimeMillis() + buddyList.getTimeToLive()), MEG.toJson(buddyList.getPayload())});
			db.close();
		}
		else
			Log.w(AppState.logTag, "Received a buddy list to save to SQL but the list or the payload were null!");
	}
	
	public void updateBeerBuddyListInCache(List<BuddyDTO> updatedBuddyList)
	{
		if(updatedBuddyList != null)
		{
			Log.d(AppState.logTag, "Updating buddy list to cache.");

			SQLiteDatabase db = getWritableDatabase();
			ContentValues x = new ContentValues();
			x.put(COL_BUDDYCACHE_DATA, MEG.toJson(updatedBuddyList));
			db.update(TABLE_BUDDYCACHE, x, COL_BUDDYCACHE_KEY + " = ?", new String[]{KEY_BUDDYCACHE_BUDDYLIST});
			db.close();
		}
		else
			Log.w(AppState.logTag, "Received an updatedBuddyList to save to SQL but the list was null!");
	}
	
	public void clearBeerBuddyListCache()
	{
		Log.d(AppState.logTag, "Clearing beer buddy list cache.");
		
		SQLiteDatabase db = getWritableDatabase();
		db.execSQL("DELETE FROM " + TABLE_BUDDYCACHE);
		db.close();
	}
	
	private boolean buddyIdsMatch(List<Long> list1, List<Long> list2)
	{
		if(list1.size() != list2.size())
			return false;
		
		outer: for(Long x : list1)
		{
			for(Long y : list2)
			{
				if(x.equals(y))
					continue outer;
			}
			
			return false;
		}
		
		return true;
	}
	
	public List<BeerDTO> getCachedBeerBuddyBeerList()
	{
		List<BeerDTO> buddyBeerList = null;

		SQLiteDatabase db = getReadableDatabase();
		if(db == null)
			return null;

		Cursor c = db.rawQuery("SELECT * FROM " + TABLE_BUDDYCACHE + " WHERE " + COL_BUDDYCACHE_KEY + " = ?", new String[]{KEY_BUDDYCACHE_BUDDYBEERS});
		if(c.moveToNext())
		{
			int expiryColumn = c.getColumnIndex(COL_BUDDYCACHE_EXPIRY);
			int dataColumn = c.getColumnIndex(COL_BUDDYCACHE_DATA);

			if(System.currentTimeMillis() <= c.getLong(expiryColumn))
			{
				BuddyBeersDTO buddyBeers = MEG.fromJson(c.getString(dataColumn), BuddyBeersDTO.class);
				List<Long> buddyBeerMemberIds = buddyBeers.getMemberIds();
				List<Long> cachedBuddyMemberIds = new ArrayList<Long>();

				List<BuddyDTO> cachedBuddies = getCachedBeerBuddyList();
				if(cachedBuddies != null)
				{
					for(BuddyDTO cachedBuddy : cachedBuddies)
					{
						if(cachedBuddy.getStatus() == BuddyStatusDTO.ACCEPTED)
							cachedBuddyMemberIds.add(cachedBuddy.getMemberId());
					}
				}

				if(buddyIdsMatch(buddyBeerMemberIds, cachedBuddyMemberIds))
				{
					Log.d(AppState.logTag, "Found a cached buddy beer list");
					buddyBeerList = buddyBeers.getBeers();
				}
				else
					Log.d(AppState.logTag, "Found a cached buddy beer list, but buddy IDs don't match");
			}
			else
				Log.d(AppState.logTag, "Found a cached buddy beer list, but it has expired");
		}
		else
			Log.d(AppState.logTag, "Didn't find a cached buddy beer list");

		if(c != null)
			c.close();
		if(db != null)
			db.close();

		return buddyBeerList;
	}
	
	public void saveBeerBuddyBeerListToCache(TTLResponse<BuddyBeersDTO> buddyBeerList)
	{
		if(buddyBeerList != null && buddyBeerList.getPayload() != null)
		{
			Log.d(AppState.logTag, "Saving buddy beer list to cache with TTL: " + buddyBeerList.getTimeToLive());

			SQLiteDatabase db = getWritableDatabase();
			db.execSQL("DELETE FROM " + TABLE_BUDDYCACHE + " WHERE " + COL_BUDDYCACHE_KEY + " = ?", new String[]{KEY_BUDDYCACHE_BUDDYBEERS});
			db.execSQL("INSERT INTO " + TABLE_BUDDYCACHE + " (" + COL_BUDDYCACHE_KEY + ", " + COL_BUDDYCACHE_EXPIRY + ", " + COL_BUDDYCACHE_DATA + ") VALUES (?, ?, ?)", new Object[]{KEY_BUDDYCACHE_BUDDYBEERS, Long.valueOf(System.currentTimeMillis() + buddyBeerList.getTimeToLive()), MEG.toJson(buddyBeerList.getPayload())});
			db.close();
		}
		else
			Log.w(AppState.logTag, "Received a buddy beer list to save to SQL but the list or the payload were null!");
	}
	
	public void addBeerToPendingQueue(BeerDTO beer, SaucerLocationDTO location)
	{
		SQLiteDatabase db = getWritableDatabase();
		
		ContentValues x = new ContentValues();
		x.put(COL_PENDINGQUEUE_BEERID, beer.getId());
		x.put(COL_PENDINGQUEUE_COMMENT, beer.getComments());
		x.put(COL_PENDINGQUEUE_RATING, beer.getRating());
		x.put(COL_PENDINGQUEUE_REVIEWID, beer.getReviewId());
		x.put(COL_PENDINGQUEUE_LOC, location.displayName);
		db.insert(TABLE_PENDINGQUEUE, null, x);
		db.close();
	}
	
	public boolean removeBeerFromPendingQueue(Long beerId)
	{	
		SQLiteDatabase db = getWritableDatabase();
		int rowsAffected = db.delete(TABLE_PENDINGQUEUE, COL_PENDINGQUEUE_BEERID + " = ?", new String[]{String.valueOf(beerId)});
		db.close();
		
		return rowsAffected > 0;
	}

	public boolean addCommentRatingToPendingQueue(BeerDTO beer)
	{
		SQLiteDatabase db = getWritableDatabase();
		
		ContentValues x = new ContentValues();
		x.put(COL_PENDINGQUEUE_COMMENT, beer.getComments());
		x.put(COL_PENDINGQUEUE_RATING, beer.getRating());
		int rowsAffected = db.update(TABLE_PENDINGQUEUE, x, COL_PENDINGQUEUE_BEERID + " = ?", new String[]{String.valueOf(beer.getId())});
		db.close();
		
		return rowsAffected > 0;
	}
	
	public void addBeerToRemoveQueue(Long beerId)
	{
		SQLiteDatabase db = getWritableDatabase();
		
		ContentValues x = new ContentValues();
		x.put(COL_REMOVEQUEUE_BEERID, beerId);
		db.insert(TABLE_REMOVEQUEUE, null, x);
		db.close();
	}
	
	public void addBeerToCommentRatingQueue(BeerDTO beer)
	{
		SQLiteDatabase db = getWritableDatabase();
		
		//Attempt an update first in case this beer is already in the queue
		ContentValues x = new ContentValues();
		x.put(COL_PENDINGQUEUE_COMMENT, beer.getComments());
		x.put(COL_PENDINGQUEUE_RATING, beer.getRating());
		int rowsAffected = db.update(TABLE_COMMENTRATINGQUEUE, x, COL_COMMENTRATINGQUEUE_BEERID + " = ?", new String[]{String.valueOf(beer.getId())});
		
		//If the update affected 0 rows, it must not have been there already. Add it now.
		if(rowsAffected < 1)
		{
			x.put(COL_COMMENTRATINGQUEUE_BEERID, beer.getId());
			db.insert(TABLE_COMMENTRATINGQUEUE, null, x);
		}
		
		db.close();
	}
	
	public Map<SaucerLocationDTO, List<BeerRatingDTO>> getPendingQueue()
	{
		Map<SaucerLocationDTO, List<BeerRatingDTO>> pendingQueue = new HashMap<>();
		
		SQLiteDatabase db = getReadableDatabase();
		Cursor c = db.rawQuery("SELECT * FROM " + TABLE_PENDINGQUEUE, null);
		int idColumn = c.getColumnIndex(COL_PENDINGQUEUE_BEERID);
		int commentColumn = c.getColumnIndex(COL_PENDINGQUEUE_COMMENT);
		int ratingColumn = c.getColumnIndex(COL_PENDINGQUEUE_RATING);
		int reviewIdColumn = c.getColumnIndex(COL_PENDINGQUEUE_REVIEWID);
		int locColumn = c.getColumnIndex(COL_PENDINGQUEUE_LOC);
		while(c.moveToNext())
		{
			SaucerLocationDTO location = SaucerLocationDTO.getByDisplayName(c.getString(locColumn));
			BeerRatingDTO beerRating = new BeerRatingDTO(c.getLong(idColumn), c.getInt(ratingColumn), c.getString(commentColumn), c.getString(reviewIdColumn));
			
			if(pendingQueue.containsKey(location))
				pendingQueue.get(location).add(beerRating);
			else
				pendingQueue.put(location, new ArrayList<BeerRatingDTO>(Arrays.asList(beerRating)));
		}
		
		if(c != null)
			c.close();
		if(db != null)
			db.close();
		
		return pendingQueue;
	}
	public void clearPendingQueueForLocation(SaucerLocationDTO location)
	{
		SQLiteDatabase db = getWritableDatabase();
		db.delete(TABLE_PENDINGQUEUE, COL_PENDINGQUEUE_LOC + " = ?", new String[]{location.displayName});
		db.close();
	}
	
	public List<BeerRatingDTO> getRemoveQueue()
	{
		List<BeerRatingDTO> removeQueue = new ArrayList<BeerRatingDTO>();
		
		SQLiteDatabase db = getReadableDatabase();
		Cursor c = db.rawQuery("SELECT * FROM " + TABLE_REMOVEQUEUE, null);
		int idColumn = c.getColumnIndex(COL_REMOVEQUEUE_BEERID);
		while(c.moveToNext())
			removeQueue.add(new BeerRatingDTO(c.getLong(idColumn), null, null, null));
		
		if(c != null)
			c.close();
		if(db != null)
			db.close();
		
		return removeQueue;
	}
	public void clearRemoveQueue()
	{
		SQLiteDatabase db = getWritableDatabase();
		db.delete(TABLE_REMOVEQUEUE, null, null);
		db.close();
	}
	public void removeFromRemoveQueue(BeerRatingDTO beer)
	{
		SQLiteDatabase db = getWritableDatabase();
		db.delete(TABLE_REMOVEQUEUE, COL_REMOVEQUEUE_BEERID + " = ?", new String[]{String.valueOf(beer.getBeerId())});
		db.close();
	}

	public List<BeerRatingDTO> getCommentRatingQueue()
	{
		List<BeerRatingDTO> commentRatingQueue = new ArrayList<BeerRatingDTO>();
		
		SQLiteDatabase db = getReadableDatabase();
		Cursor c = db.rawQuery("SELECT * FROM " + TABLE_COMMENTRATINGQUEUE, null);
		int idColumn = c.getColumnIndex(COL_COMMENTRATINGQUEUE_BEERID);
		int commentColumn = c.getColumnIndex(COL_COMMENTRATINGQUEUE_COMMENT);
		int ratingColumn = c.getColumnIndex(COL_COMMENTRATINGQUEUE_RATING);
		int reviewIdColumn = c.getColumnIndex(COL_COMMENTRATINGQUEUE_REVIEWID);
		while(c.moveToNext())
			commentRatingQueue.add(new BeerRatingDTO(c.getLong(idColumn), c.getInt(ratingColumn), c.getString(commentColumn), c.getString(reviewIdColumn)));
		
		if(c != null)
			c.close();
		if(db != null)
			db.close();
		
		return commentRatingQueue;
	}
	public void clearCommentRatingQueue()
	{
		SQLiteDatabase db = getWritableDatabase();
		db.delete(TABLE_COMMENTRATINGQUEUE, null, null);
		db.close();
	}
	public void removeFromCommentRatingQueue(List<BeerRatingDTO> beers)
	{
		StringBuilder whereClause = new StringBuilder(COL_COMMENTRATINGQUEUE_BEERID + " IN (");
		String[] whereArgs = new String[beers.size()];
		
		for(int i = 0; i < beers.size(); i++)
		{
			BeerRatingDTO beer = beers.get(i);
			
			whereClause.append("?,");
			whereArgs[i] = String.valueOf(beer.getBeerId());
		}
		whereClause.deleteCharAt(whereClause.length() - 1);
		whereClause.append(")");
		
		SQLiteDatabase db = getWritableDatabase();
		db.close();
	}
}