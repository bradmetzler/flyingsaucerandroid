package com.mellowelephant.saucer.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu.OnSlideListener;
import com.jeremyfeinstein.slidingmenu.lib.app.SlidingFragmentActivity;
import com.mellowelephant.saucer.R;
import com.mellowelephant.saucer.data.AppData;
import com.mellowelephant.saucer.dto.SaucerLocationDTO;
import com.mellowelephant.saucer.inappbilling.util.PurchaseAware;
import com.mellowelephant.saucer.location.LocationAware;
import com.mellowelephant.saucer.location.LocationFinder;
import com.mellowelephant.saucer.location.LocationPrompt;
import com.mellowelephant.saucer.util.AppState;
import com.mellowelephant.saucer.util.MenuOption;
import com.mellowelephant.saucer.view.DrawerArrowDrawable;

public class MainActivity extends SlidingFragmentActivity implements OnSlideListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationAware
{
	public static final String EXTRA_MOVETOMENUOPTION = "MOVETOMENUOPTION";
	
	private DrawerArrowDrawable drawerIcon;
	
	//For GooglePlayServices (to determine location)
	private final int PLAY_CONN_FAILURE_RESOLUTION_REQUEST = 1235;
	private GoogleApiClient googleApiClient = null;
	private ProgressDialog acquiringSpinner = null;
	private int locationRetryCount = 0;
	
	@Override
	protected void onNewIntent(Intent intent)
	{
		super.onNewIntent(intent);
		
		//Log.w(AppState.logTag, "--onNewIntent");
		
		if(intent.hasExtra(EXTRA_MOVETOMENUOPTION))
			AppState.goTo((MenuOption)intent.getSerializableExtra(EXTRA_MOVETOMENUOPTION));
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		//Log.w(AppState.logTag, "--onCreate");
		
		MenuOption startingMenuOption = null;
		if(getIntent().hasExtra(EXTRA_MOVETOMENUOPTION))
			startingMenuOption = (MenuOption)getIntent().getSerializableExtra(EXTRA_MOVETOMENUOPTION);
		
		AppState.initializeAppState(this, startingMenuOption);
		AppState.registerAsLocationAware("MainActivity", this);
		
		getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.titlebar_background));
		
		setContentView(R.layout.mainactivity_frame);
		
		// Check if the layout contains the menu frame (if it does, menu and content are visible at all times, otherwise it is 1 or the other)
		if(findViewById(R.id.main_menu_frame) == null)
		{
			setBehindContentView(R.layout.main_menu_frame);
			getSlidingMenu().setSlidingEnabled(true);
			getSlidingMenu().setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);

			AppState.twoPaneMode = false;
			getSupportActionBar().setSubtitle(AppState.selectedMenuOption.menuText);

            drawerIcon = new DrawerArrowDrawable(getResources());
            drawerIcon.setStrokeColor(0xFFFFFFFF);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(drawerIcon);
		}
		else
		{
			// Add a dummy view
			View v = new View(this);
			setBehindContentView(v);
			getSlidingMenu().setSlidingEnabled(false);
			getSlidingMenu().setTouchModeAbove(SlidingMenu.TOUCHMODE_NONE);

            AppState.twoPaneMode = true;
			getSupportActionBar().setSubtitle(null);

            drawerIcon = null;
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setIcon(R.drawable.titlebar_icon);
			
			new Handler().postDelayed(new Runnable()
			{
				public void run()
				{
					getSlidingMenu().showContent(false);
				}
			}, 100);	//Without this, there was a bug when, in onePaneMode portrait switching to twoPaneMode landscape, if the menu was open when the orientation changed, it would still be slid open in twoPaneMode, rather than in its always-open position.
		}
		
		if(getSupportFragmentManager().findFragmentById(R.id.main_content_frame) == null)
		{
			// Set the content fragment
			getSupportFragmentManager().beginTransaction().add(R.id.main_content_frame, AppState.contentFragment).commit();

			// Set the menu fragment
			getSupportFragmentManager().beginTransaction().add(R.id.main_menu_frame, AppState.menuFragment).commit();
		}
		
		// Customize the SlidingMenu
		SlidingMenu sm = getSlidingMenu();
		sm.setBehindOffsetRes(R.dimen.slidingmenu_offset);
		sm.setBehindScrollScale(0.25f);
		sm.setFadeDegree(0.25f);
        sm.setOnSlideListener(this);
		setSlidingActionBarEnabled(false);
	}
	
	@Override
	protected void onDestroy()
	{
		super.onDestroy();
		AppState.shutdown();
	}
	
	@Override
	public void onStart()
	{
		super.onStart();
		
		if(AppState.currentLocation == null || !AppData.isSavedLocationCurrent())
			determineLocation(false, true);
	}

    @Override
    public void onResume()
    {
        super.onResume();

        //Whenever app starts/resumes, ask Google for in-app purchase info
        AppState.loadIabInventory();
    }
	
	@Override
	public void onStop()
	{
		super.onStop();
		
		AppState.historyStack.clear();
		
		if(googleApiClient != null)
			googleApiClient.disconnect();
	}
	
	@Override
	public void onBackPressed()
	{
		if(AppState.contentFragment != null && AppState.contentFragment.handleBackButton())
			return;
		
		if(!AppState.historyStack.isEmpty())
		{
			AppState.goTo(AppState.historyStack.pop(), false);
			return;
		}
		
		super.onBackPressed();
	}

	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event)
	{
		if(keyCode == KeyEvent.KEYCODE_MENU)
		{
			toggle();
			return true;
		}

        return super.onKeyUp(keyCode, event);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch(item.getItemId())
		{
			case android.R.id.home:
				toggle();	//Show the sliding menu when user presses the app icon
		}
		return super.onOptionsItemSelected(item);
	}

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == AppState.purchaseRequestCode && AppState.contentFragment instanceof PurchaseAware)
            ((PurchaseAware)AppState.contentFragment).onPurchaseResponse(requestCode, resultCode, data);    //We have to pass this through in order to get it to the IabHelper
    }

    private void determineLocation(boolean forceGeo, boolean allowPlayPopup)
	{
		SaucerLocationDTO curLocation = null;
		
		if(!forceGeo)
			curLocation = AppData.getSavedLocation();
		
		if(curLocation == null)
		{
			Log.d(AppState.logTag, "Attempting to acquire a location...");
			if(AppState.isGooglePlayAvailable(allowPlayPopup))
			{
				locationRetryCount = 0;
				acquiringSpinner = null;

                googleApiClient = new GoogleApiClient.Builder(this).addApi(LocationServices.API).addConnectionCallbacks(this).addOnConnectionFailedListener(this).build();
                googleApiClient.connect();
			}
			else
				onGeoLocationFailed(false);
		}
		else
			AppState.propagateNewLocation(curLocation, AppData.isSavedLocationAutomatic());
	}
	
	private void onGeoLocationFailed(boolean noPermission)
	{
		Log.w(AppState.logTag, "Giving up on geoLocation!");
		
		if(acquiringSpinner != null && acquiringSpinner.isShowing())
			acquiringSpinner.dismiss();
		
		final SaucerLocationDTO lastLoc = AppData.getSavedLocation(true);
		if (noPermission) {
			//Location failed because user didn't give us permission. Explain to them and then force them to pick a location manually
			Log.w(AppState.logTag, "No permission to determine location. Prompting user to pick manual location.");
			AlertDialog.Builder dialog = new AlertDialog.Builder(this);
			dialog.setTitle(getResources().getString(R.string.locationError));
			dialog.setMessage(getResources().getString(R.string.noLocationNoPermission));
			dialog.setPositiveButton("Choose Location", new OnClickListener()
			{
				@Override
				public void onClick(DialogInterface dialog, int which)
				{
					new LocationPrompt(MainActivity.this).show(false, false);
				}
			});
			dialog.setCancelable(false);
			dialog.show();
		}
		else if(lastLoc != null)
		{
			//Location failed for some reason, but we have a last known location. Prompt to retry or use the last known location
			Log.w(AppState.logTag, "Prompting user to retry or use last known location.");
			AlertDialog.Builder dialog = new AlertDialog.Builder(this);
			dialog.setTitle(getResources().getString(R.string.locationError));
			dialog.setMessage(getResources().getString(R.string.noLocationRetryCancel));
			dialog.setPositiveButton("Retry", new OnClickListener()
			{
				@Override
				public void onClick(DialogInterface dialog, int which)
				{
					determineLocation(true, true);
				}
			});
			dialog.setNegativeButton("Use Last Location", new OnClickListener()
			{
				@Override
				public void onClick(DialogInterface dialog, int which)
				{
					AppState.propagateNewLocation(lastLoc, AppData.isSavedLocationAutomatic());
				}
			});
            dialog.setCancelable(false);
			dialog.show();
		}
		else
		{
			//Location failed for some reason and we DON'T have a last known location. Prompt to retry or pick a location manually
			Log.w(AppState.logTag, "Prompting user to retry or pick manual location.");
			AlertDialog.Builder dialog = new AlertDialog.Builder(this);
			dialog.setTitle(getResources().getString(R.string.locationError));
			dialog.setMessage(getResources().getString(R.string.noLocationRetryChoose));
			dialog.setPositiveButton("Retry", new OnClickListener()
			{
				@Override
				public void onClick(DialogInterface dialog, int which)
				{
					determineLocation(true, true);
				}
			});
			dialog.setNegativeButton("Choose Location", new OnClickListener()
			{
				@Override
				public void onClick(DialogInterface dialog, int which)
				{
					new LocationPrompt(MainActivity.this).show(false, false);
				}
			});
			dialog.setCancelable(false);
			dialog.show();
		}
	}
	
	public void switchContent(final MenuOption newSelection, boolean evenIfItsAlreadyThere)
	{
		if(evenIfItsAlreadyThere || AppState.selectedMenuOption != newSelection)
		{
			try
			{
				AppState.contentFragment = newSelection.contentFragmentClass.newInstance();
				
				if(!AppState.twoPaneMode)
				{
					getSupportActionBar().setSubtitle(newSelection.menuText);
					AppState.setMenuTouchMode(true);	//Re-enable sliding menu, in case the current content has it disallowed
				}
			}
			catch(Exception e)
			{
				Log.e(AppState.logTag, "Exception in switchContent()", e);
				return;
			}
			
			getSupportFragmentManager().beginTransaction().replace(R.id.main_content_frame, AppState.contentFragment).commit();
		}
		
		new Handler().postDelayed(new Runnable()
		{
			public void run()
			{
				getSlidingMenu().showContent();
			}
		}, 100);	//Close the sliding menu once the new content fragment is loaded
	}

    @Override
    public void onSlide(float percentOpen)
    {
        if(drawerIcon == null)
            return;

        if(percentOpen == 1)
            drawerIcon.setFlip(true);
        else if(percentOpen == 0)
            drawerIcon.setFlip(false);

        drawerIcon.setParameter(percentOpen);
    }
	
	//Below here is stuff for PlayServices connection
	@Override
	public void onConnected(Bundle dataBundle)
	{
		Log.d(AppState.logTag, "Google Play Services connected");

		if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED)
		{
			Location lastLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
			if(lastLocation != null)
			{
				Log.d(AppState.logTag, "Location acquired!");
				Log.d(AppState.logTag, "Latitude: " + lastLocation.getLatitude());
				Log.d(AppState.logTag, "Longitude: " + lastLocation.getLongitude());
				Log.d(AppState.logTag, "Accuracy:" + lastLocation.getAccuracy());

				SaucerLocationDTO newLocation = new LocationFinder().determineClosestLocation(lastLocation);
				AppState.propagateNewLocation(newLocation, true);
				AppData.setSavedLocation(newLocation, true);

				googleApiClient.disconnect();

				if(acquiringSpinner != null)
					acquiringSpinner.dismiss();
			}
			else
			{
				if(++locationRetryCount <= 10)
				{
					if(locationRetryCount == 1)
					{
						acquiringSpinner = new ProgressDialog(this, ProgressDialog.STYLE_SPINNER);
						acquiringSpinner.setMessage(getResources().getString(R.string.findingClosestLocation));
						acquiringSpinner.setCancelable(false);
						acquiringSpinner.setCanceledOnTouchOutside(false);
						acquiringSpinner.show();
					}

					Log.w(AppState.logTag, "Location acquired but it was null! Retrying " + locationRetryCount + " of 10...");
					new LocationRetryDelayTask().execute();
				}
				else
				{
					Log.w(AppState.logTag, "Location acquired but it was null! Done retrying.");
					googleApiClient.disconnect();
					onGeoLocationFailed(false);
				}
			}
		}
		else
		{
			//User has not given us permission to use geolocation.
			onGeoLocationFailed(true);
		}
	}
	
	@Override
	public void onConnectionSuspended(int i)
	{
		Log.d(AppState.logTag, "Google Play Services disconnected");
	}
	
	@Override
	public void onConnectionFailed(ConnectionResult connectionResult)
	{
		Log.w(AppState.logTag, "Google Play Services connection failed!");
		onGeoLocationFailed(false);
	}

	@Override
	public void onNewLocation(SaucerLocationDTO location, boolean wasSetAutomatically)
	{
		if(location == null && wasSetAutomatically)	//User just chose to use auto location. Let's try to find out where they are
			determineLocation(true, true);
		else
		{
			if(wasSetAutomatically && (AppData.getSavedLocation(true) != location || !AppData.isSavedLocationAutomatic()))	//If the new location was set automatically, and it doesn't match the old location OR the old location wasn't automatic, tell the user about the new location.
				Toast.makeText(this, "Location set to " + location.displayName, Toast.LENGTH_LONG).show();
			
			new Handler().postDelayed(new Runnable()
			{
				public void run()
				{
					getSlidingMenu().showContent();
				}
			}, 100);	//Close the sliding menu once the location has been set.
		}
	}
	
	private class LocationRetryDelayTask extends AsyncTask<Void, Void, Void>
	{	
		@Override
		protected Void doInBackground(Void... params)
		{
			try{Thread.sleep(1000);}catch(Exception e){}
			return null;
		}
		
		@Override
		protected void onPostExecute(Void nothing)
		{
			if(googleApiClient != null)
			{
				googleApiClient.disconnect();
                googleApiClient = new GoogleApiClient.Builder(MainActivity.this).addApi(LocationServices.API).addConnectionCallbacks(MainActivity.this).addOnConnectionFailedListener(MainActivity.this).build();
				googleApiClient.connect();
			}
		}
	}
}