package com.mellowelephant.saucer.async;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.mellowelephant.saucer.BackendAPI;
import com.mellowelephant.saucer.data.AppData;
import com.mellowelephant.saucer.util.AppState;

public class UnRegisterForNotificationsTask extends AsyncTask<Void, Void, Void>
{	
	private BackendAPI api = new BackendAPI();
	
	@Override
	protected Void doInBackground(Void... params)
	{
		try
		{
			Log.d(AppState.logTag, "UnRegisterForNotificationsTask: Starting");
			Context appContext = AppState.context;
			Long memberId = AppData.getNotificationsMbrId();
			String deviceId = AppData.getDeviceId(appContext);
			
			//1. Unregister for GCM
			Log.d(AppState.logTag, "UnRegisterForNotificationsTask: GCM Unregister...");
			GoogleCloudMessaging.getInstance(appContext).unregister();
			
			//2. Send RegKey to our backend
			Log.d(AppState.logTag, "UnRegisterForNotificationsTask: API Unregister...");
			//api.unregisterDeviceForNotifications(memberId, deviceId);
			//TODO Re-implement
			
			//3. Save RegKey to our appdata
			Log.d(AppState.logTag, "UnRegisterForNotificationsTask: Clearing key...");
			AppData.setNotificationsKey(null, null);
			
			Log.d(AppState.logTag, "UnRegisterForNotificationsTask: Success");
		}
		catch(Exception e)
		{
			Log.e(AppState.logTag, "Exception while unregistering for notifications", e);
		}
		finally
		{
			//4. Unset the busy flag
			AppData.setNotificationsBusy(false);
		}
		
		return null;
	}
	
	@Override
	protected void onPostExecute(Void nothing){}
}