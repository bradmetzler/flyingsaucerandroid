package com.mellowelephant.saucer.async;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import com.mellowelephant.saucer.BackendAPI;
import com.mellowelephant.saucer.callback.FoodMenuListener;
import com.mellowelephant.saucer.dto.SaucerLocationDTO;
import com.mellowelephant.saucer.util.AppState;

public class RetrieveFoodMenuTask extends AsyncTask<FoodMenuListener, Void, Bitmap>
{
	public volatile FoodMenuListener callback;

	private SaucerLocationDTO location;
	private BackendAPI api = new BackendAPI();

	public RetrieveFoodMenuTask(SaucerLocationDTO location)
	{
		this.location = location;
	}

	public SaucerLocationDTO getLocation()
	{
		return location;
	}

	@Override
	protected Bitmap doInBackground(FoodMenuListener... params)
	{
		callback = params[0];

		String menuUrl = null;
		Bitmap foodMenu = null;
		InputStream in = null;

		try
		{
			Log.d(AppState.logTag, "Getting menu image URL...");
			menuUrl = null; //api.getMenuByLocation(location);
			//TODO Re-implement
			Log.d(AppState.logTag, "Got menu image URL: " + menuUrl);
			
			Log.d(AppState.logTag, "Downloading menu image...");
			in = new URL(menuUrl).openStream();
			foodMenu = BitmapFactory.decodeStream(in);
			Log.d(AppState.logTag, "Got menu image with dimensions " + foodMenu.getWidth() + "x" + foodMenu.getHeight());
		}
		catch(Exception e)
		{
			Log.e(AppState.logTag, "Could not load FoodMenu Bitmap from: " + menuUrl);
		}
		finally
		{
			if(in != null){try{in.close();}catch(IOException e){}}
		}

		return foodMenu;
	}

	@Override
	protected void onPostExecute(Bitmap foodMenu)
	{
		if(callback != null)
		{
			if(foodMenu != null)
				callback.onFoodMenuRetrieved(foodMenu);
			else
				callback.onFoodMenuError();
		}
	}
}
