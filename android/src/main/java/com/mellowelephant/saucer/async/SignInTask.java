package com.mellowelephant.saucer.async;

import android.os.AsyncTask;

import com.mellowelephant.saucer.BackendAPI;
import com.mellowelephant.saucer.callback.SignInListener;
import com.mellowelephant.saucer.data.AppData;
import com.mellowelephant.saucer.dto.PlatformDTO;
import com.mellowelephant.saucer.dto.SaucerLocationDTO;
import com.mellowelephant.saucer.dto.UserDTO;
import com.mellowelephant.saucer.exception.AuthenticationException;

public class SignInTask extends AsyncTask<SignInListener, Void, UserDTO>
{
	public volatile SignInListener callback;
	
	private BackendAPI api = new BackendAPI();
	
	private String ufoNumber;
	private String password;
	private SaucerLocationDTO location;
	private boolean isMou;
	
	public SignInTask(String ufoNumber, String password, SaucerLocationDTO location, boolean isMou)
	{
		this.ufoNumber = ufoNumber;
		this.password = password;
		this.location = location;
		this.isMou = isMou;
	}
	
	@Override
	protected UserDTO doInBackground(SignInListener... params)
	{
		callback = params[0];
		
		try
		{
			return api.loginUser(ufoNumber, password, location, isMou, PlatformDTO.ANDROID.name(), AppData.getDeviceId());
		}
		catch(AuthenticationException ae)
		{
			return null;
		}
		catch(Exception e)
		{
			return null;	//TODO May want to handle this differently. If we leave it like this, user gets the same error for invalid password as they would for server being down, etc
		}
	}
	
	@Override
	protected void onPostExecute(UserDTO loggedInUser)
	{
		if(callback != null)
		{
			if(loggedInUser != null)
				callback.onLoginSuccessful(loggedInUser);
			else
				callback.onLoginFailure();
		}
	}
}
