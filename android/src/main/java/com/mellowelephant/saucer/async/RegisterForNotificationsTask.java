package com.mellowelephant.saucer.async;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.mellowelephant.saucer.BackendAPI;
import com.mellowelephant.saucer.data.AppData;
import com.mellowelephant.saucer.util.AppState;

public class RegisterForNotificationsTask extends AsyncTask<Void, Void, Void>
{	
	private BackendAPI api = new BackendAPI();
	
	@Override
	protected Void doInBackground(Void... params)
	{
		try
		{
			Log.d(AppState.logTag, "RegisterForNotificationsTask: Starting");
			Context appContext = AppState.context;
			Long memberId = AppData.getLoggedInUser(appContext).getId();
			String deviceId = AppData.getDeviceId(appContext);
			
			//1. Register for GCM
			Log.d(AppState.logTag, "RegisterForNotificationsTask: GCM Register... " + BackendAPI.GCM_SENDER_ID);
			String regKey = GoogleCloudMessaging.getInstance(appContext).register(BackendAPI.GCM_SENDER_ID);
			
			//2. Send RegKey to our backend
			Log.d(AppState.logTag, "RegisterForNotificationsTask: API Register...");
			//api.registerDeviceForNotifications(memberId, deviceId, regKey);
			//TODO Re-implement notifications
			
			//3. Save RegKey to our appdata
			Log.d(AppState.logTag, "RegisterForNotificationsTask: Saving key...");
			AppData.setNotificationsKey(regKey, memberId);
			
			Log.d(AppState.logTag, "RegisterForNotificationsTask: Success");
		}
		catch(Exception e)
		{
			Log.e(AppState.logTag, "Exception while registering for notifications", e);
		}
		finally
		{
			//4. Unset the busy flag
			AppData.setNotificationsBusy(false);
		}
		
		return null;
	}
	
	@Override
	protected void onPostExecute(Void nothing){}
}
