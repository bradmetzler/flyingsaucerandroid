package com.mellowelephant.saucer.async;

import android.os.AsyncTask;
import android.util.Log;

import com.mellowelephant.saucer.BackendAPI;
import com.mellowelephant.saucer.callback.BeerBuddyActionListener;
import com.mellowelephant.saucer.data.AppData;
import com.mellowelephant.saucer.dto.BuddyDTO;
import com.mellowelephant.saucer.dto.UserDTO;
import com.mellowelephant.saucer.exception.AuthenticationException;
import com.mellowelephant.saucer.util.AppState;

import java.util.List;

public class InviteBuddyTask extends AsyncTask<BeerBuddyActionListener, Void, Boolean>
{
	public volatile BeerBuddyActionListener callback;
	
	private UserDTO loggedInUser;
	private String cardNumber;
	private String dob;
	
	private BackendAPI api = new BackendAPI();
	
	public InviteBuddyTask(UserDTO loggedInUser, String cardNumber, String dob)
	{
		this.loggedInUser = loggedInUser;
		this.cardNumber = cardNumber;
		this.dob = dob;
	}
	
	public UserDTO getLoggedInUser()
	{
		return loggedInUser;
	}
	
	@Override
	protected Boolean doInBackground(BeerBuddyActionListener... params)
	{
		callback = params[0];
		
		try
		{
			if(loggedInUser == null)
				return null;
			else
			{
				Log.d(AppState.logTag, "Sending invite for card number " + cardNumber + ", and dob " + dob);
				
				BuddyDTO buddy = null; //api.addBuddy(loggedInUser.getId(), AppData.getDeviceId(), cardNumber, dob);
				if(buddy != null)
				{
					List<BuddyDTO> cachedBuddies = AppData.getValidCachedBeerBuddyList();
					
					if(cachedBuddies != null)
					{
						Log.d(AppState.logTag, "Updating cached buddy list with new info for memberId " + buddy.getMemberId());
						
						boolean found = false;
						for(BuddyDTO cachedBuddy : cachedBuddies)
						{
							if(cachedBuddy.getMemberId().longValue() == buddy.getMemberId().longValue())
							{
								Log.w(AppState.logTag, "Found existing cached record for this pendingBuddyMessage. memberId " + buddy.getMemberId());
								
								found = true;
								cachedBuddy.setStatus(buddy.getStatus());
								break;
							}
						}
						
						if(!found)
							cachedBuddies.add(buddy);
						
						AppData.updateBeerBuddyListInCache(cachedBuddies);
					}
					
					return true;
				}
				else
					return false;
			}
		}
		catch(AuthenticationException ae)
		{
			return null;
		}
		catch(Exception e)
		{
			Log.e(AppState.logTag, "Exception caught in RetrieveBeerBuddyListTask!", e);
		}
		
		return false;
	}
	
	@Override
	protected void onPostExecute(Boolean success)
	{
		if(callback != null)
		{
			if(success == null)
				callback.onInvalidCredentials();
			else if(success)
				callback.onBeerBuddyActionSuccess();
			else
				callback.onBeerBuddyActionError();
		}
	}
}
