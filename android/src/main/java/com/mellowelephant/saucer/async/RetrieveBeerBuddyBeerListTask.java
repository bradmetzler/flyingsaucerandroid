package com.mellowelephant.saucer.async;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.mellowelephant.saucer.BackendAPI;
import com.mellowelephant.saucer.callback.BeerListListener;
import com.mellowelephant.saucer.data.SQLData;
import com.mellowelephant.saucer.dto.BeerDTO;
import com.mellowelephant.saucer.dto.BuddyBeersDTO;
import com.mellowelephant.saucer.dto.SaucerLocationDTO;
import com.mellowelephant.saucer.dto.TTLResponse;
import com.mellowelephant.saucer.dto.UserDTO;
import com.mellowelephant.saucer.exception.AuthenticationException;
import com.mellowelephant.saucer.util.AppState;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class RetrieveBeerBuddyBeerListTask extends AsyncTask<BeerListListener, Void, TTLResponse<List<BeerDTO>>>
{
	public volatile BeerListListener callback;
	
	private Context context;
	private SaucerLocationDTO location;
	private UserDTO loggedInUser;

	
	private BackendAPI api = new BackendAPI();
	private SQLData sqlData;
	
	public RetrieveBeerBuddyBeerListTask(Context context, SaucerLocationDTO location, UserDTO loggedInUser)
	{
		this.context = context;
		this.location = location;
		this.loggedInUser = loggedInUser;
		
		this.sqlData = new SQLData(context);
	}
	
	public UserDTO getLoggedInUser()
	{
		return loggedInUser;
	}
	
	@Override
	protected TTLResponse<List<BeerDTO>> doInBackground(BeerListListener... params)	//We really just need List<Beer> here, but wrapping it in TTLResponse lets us differentiate between AuthException and general errors easily, like we do in other AsyncTasks
	{
		callback = params[0];
		
		try
		{
			//Get user and buddy beer lists (and save to cache if we didn't already have them)
			List<BeerDTO> userBeers = retrieveUserBeers();
			List<BeerDTO> buddyBeers = retrieveBuddyBeers();
			
			//Merge beer lists
			return new TTLResponse<List<BeerDTO>>(mergeBeerLists(userBeers, buddyBeers), 0);
		}
		catch(AuthenticationException ae)
		{
			return null;
		}
		catch(Exception e)
		{
			Log.e(AppState.logTag, "Exception caught in RetrieveBeerBuddyBeerListTask!", e);
			return new TTLResponse<List<BeerDTO>>(null, 0);
		}
	}
	
	@Override
	protected void onPostExecute(TTLResponse<List<BeerDTO>> beers)
	{
		if(callback != null)
		{
			if(beers == null)
				callback.onInvalidCredentials();
			else if(beers.getPayload() == null)
				callback.onBeerListError();
			else	
				callback.onBeerListRetrieved(beers.getPayload());
		}
	}
	
	private List<BeerDTO> retrieveUserBeers() throws Exception
	{
		Log.d(AppState.logTag, "RetrieveBeerBuddyBeerListTask couldn't find cached beer list. Calling server for a new one.");
		TTLResponse<List<BeerDTO>> beersFromServer = new RetrieveBeerListTask(context, location, loggedInUser).doInBackground((BeerListListener)null);
		
		if(beersFromServer == null)
			throw new AuthenticationException();
		else if(beersFromServer.getPayload() == null || beersFromServer.getPayload().isEmpty())
			throw new Exception();	//This will trigger code in doInBackground and onPostExecute that will call the error callback.
		
		return beersFromServer.getPayload();
	}
	
	private List<BeerDTO> retrieveBuddyBeers() throws Exception
	{
		List<BeerDTO> beers = sqlData.getCachedBeerBuddyBeerList();
		if(beers == null)
		{
			Log.d(AppState.logTag, "RetrieveBeerBuddyBeerListTask couldn't find cached buddy beer list. Calling server for a new one.");
			TTLResponse<BuddyBeersDTO> beersFromServer = null; //api.getBeersForBuddies(loggedInUser.getId(), AppData.getDeviceId());
			//TODO Re-implement
			
			if(beersFromServer.getPayload() == null)
				throw new Exception();	//This will trigger code in doInBackground and onPostExecute that will call the error callback.
			
			sqlData.saveBeerBuddyBeerListToCache(beersFromServer);
			beers = beersFromServer.getPayload().getBeers();
		}
		
		return beers;
	}
	
	@SuppressLint("UseSparseArrays")
	private List<BeerDTO> mergeBeerLists(List<BeerDTO> userBeers, List<BeerDTO> buddyBeers)
	{
		Map<Long, BeerDTO> mergedBeers = new HashMap<>();
		
		//Put all user beers in map
		if(userBeers != null)
		{
			for(BeerDTO userBeer : userBeers)
				mergedBeers.put(Long.valueOf(userBeer.getId()), userBeer);
		}
		
		//Supplement user beers with buddy data (and add buddy beers if they aren't in map already)
		if(buddyBeers != null)
		{
			for(BeerDTO buddyBeer : buddyBeers)
			{
				Long key = Long.valueOf(buddyBeer.getId());
				if(mergedBeers.containsKey(key))
					mergedBeers.get(key).setBuddies(buddyBeer.getBuddies());
				else
					mergedBeers.put(key, buddyBeer);
			}
		}
		
		//Remove any beers that haven't been tasted by anyone
		List<BeerDTO> beers = new ArrayList<BeerDTO>(mergedBeers.values());
		Iterator<BeerDTO> beerIter = beers.iterator();
		while(beerIter.hasNext())
		{
			BeerDTO beer = beerIter.next();
			
			if(!beer.hasBeenConsumed() && (beer.getBuddies() == null || beer.getBuddies().isEmpty()))
				beerIter.remove();
		}

		return beers;
	}
}
