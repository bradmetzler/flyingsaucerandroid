package com.mellowelephant.saucer.async;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.mellowelephant.saucer.BackendAPI;
import com.mellowelephant.saucer.callback.BeerListListener;
import com.mellowelephant.saucer.data.AppData;
import com.mellowelephant.saucer.data.SQLData;
import com.mellowelephant.saucer.dto.BeerDTO;
import com.mellowelephant.saucer.dto.SaucerLocationDTO;
import com.mellowelephant.saucer.dto.TTLResponse;
import com.mellowelephant.saucer.dto.UserDTO;
import com.mellowelephant.saucer.exception.AuthenticationException;
import com.mellowelephant.saucer.thread.SubmitQueues;
import com.mellowelephant.saucer.util.AppState;

import java.util.List;

public class RetrieveBeerListTask extends AsyncTask<BeerListListener, Void, TTLResponse<List<BeerDTO>>>
{
	public volatile BeerListListener callback;
	
	private SaucerLocationDTO location;
	private UserDTO loggedInUser;
	
	private BackendAPI api = new BackendAPI();
	private SQLData sqlData;
	
	public RetrieveBeerListTask(Context context, SaucerLocationDTO location, UserDTO loggedInUser)
	{
		this.location = location;
		this.loggedInUser = loggedInUser;
		
		this.sqlData = new SQLData(context);
	}
	
	public SaucerLocationDTO getLocation()
	{
		return location;
	}
	public UserDTO getLoggedInUser()
	{
		return loggedInUser;
	}
	
	@Override
	public TTLResponse<List<BeerDTO>> doInBackground(BeerListListener... params)	//Changing scope to public because we need to call this from RetrieveBeerBuddyBeerListTask
	{
		if(params != null)
			callback = params[0];
		
		try
		{
			List<BeerDTO> beers = sqlData.getCachedBeerList(location);
			if(beers != null)
				return new TTLResponse<List<BeerDTO>>(beers, -1);	//-1 tells onPostExecute() to not save the list to cache (because it's already in cache)
			
			if(loggedInUser == null)
				return api.getBeersByLocation(location);
			else
			{
				boolean failedSends = new SubmitQueues(api, AppData.getSQLData(), loggedInUser.getId(), AppData.getDeviceId()).submit();	//Check to see if there are any pending beers/comments/ratings we need to send to the server. If there are, send them first.
				if(failedSends)
					throw new Exception();
				
				return api.getUserBeersByLocation(location, loggedInUser.getId(), AppData.getDeviceId());
			}
		}
		catch(AuthenticationException ae)
		{
			return null;
		}
		catch(Exception e)
		{
			Log.e(AppState.logTag, "Exception caught in RetrieveBeerListTask!", e);
			return new TTLResponse<List<BeerDTO>>(null, 0);
		}
	}
	
	@Override
	protected void onPostExecute(TTLResponse<List<BeerDTO>> beers)
	{
		if(callback != null)
		{
			if(beers == null)
				callback.onInvalidCredentials();
			else if(beers.getPayload() == null || beers.getPayload().isEmpty())
				callback.onBeerListError();
			else
			{
				if(beers.getTimeToLive() > 0)
					AppData.saveBeerListToCache(location, beers);
				
				callback.onBeerListRetrieved(beers.getPayload());
			}
		}
	}
}
