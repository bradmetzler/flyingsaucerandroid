package com.mellowelephant.saucer.async;

import android.os.AsyncTask;
import android.util.Log;

import com.mellowelephant.saucer.BackendAPI;
import com.mellowelephant.saucer.callback.BeerBuddyActionListener;
import com.mellowelephant.saucer.data.AppData;
import com.mellowelephant.saucer.dto.BuddyDTO;
import com.mellowelephant.saucer.dto.UserDTO;
import com.mellowelephant.saucer.exception.AuthenticationException;
import com.mellowelephant.saucer.util.AppState;

import java.util.List;

public class RemoveBuddyTask extends AsyncTask<BeerBuddyActionListener, Void, Boolean>
{
	public volatile BeerBuddyActionListener callback;
	
	private UserDTO loggedInUser;
	private Long removeMemberId;
	
	private BackendAPI api = new BackendAPI();
	
	public RemoveBuddyTask(UserDTO loggedInUser, Long removeMemberId)
	{
		this.loggedInUser = loggedInUser;
		this.removeMemberId = removeMemberId;
	}
	
	public UserDTO getLoggedInUser()
	{
		return loggedInUser;
	}
	
	@Override
	protected Boolean doInBackground(BeerBuddyActionListener... params)
	{
		callback = params[0];
		
		try
		{
			if(loggedInUser == null)
				return null;
			else
			{
				Log.d(AppState.logTag, "Sending buddy removal memberId " + removeMemberId);
				
				//api.removeBuddy(loggedInUser.getMemberId(), AppData.getDeviceId(), removeMemberId);
				//TODO Re-implement
				
				List<BuddyDTO> cachedBuddies = AppData.getValidCachedBeerBuddyList();
				if(cachedBuddies != null)
				{
					Log.d(AppState.logTag, "Updating cached buddy list to remove " + removeMemberId);
					
					boolean found = false;
					for(BuddyDTO cachedBuddy : cachedBuddies)
					{
						if(cachedBuddy.getMemberId().longValue() == removeMemberId.longValue())
						{
							found = true;
							cachedBuddies.remove(cachedBuddy);
							break;
						}
					}
					
					if(found)
						AppData.updateBeerBuddyListInCache(cachedBuddies);
					else
						Log.w(AppState.logTag, "Did not find buddy to be deleted in the cached beer list: " + removeMemberId);
				}
				
				return true;
			}
		}
		catch(AuthenticationException ae)
		{
			return null;
		}
		catch(Exception e)
		{
			Log.e(AppState.logTag, "Exception caught in RetrieveBeerBuddyListTask!", e);
		}
		
		return false;
	}
	
	@Override
	protected void onPostExecute(Boolean success)
	{
		if(callback != null)
		{
			if(success == null)
				callback.onInvalidCredentials();
			else if(success)
				callback.onBeerBuddyActionSuccess();
			else
				callback.onBeerBuddyActionError();
		}
	}
}
