package com.mellowelephant.saucer.async;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.mellowelephant.saucer.BackendAPI;
import com.mellowelephant.saucer.callback.BeerBuddyListListener;
import com.mellowelephant.saucer.data.AppData;
import com.mellowelephant.saucer.data.SQLData;
import com.mellowelephant.saucer.dto.BuddyDTO;
import com.mellowelephant.saucer.dto.TTLResponse;
import com.mellowelephant.saucer.dto.UserDTO;
import com.mellowelephant.saucer.exception.AuthenticationException;
import com.mellowelephant.saucer.util.AppState;

import java.util.List;

public class RetrieveBeerBuddyListTask extends AsyncTask<BeerBuddyListListener, Void, TTLResponse<List<BuddyDTO>>>
{
	public volatile BeerBuddyListListener callback;
	
	private UserDTO loggedInUser;
	
	private BackendAPI api = new BackendAPI();
	private SQLData sqlData;
	
	public RetrieveBeerBuddyListTask(Context context, UserDTO loggedInUser)
	{
		this.loggedInUser = loggedInUser;
		
		this.sqlData = new SQLData(context);
	}
	
	public UserDTO getLoggedInUser()
	{
		return loggedInUser;
	}
	
	@Override
	protected TTLResponse<List<BuddyDTO>> doInBackground(BeerBuddyListListener... params)
	{
		callback = params[0];
		
		try
		{
			List<BuddyDTO> buddies = sqlData.getCachedBeerBuddyList();
			if(buddies != null)
				return new TTLResponse<List<BuddyDTO>>(buddies, -1);	//-1 tells onPostExecute() to not save the list to cache (because it's already in cache)
			
			if(loggedInUser == null)
				return null;
			else
				//return new TTLResponse<List<BuddyDTO>>(api.getBuddyList(loggedInUser.getId(), AppData.getDeviceId()).getBuddies(), 8 * 60 * 60 * 1000);	//Server doesn't send this back as a TTLResponse, because it doesn't have to periodically call to FS to update them. We will wrap it in a TTLResponse so that we can handle caching identically to beer list.
				//TODO Re-implement
				return null;
		}
		catch(AuthenticationException ae)
		{
			return null;
		}
		catch(Exception e)
		{
			Log.e(AppState.logTag, "Exception caught in RetrieveBeerBuddyListTask!", e);
			return new TTLResponse<List<BuddyDTO>>(null, 0);
		}
	}
	
	@Override
	protected void onPostExecute(TTLResponse<List<BuddyDTO>> buddies)
	{
		if(callback != null)
		{
			if(buddies == null)
				callback.onInvalidCredentials();
			else if(buddies.getPayload() == null)
				callback.onBeerBuddyListError();
			else
			{
				if(buddies.getTimeToLive() > 0)
					AppData.saveBeerBuddyListToCache(buddies);
				
				callback.onBeerBuddyListRetrieved(buddies.getPayload());
			}
		}
	}
}
