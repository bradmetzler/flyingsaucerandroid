package com.mellowelephant.saucer.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.TextUtils.TruncateAt;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mellowelephant.saucer.R;
import com.mellowelephant.saucer.dto.BeerDTO;
import com.mellowelephant.saucer.util.AppState;

import java.util.List;

public class BeerMenuArrayAdapter extends ArrayAdapter<BeerDTO>
{
	public BeerMenuArrayAdapter(Context context, List<BeerDTO> items)
	{
		super(context, android.R.layout.simple_list_item_1, items);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		BeerDTO beer = getItem(position);
		View view = super.getView(position, convertView, parent);
		
		TextView textView = (TextView) view.findViewById(android.R.id.text1);
		textView.setText(beer.getName());
		textView.setSingleLine();
		textView.setEllipsize(TruncateAt.END);
		
		if(AppState.isUserLoggedIn())
		{
			Drawable drawable = getContext().getResources().getDrawable(beer.hasBeenConsumed() ? R.drawable.icon_check : R.drawable.icon_blank);
			if(beer.getPendingLocation() != null)
				drawable.mutate().setAlpha(70);
			
			textView.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
			textView.setCompoundDrawablePadding((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 12, getContext().getResources().getDisplayMetrics()));
		}
		
		return view;
	}
}