package com.mellowelephant.saucer.adapter;

import java.util.List;
import java.util.Map.Entry;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.mellowelephant.saucer.R;

public class BeerFilterArrayAdapter extends ArrayAdapter<Entry<String, Integer>>
{
	private ListView listView;
	
	public BeerFilterArrayAdapter(Context context, List<Entry<String, Integer>> items, ListView listView)
	{
		super(context, R.layout.beerfilter_list_item, R.id.beerFilterListItemTitle, items);
		this.listView = listView;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		Entry<String, Integer> item = getItem(position);
		View view = super.getView(position, convertView, parent);
		
		view.setBackgroundColor(getContext().getResources().getColor(listView.isItemChecked(position) ? R.color.paleblue : android.R.color.transparent));
		
		((TextView)view.findViewById(R.id.beerFilterListItemTitle)).setText(item.getKey());
		((TextView)view.findViewById(R.id.beerFilterListItemCount)).setText("(" + item.getValue() + ")");
		
		return view;
	}
}