package com.mellowelephant.saucer.adapter;

import java.util.List;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mellowelephant.saucer.R;
import com.mellowelephant.saucer.dto.BuddyDTO;
import com.mellowelephant.saucer.dto.BuddyStatusDTO;
import com.mellowelephant.saucer.util.BuddyUtil;

public class DrinkingBuddyArrayAdapter extends ArrayAdapter<BuddyDTO>
{
	public DrinkingBuddyArrayAdapter(Context context, List<BuddyDTO> items)
	{
		super(context, R.layout.drinkingbuddylist_list_item, R.id.drinkingBuddiesBuddyName, items);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		BuddyDTO buddy = getItem(position);
		View view = super.getView(position, convertView, parent);
		
		String cardNumberText = "";
		switch(buddy.getStatus())
		{
			case ACCEPTED:
				cardNumberText = getContext().getResources().getString(R.string.drinkingBuddiesCardNumber) + " " + buddy.getUfoNumber().toString();
				break;
			case PENDING:
				cardNumberText = getContext().getResources().getString(R.string.drinkingBuddiesTapToRespond);
				break;
			case REQUESTED:
				cardNumberText = getContext().getResources().getString(R.string.drinkingBuddiesWaiting);
				break;
		}
		
		TextView colorSquare = (TextView)view.findViewById(R.id.drinkingBuddiesBuddyColorSquare);
		colorSquare.setBackgroundColor(BuddyUtil.getColorSquareColor(buddy));
		colorSquare.setText(BuddyUtil.getColorSquareText(buddy));
		
		((TextView)view.findViewById(R.id.drinkingBuddiesBuddyName)).setText(buddy.getFullName());
		((TextView)view.findViewById(R.id.drinkingBuddiesBuddyCardNumber)).setText(cardNumberText);
		((TextView)view.findViewById(R.id.drinkingBuddiesBuddyCardNumber)).setTypeface(null, buddy.getStatus() == BuddyStatusDTO.PENDING ? Typeface.BOLD : Typeface.NORMAL);
				
		view.setAlpha(buddy.getStatus() == BuddyStatusDTO.ACCEPTED ? 1f : 0.5f);

		return view;
	}
}