package com.mellowelephant.saucer.adapter;

import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mellowelephant.saucer.R;
import com.mellowelephant.saucer.adapter.BuddyRatingArrayAdapter.Container;
import com.mellowelephant.saucer.dto.BuddyBeerDTO;
import com.mellowelephant.saucer.dto.BuddyDTO;
import com.mellowelephant.saucer.util.BuddyUtil;
import com.mellowelephant.saucer.util.Formatter;

public class BuddyRatingArrayAdapter extends ArrayAdapter<Container>
{
	public BuddyRatingArrayAdapter(Context context, List<Container> items)
	{
		super(context, R.layout.buddyrating_list_item, R.id.buddyRatingBuddyName, items);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		Container container = getItem(position);
		BuddyDTO buddy = container.buddy;
		BuddyBeerDTO buddyBeer = container.buddyBeer;
		
		View view = super.getView(position, convertView, parent);
		
		TextView colorSquare = (TextView)view.findViewById(R.id.buddyRatingBuddyColorSquare);
		colorSquare.setBackgroundColor(buddy == null ? getContext().getResources().getColor(R.color.lightgray) : BuddyUtil.getColorSquareColor(buddy));
		colorSquare.setText(buddy == null ? "Me" : BuddyUtil.getColorSquareText(buddy));
		
		((TextView)view.findViewById(R.id.buddyRatingBuddyName)).setText(buddy == null ? "Me" : buddy.getFullName());
		
		if(buddyBeer == null)
		{
			//This buddy has not tasted this beer
			view.findViewById(R.id.buddyRatingRating).setVisibility(View.GONE);
			view.findViewById(R.id.buddyRatingDate).setVisibility(View.GONE);
			view.findViewById(R.id.buddyRatingComments).setVisibility(View.VISIBLE);
			
			((TextView)view.findViewById(R.id.buddyRatingComments)).setText(buddy == null ? getContext().getResources().getString(R.string.hasNotBeenTastedUser) : (BuddyUtil.getFirstName(buddy) + " " + getContext().getResources().getString(R.string.hasNotBeenTastedBuddy)));
		}
		else
		{
			//This buddy has tasted this beer
			
			//Rating
			int starDrawable = R.drawable.stars_small_0;
			if(buddyBeer.getBuddyRating() != null)
			{
				switch(buddyBeer.getBuddyRating())
				{
					case 1:
						starDrawable = R.drawable.stars_small_1;
						break;
					case 2:
						starDrawable = R.drawable.stars_small_2;
						break;
					case 3:
						starDrawable = R.drawable.stars_small_3;
						break;
					case 4:
						starDrawable = R.drawable.stars_small_4;
						break;
					case 5:
						starDrawable = R.drawable.stars_small_5;
						break;
				}
			}
			ImageView buddyRating = (ImageView)view.findViewById(R.id.buddyRatingRating);
			buddyRating.setVisibility(View.VISIBLE);
			buddyRating.setImageResource(starDrawable);
			
			//Date
			TextView buddyDate = (TextView)view.findViewById(R.id.buddyRatingDate);
			buddyDate.setVisibility(View.VISIBLE);
			buddyDate.setText(Formatter.formatForDisplay(buddyBeer.getDateConsumed()));
			
			//Comments
			if(buddy == null)
			{
				TextView comments = (TextView)view.findViewById(R.id.buddyRatingComments);
				
				comments.setVisibility(View.VISIBLE);
				comments.setText(getContext().getResources().getString(R.string.edit));
			}
			else if(buddyBeer.getComments() != null && !buddyBeer.getComments().isEmpty())
			{
				TextView comments = (TextView)view.findViewById(R.id.buddyRatingComments);
				
				comments.setVisibility(View.VISIBLE);
				comments.setText(getContext().getResources().getString(R.string.tapToViewComments));
			}
			else
				view.findViewById(R.id.buddyRatingComments).setVisibility(View.GONE);
		}

		return view;
	}
	
	public static class Container
	{
		public BuddyDTO buddy;
		public BuddyBeerDTO buddyBeer;
		
		public Container(){}
	}
}