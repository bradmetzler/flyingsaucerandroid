package com.mellowelephant.saucer.adapter;

import java.util.List;

import android.content.Context;
import android.graphics.Typeface;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.mellowelephant.saucer.R;
import com.mellowelephant.saucer.dto.BeerDTO;
import com.mellowelephant.saucer.dto.BuddyBeerDTO;
import com.mellowelephant.saucer.dto.BuddyDTO;
import com.mellowelephant.saucer.util.BuddyUtil;

public class BeerCompareArrayAdapter extends ArrayAdapter<BeerDTO>
{
	private List<BuddyDTO> buddies;
	
	public BeerCompareArrayAdapter(Context context, List<BeerDTO> items, List<BuddyDTO> buddies)
	{
		super(context, R.layout.beercompare_list_item, R.id.beerCompareListItemBeerName, items);
		
		this.buddies = buddies;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		BeerDTO beer = getItem(position);
		
		View view = super.getView(position, convertView, parent);
		((TextView)view.findViewById(R.id.beerCompareListItemBeerName)).setText(beer.getName());
		
		LinearLayout ratingContainer = (LinearLayout)view.findViewById(R.id.beerCompareListItemRatingContainer);
		ratingContainer.removeAllViews();
		ratingContainer.setWeightSum(buddies.size() + 1);
		
		ratingContainer.addView(buildRatingView(beer.hasBeenConsumed() ? ((beer.getRating() != null && beer.getRating().intValue() > 0) ? beer.getRating().toString() : "?") : null, getContext().getResources().getColor(R.color.contentdarkgray)));
		
		for(int i = 0; i < buddies.size(); i++)
		{
			BuddyDTO buddy = buddies.get(i);
			
			String rating = null;
			Long buddyMemberId = buddy.getMemberId();
			List<BuddyBeerDTO> buddyRatings = beer.getBuddies();
			if(buddyRatings != null)
			{
				for(BuddyBeerDTO buddyRating : buddyRatings)
				{
					if(buddyRating.getMemberId().longValue() == buddyMemberId.longValue())
					{
						Integer x = buddyRating.getBuddyRating();
						
						if(x == null)
							rating = "?";
						else
							rating = buddyRating.getBuddyRating().toString();
					}
				}
			}
			
			ratingContainer.addView(buildRatingView(rating, BuddyUtil.getFadedSquareColor(buddy)));
		}
		
		return view;
	}
	
	private View buildRatingView(String text, int color)
	{
        TextView textView = new TextView(this.getContext());
        textView.setLayoutParams(new LayoutParams(0, LayoutParams.WRAP_CONTENT, 1));
        textView.setText(text == null ? "-" : text);
        if(text == null)
            textView.setTextColor(getContext().getResources().getColor(R.color.palegray));
        textView.setTypeface(null, Typeface.BOLD);
        textView.setGravity(Gravity.CENTER);
        textView.setBackgroundColor(color);

        return textView;
	}
}