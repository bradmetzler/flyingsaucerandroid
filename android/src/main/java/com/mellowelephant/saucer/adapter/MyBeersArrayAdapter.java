package com.mellowelephant.saucer.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mellowelephant.saucer.R;
import com.mellowelephant.saucer.dto.BeerDTO;
import com.mellowelephant.saucer.util.Formatter;

import java.util.List;

public class MyBeersArrayAdapter extends ArrayAdapter<BeerDTO>
{
	public MyBeersArrayAdapter(Context context, List<BeerDTO> items)
	{
		super(context, R.layout.mybeers_list_item, R.id.myBeersListItemBeerName, items);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		BeerDTO beer = getItem(position);
		
		int starDrawable = R.drawable.stars_small_0;
		if(beer.getRating() != null)
		{
			switch(beer.getRating())
			{
				case 1:
					starDrawable = R.drawable.stars_small_1;
					break;
				case 2:
					starDrawable = R.drawable.stars_small_2;
					break;
				case 3:
					starDrawable = R.drawable.stars_small_3;
					break;
				case 4:
					starDrawable = R.drawable.stars_small_4;
					break;
				case 5:
					starDrawable = R.drawable.stars_small_5;
					break;
			}
		}
		
		View view = super.getView(position, convertView, parent);
		((TextView)view.findViewById(R.id.myBeersListItemBeerName)).setText(beer.getName());
		((TextView)view.findViewById(R.id.myBeersListItemDate)).setText(Formatter.formatForDisplay(beer.getDateConsumed()));
		((ImageView)view.findViewById(R.id.myBeersListItemRating)).setImageResource(starDrawable);
		
		boolean isPending = (beer.getPendingLocation() != null);
		view.findViewById(R.id.myBeersListItemPending).setVisibility(isPending ? View.VISIBLE : View.INVISIBLE);
		((TextView)view.findViewById(R.id.myBeersListItemBeerName)).setTypeface(Typeface.DEFAULT, isPending ? Typeface.ITALIC : Typeface.NORMAL);
		((TextView)view.findViewById(R.id.myBeersListItemDate)).setTypeface(Typeface.DEFAULT, isPending ? Typeface.ITALIC : Typeface.NORMAL);
		view.setAlpha(isPending ? 0.5f : 1f);
		
		return view;
	}
}