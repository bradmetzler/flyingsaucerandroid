package com.mellowelephant.saucer.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.mellowelephant.saucer.util.AppState;
import com.mellowelephant.saucer.util.MenuOption;

public class UILocalBroadcastReceiver extends BroadcastReceiver
{
	public static final String ACTION_NAME = "UILocalBroadcastReceiver";
	public static final String EXTRA_EVENTNAME = "UIBroadcastEvent";
    public static final String EXTRA_CONTENTTOREFRESH = "contentToRefresh";
	
	public enum UIBroadcastEvent
	{
		REFRESHCONTENT;
	}
	
	@Override
	public void onReceive(Context context, Intent intent)
	{
		try
		{
			Bundle extras = intent.getExtras();
			UIBroadcastEvent event = (UIBroadcastEvent)extras.getSerializable(EXTRA_EVENTNAME);
			
			switch(event)
			{
				case REFRESHCONTENT:
					refreshContent(extras);
					break;
			}
		}
		catch(Exception e)
		{
			Log.e(AppState.logTag, "Exception in UILocalBroadcastReceiver", e);
		}
	}
	
	private void refreshContent(Bundle extras) throws Exception
	{
		AppState.refreshSpecificFragment((MenuOption)extras.getSerializable(EXTRA_CONTENTTOREFRESH));
	}
}
