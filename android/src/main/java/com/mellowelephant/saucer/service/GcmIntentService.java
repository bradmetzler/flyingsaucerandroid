package com.mellowelephant.saucer.service;

import android.annotation.SuppressLint;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.mellowelephant.saucer.R;
import com.mellowelephant.saucer.activity.MainActivity;
import com.mellowelephant.saucer.data.SQLData;
import com.mellowelephant.saucer.dto.BuddyDTO;
import com.mellowelephant.saucer.dto.BuddyStatusDTO;
import com.mellowelephant.saucer.util.AppState;
import com.mellowelephant.saucer.util.MenuOption;

import java.text.SimpleDateFormat;
import java.util.List;

public class GcmIntentService extends IntentService
{
	private SQLData sqlData = null;
	
	public GcmIntentService()
	{
		super("GcmIntentService");
	}

	@Override
	protected void onHandleIntent(Intent intent)
	{
		try
		{
			Log.d(AppState.logTag, "GcmIntentService onHandleIntent()");

			Bundle extras = intent.getExtras();
			String messageType = GoogleCloudMessaging.getInstance(this).getMessageType(intent);
			
			if(GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType))
			{
				String type = extras.getString("type");
				if("pendingBuddy".equalsIgnoreCase(type))
					handlePendingBuddyMessage(extras);
				else if("acceptBuddy".equalsIgnoreCase(type))
					handleAcceptBuddyMessage(extras);
				else if("removeBuddy".equalsIgnoreCase(type))
					handleRemoveBuddyMessage(extras);
			}
		}
		catch(Exception e)
		{
			Log.e(AppState.logTag, "Exception while processing GCM message", e);
		}
		finally
		{
			//Release wakelock
			GcmBroadcastReceiver.completeWakefulIntent(intent);
		}
	}
	
	private SQLData getSQLData()
	{
		if(sqlData == null)
			sqlData = new SQLData(getApplicationContext());
		
		return sqlData;
	}
	
	@SuppressLint("SimpleDateFormat")
	private void handlePendingBuddyMessage(Bundle bundle) throws Exception
	{
		Log.d(AppState.logTag, "Received GCM message pendingBuddy");
		
		BuddyDTO buddy = new BuddyDTO();
		buddy.setMemberId(Long.valueOf(bundle.getString("memberId")));
		buddy.setUfoNumber(Long.valueOf(bundle.getString("ufo")));
		buddy.setFirstName(bundle.getString("firstName"));
		buddy.setLastName(bundle.getString("lastName"));
		buddy.setBirthDate(new SimpleDateFormat("yyyyMMdd").parse(bundle.getString("dob")));
		buddy.setStatus(BuddyStatusDTO.valueOf(bundle.getString("status")));
		
		//Update cached buddy list with the new info
		List<BuddyDTO> cachedBuddies = getSQLData().getCachedBeerBuddyList();
		if(cachedBuddies != null)
		{
			Log.d(AppState.logTag, "Updating cached buddy list with new info for memberId " + buddy.getMemberId());
			
			boolean found = false;
			for(BuddyDTO cachedBuddy : cachedBuddies)
			{
				if(cachedBuddy.getMemberId().longValue() == buddy.getMemberId().longValue())
				{
					Log.w(AppState.logTag, "Found existing cached record for this pendingBuddyMessage. memberId " + buddy.getMemberId());
					
					found = true;
					cachedBuddy.setStatus(buddy.getStatus());
					break;
				}
			}
			
			if(!found)
				cachedBuddies.add(buddy);
			
			getSQLData().updateBeerBuddyListInCache(cachedBuddies);
		}
		
		//Show notification
		showAddAcceptBuddyNotification(getResources().getString(R.string.drinkingBuddiesNotificationTitleInvite), buddy.getFullName() + " " + getResources().getString(R.string.drinkingBuddiesNotificationBodyInvite));
		
		//Send local broadcast (refresh buddy screen if user is on buddy screen when this message comes in)
		Log.d(AppState.logTag, "Sending local broadcast in handlePendingBuddyMessage");
		Intent uiBroadcast = new Intent(UILocalBroadcastReceiver.ACTION_NAME);
		uiBroadcast.putExtra(UILocalBroadcastReceiver.EXTRA_EVENTNAME, UILocalBroadcastReceiver.UIBroadcastEvent.REFRESHCONTENT);
		uiBroadcast.putExtra(UILocalBroadcastReceiver.EXTRA_CONTENTTOREFRESH, MenuOption.Buddies);
		LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(uiBroadcast);
	}
	
	private void handleAcceptBuddyMessage(Bundle bundle)
	{
		Log.d(AppState.logTag, "Received GCM message acceptBuddy");
		
		Long buddyMemberId = Long.valueOf(bundle.getString("memberId"));
		String buddyName = bundle.getString("name");
		
		//Update cached buddy list with the new info
		List<BuddyDTO> cachedBuddies = getSQLData().getCachedBeerBuddyList();
		if(cachedBuddies != null)
		{
			Log.d(AppState.logTag, "Updating cached buddy list with new info for memberId " + buddyMemberId);
			
			boolean found = false;
			for(BuddyDTO cachedBuddy : cachedBuddies)
			{
				if(cachedBuddy.getMemberId().longValue() == buddyMemberId.longValue())
				{
					found = true;
					cachedBuddy.setStatus(BuddyStatusDTO.ACCEPTED);
					break;
				}
			}
			
			if(found)
				getSQLData().updateBeerBuddyListInCache(cachedBuddies);
			else
			{
				Log.w(AppState.logTag, "Didn't find a cached buddy for this acceptBuddy message. Clearing cache");
				getSQLData().clearBeerBuddyListCache();
			}
		}
		
		//Show notification
		showAddAcceptBuddyNotification(getResources().getString(R.string.drinkingBuddiesNotificationTitleInviteAccept), buddyName + " " + getResources().getString(R.string.drinkingBuddiesNotificationBodyInviteAccept));
		
		//Send local broadcast (refresh buddy screen if user is on buddy screen when this message comes in)
		Log.d(AppState.logTag, "Sending local broadcast in handleAcceptBuddyMessage");
		Intent uiBroadcast = new Intent(UILocalBroadcastReceiver.ACTION_NAME);
		uiBroadcast.putExtra(UILocalBroadcastReceiver.EXTRA_EVENTNAME, UILocalBroadcastReceiver.UIBroadcastEvent.REFRESHCONTENT);
		uiBroadcast.putExtra(UILocalBroadcastReceiver.EXTRA_CONTENTTOREFRESH, MenuOption.Buddies);
		LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(uiBroadcast);
	}
	
	private void handleRemoveBuddyMessage(Bundle bundle)
	{
		Log.d(AppState.logTag, "Received GCM message removeBuddy");
		
		Long buddyMemberId = Long.valueOf(bundle.getString("memberId"));
		
		//Update cached buddy list with the new info
		List<BuddyDTO> cachedBuddies = getSQLData().getCachedBeerBuddyList();
		if(cachedBuddies != null)
		{
			Log.d(AppState.logTag, "Updating cached buddy list with new info for memberId " + buddyMemberId);
			
			boolean found = false;
			for(BuddyDTO cachedBuddy : cachedBuddies)
			{
				if(cachedBuddy.getMemberId().longValue() == buddyMemberId.longValue())
				{
					found = true;
					cachedBuddies.remove(cachedBuddy);
					break;
				}
			}
			
			if(found)
				getSQLData().updateBeerBuddyListInCache(cachedBuddies);
			else
				Log.w(AppState.logTag, "Didn't find a cached buddy for this removeBuddy message");
		}
		
		//Send local broadcast (refresh buddy screen if user is on buddy screen when this message comes in)
		Log.d(AppState.logTag, "Sending local broadcast in handleRemoveBuddyMessage");
		Intent uiBroadcast = new Intent(UILocalBroadcastReceiver.ACTION_NAME);
		uiBroadcast.putExtra(UILocalBroadcastReceiver.EXTRA_EVENTNAME, UILocalBroadcastReceiver.UIBroadcastEvent.REFRESHCONTENT);
		uiBroadcast.putExtra(UILocalBroadcastReceiver.EXTRA_CONTENTTOREFRESH, MenuOption.Buddies);
		LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(uiBroadcast);
	}
	
	private void showAddAcceptBuddyNotification(String title, String message)
	{
		Log.d(AppState.logTag, "Creating system notification");
		
		Intent notifyIntent = new Intent(this, MainActivity.class);
		notifyIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
		notifyIntent.putExtra(MainActivity.EXTRA_MOVETOMENUOPTION, MenuOption.Buddies);
		PendingIntent resultPendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, notifyIntent, PendingIntent.FLAG_UPDATE_CURRENT);
		
		NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
		
		builder.setSmallIcon(R.drawable.titlebar_icon);
		builder.setContentTitle(title);
		builder.setContentText(message);
		builder.setContentIntent(resultPendingIntent);
		builder.setDefaults(Notification.DEFAULT_ALL);
		builder.setAutoCancel(true);

		((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).notify(0, builder.build());
	}
}
