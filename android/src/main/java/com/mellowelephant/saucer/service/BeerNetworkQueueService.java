package com.mellowelephant.saucer.service;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import com.mellowelephant.saucer.dto.BeerDTO;
import com.mellowelephant.saucer.thread.AddPendingThread;
import com.mellowelephant.saucer.thread.CommentRatingThread;
import com.mellowelephant.saucer.thread.RemovePendingThread;
import com.mellowelephant.saucer.thread.SubmitQueuesThread;
import com.mellowelephant.saucer.util.AppState;

public class BeerNetworkQueueService extends Service
{
	public static final short ADDPENDING = 0;
	public static final short REMOVEPENDING = 1;
	public static final short COMMENTRATING = 2;
	
	private static Timer timer = null;
	private static final long defaultTimerLength = 300000; //5 minutes
	private static final long maxBackoffTimerLength = 3600000; //1 hour
	private static long backoffTimerLength = defaultTimerLength;
	private static final ExecutorService threadPool = Executors.newFixedThreadPool(3);
	
	@Override
	public IBinder onBind(Intent intent)
	{
		return null;
	}
	
	@Override
	public void onCreate()
	{
		super.onCreate();
		Log.d(AppState.logTag, "onCreate()");
	}
	
	@Override
	public void onDestroy()
	{
		super.onDestroy();
		Log.d(AppState.logTag, "onDestroy()");
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId)
	{
		if(intent == null)	//Will receive a null intent if Android had to kill the service but then restarted it when memory was available.
			Log.d(AppState.logTag, "BNQS: Intent was null");
		else
		{
			short action = intent.getShortExtra("action", (short)-1);
			BeerDTO beer = (BeerDTO) intent.getSerializableExtra("beer");
			
			switch(action)
			{
				case ADDPENDING:
					Log.d(AppState.logTag, "BNQS: Add " + beer.getId() + " as pending");
					threadPool.execute(new AddPendingThread(beer, AppState.currentLocation, this));
					break;
				case REMOVEPENDING:
					Log.d(AppState.logTag, "BNQS: Remove " + beer.getId() + " as pending");
					threadPool.execute(new RemovePendingThread(beer.getId(), this));
					break;
				case COMMENTRATING:
					Log.d(AppState.logTag, "BNQS: Set comment/rating for " + beer.getId());
					threadPool.execute(new CommentRatingThread(beer, this));
					break;
			}
		}
		
		backoffTimerLength = defaultTimerLength;
		startTimer(defaultTimerLength); 
		return START_STICKY;
	}
	
	public void retryTimer()
	{
		if(backoffTimerLength < maxBackoffTimerLength)
			backoffTimerLength *= 2;
		
		startTimer(backoffTimerLength);
	}
	public void startTimer(long time)
	{
		Log.d(AppState.logTag, "Starting BNQS timer with delay of: " + time);
		
		if(timer != null)
			timer.cancel();
		
		timer = new Timer();
		timer.schedule(new TimerTick(), time);	
	}
	
	private class TimerTick extends TimerTask
	{
		@Override
		public void run()
		{
			Log.w(AppState.logTag, "TICK!");
			threadPool.execute(new SubmitQueuesThread(BeerNetworkQueueService.this));
		}
	}
}
