package com.mellowelephant.saucer.inappbilling.util;

import android.content.Intent;

/**
 * Created by BradMetzler on 12/13/2014.
 */
public interface PurchaseAware
{
    public void onPurchaseResponse(int requestCode, int resultCode, Intent data);
}
