package com.mellowelephant.saucer.location;

import android.location.Location;
import android.util.Log;

import com.mellowelephant.saucer.dto.SaucerLocationDTO;
import com.mellowelephant.saucer.util.AppState;

public class LocationFinder
{
	public SaucerLocationDTO determineClosestLocation(Location currentLocation)
	{
		SaucerLocationDTO closest = null;
		float distance = -1;
		
		for(SaucerLocationDTO saucer : SaucerLocationDTO.values())
		{
			if(!saucer.isActiveLocation)
				continue;
			
			Location saucerLoc = new Location("");
			saucerLoc.setLatitude(saucer.latitude);
			saucerLoc.setLongitude(saucer.longitude);
			
			float distanceToThisSaucer = currentLocation.distanceTo(saucerLoc);
			
			Log.d(AppState.logTag, "Distance to " + saucer.displayName + ": " + distanceToThisSaucer + "m");
			
			if(distance == -1 || distanceToThisSaucer < distance)
			{
				distance = distanceToThisSaucer;
				closest = saucer;
			}
		}
		
		Log.d(AppState.logTag, "Closest location is: " + closest.displayName);
		
		return closest;
	}
}