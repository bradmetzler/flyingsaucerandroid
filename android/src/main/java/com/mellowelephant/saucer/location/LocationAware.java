package com.mellowelephant.saucer.location;

import com.mellowelephant.saucer.dto.SaucerLocationDTO;

public interface LocationAware
{
	public void onNewLocation(SaucerLocationDTO location, boolean wasSetAutomatically);
}
