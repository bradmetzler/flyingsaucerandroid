package com.mellowelephant.saucer.location;

import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.mellowelephant.saucer.R;
import com.mellowelephant.saucer.data.AppData;
import com.mellowelephant.saucer.dto.SaucerLocationDTO;
import com.mellowelephant.saucer.util.AppState;
import com.mellowelephant.saucer.util.ArrayAdapterWithIcon;

public class LocationPrompt
{
	private Context context;
	
	public LocationPrompt(Context context)
	{
		this.context = context;
	}
	
	public void show()
	{
		show(true, false);
	}
	public void show(boolean showAutomatic, boolean showInactive)
	{
		final List<String> items = new ArrayList<String>();
		final List<Integer> icons = new ArrayList<Integer>();
		if(showAutomatic)
		{
			items.add(context.getResources().getString(R.string.closestToMe));
			icons.add(R.drawable.mylocation);
		}
		for(SaucerLocationDTO location : SaucerLocationDTO.values())
		{
			if(showInactive || location.isActiveLocation)
			{
				items.add(location.displayName);
				icons.add(showAutomatic ? R.drawable.mylocation_invisible : null);
			}
		}
		
		final AlertDialog dialog = new AlertDialog.Builder(context).create();
		final View view = LayoutInflater.from(context).inflate(R.layout.locationprompt_layout, null, false);
		ListView optionsList = (ListView)view.findViewById(R.id.locationprompt_optionsList);
		
		optionsList.setAdapter(new ArrayAdapterWithIcon(context, items, icons, true));
		optionsList.setOnItemClickListener(new OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id)
			{
				if(position == 0)
					AppState.propagateNewLocation(null, true);
				else
				{
					SaucerLocationDTO chosenLocation = SaucerLocationDTO.getByDisplayName(items.get(position));
					
					AppData.setSavedLocation(chosenLocation, false);
					AppState.propagateNewLocation(chosenLocation, false);
				}
				
				dialog.dismiss();
			}
		});
		
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setCancelable(showAutomatic);
		dialog.setView(view);
		dialog.show();
	}
}
