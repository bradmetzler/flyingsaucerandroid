package com.mellowelephant.saucer.view;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.mellowelephant.saucer.R;
import com.mellowelephant.saucer.callback.BeerModifiedListener;
import com.mellowelephant.saucer.dto.BeerDTO;
import com.mellowelephant.saucer.service.BeerNetworkQueueService;

public class BeerLongPressDialog extends DialogFragment
{
	private BeerDTO beer = null;
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState)
	{
		if(savedInstanceState != null && savedInstanceState.containsKey("beer"))
			beer = (BeerDTO) savedInstanceState.getSerializable("beer");
		else if(getArguments() != null && getArguments().containsKey("beer"))
			beer = (BeerDTO) getArguments().getSerializable("beer");
		
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		if(beer == null)
			builder.setMessage(getResources().getString(R.string.error));
		else
			builder.setView(createView());
		
		return builder.create();
	}
	
	private View createView()
	{
		final View view = getActivity().getLayoutInflater().inflate(R.layout.genericlongpress_layout, null, false);
		
		//Title
		((TextView)view.findViewById(R.id.genericlongpress_title)).setText(beer != null ? beer.getName() : getResources().getString(R.string.beerLongPressTitle));
		
		//Options
		final String OPTION_REMOVE = getResources().getString(R.string.beerLongPressRemove);
		final String OPTION_ADD = getResources().getString(R.string.beerLongPressAdd);
		final String OPTION_EDIT = getResources().getString(R.string.beerLongPressEdit);
		final String OPTION_DETAILS = getResources().getString(R.string.beerLongPressDetails);
		
		int index = 0;
		final String[] options = new String[beer.getPendingLocation() != null ? 3 : 2];
		options[index++] = OPTION_DETAILS;
		options[index++] = beer.hasBeenConsumed() ? OPTION_EDIT : OPTION_ADD;
		if(beer.getPendingLocation() != null)
			options[index++] = OPTION_REMOVE;
		
		ListView optionsList = ((ListView)view.findViewById(R.id.genericlongpress_optionsList));
		optionsList.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, options));
		optionsList.setOnItemClickListener(new OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id)
			{
				String selectedOption = options[position];
				
				if(selectedOption.equals(OPTION_REMOVE))
					removeBeer();
				else if(selectedOption.equals(OPTION_ADD) || selectedOption.equals(OPTION_EDIT))
					addEditBeer();
				else if(selectedOption.equals(OPTION_DETAILS))
					showDetails();
				
				dismiss();
			}
		});
		
		return view;
	}
	
	private void removeBeer()
	{
		beer.setRating(null);
		beer.setComments(null);
		beer.setPendingLocation(null);
		beer.setDateConsumed(null);
		
		Intent queueIntent = new Intent(getActivity(), BeerNetworkQueueService.class);
		queueIntent.putExtra("beer", beer);
		queueIntent.putExtra("action", BeerNetworkQueueService.REMOVEPENDING);
		
		getActivity().startService(queueIntent);
		
		if(getTargetFragment() != null && getTargetFragment() instanceof BeerModifiedListener)
			((BeerModifiedListener)getTargetFragment()).onBeerModified(beer);
	}
	
	private void addEditBeer()
	{
		Bundle arguments = new Bundle();
		arguments.putSerializable("beer", beer);
		
		BeerCommentRatingDialog editDialog = new BeerCommentRatingDialog();
		editDialog.setTargetFragment(getTargetFragment(), 0);
		editDialog.setArguments(arguments);
		editDialog.show(getFragmentManager(), "EditBeer");
	}
	
	private void showDetails()
	{
		Bundle arguments = new Bundle();
		arguments.putSerializable("beer", beer);
		
		BeerDetailsDialog dialog = new BeerDetailsDialog();
		dialog.setTargetFragment(getTargetFragment(), 0);
		dialog.setArguments(arguments);
		dialog.show(getFragmentManager(), "BeerDetails");
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState)
	{
		super.onSaveInstanceState(outState);
		
		if(beer != null)
			outState.putSerializable("beer", beer);
	}
}
