package com.mellowelephant.saucer.view;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.Window;

import com.mellowelephant.saucer.R;
import com.mellowelephant.saucer.data.AppData;

public class BeerMeInstructionsDialog extends DialogFragment
{
	private Callback callback;
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState)
	{		
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		
		builder.setView(getActivity().getLayoutInflater().inflate(R.layout.beermeinsructions_layout, null, false));
		builder.setPositiveButton(getResources().getString(R.string.beerMeInstructionsOkButton), new OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				AppData.setBeerMeInstructionsViewed(true);
				callback.onComplete();
				dismiss();
			}
		});
		
		AlertDialog x = builder.create();
		x.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		return x;
	}
	
	public void setCallback(Callback callback)
	{
		this.callback = callback;
	}
	
	public interface Callback
	{
		public void onComplete();
	}
}
