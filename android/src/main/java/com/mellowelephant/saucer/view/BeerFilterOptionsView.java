package com.mellowelephant.saucer.view;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import android.content.Context;
import android.util.AttributeSet;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.mellowelephant.saucer.R;
import com.mellowelephant.saucer.adapter.BeerFilterArrayAdapter;
import com.mellowelephant.saucer.util.BeerFilter;

public class BeerFilterOptionsView extends LinearLayout
{
	private FilterType filterType;
	private List<Entry<String, Integer>> filterOptions;
	private List<String> preSelectedOptions; 
	private boolean multiselect;
	private FilterChangedListener callback;
	
	public BeerFilterOptionsView(Context context)
	{
		this(context, null);
	}
	
	public BeerFilterOptionsView(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		
		this.setOrientation(LinearLayout.VERTICAL);
	}
	
	public void setFilterOptions(FilterType filterType, Map<String, Integer> filterOptions, List<String> preSelectedOptions, boolean multiselect)
	{
		this.filterType = filterType;
		this.filterOptions = new ArrayList<Entry<String, Integer>>(filterOptions.entrySet());
		this.preSelectedOptions = preSelectedOptions;
		this.multiselect = multiselect;
		
		createView();
	}
	
	public void setFilterChangedListener(FilterChangedListener callback)
	{
		this.callback = callback;
	}
	
	private void createView()
	{
		removeAllViews();
		((LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.beerfilteroptions_layout, this, true);
		
		//Title
		((TextView)findViewById(R.id.beerMenuFilterOptionTitle)).setText(getContext().getResources().getString(filterType.titleResourceId));
		
		//Multi-select instructions
		((TextView)findViewById(R.id.beerMenuFilterOptionMultiselectText)).setVisibility(multiselect ? View.VISIBLE : View.INVISIBLE);
		
		//ListView
		final ListView filterList = (ListView)findViewById(R.id.beerMenuFilterOptionList);
		if(filterOptions != null)
			filterList.setAdapter(new BeerFilterArrayAdapter(getContext(), filterOptions, filterList));
		
		if(preSelectedOptions != null && preSelectedOptions.size() > 1)	//If there is already a filter consisting of more than 1 item, we want to pre-select the items (so you can edit your filter instead of starting all over)
		{
			filterList.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);		
			findViewById(R.id.beerMenuFilterOptionButtons).setVisibility(View.VISIBLE);
			
			Set<String> optionsToSelect = new HashSet<String>(preSelectedOptions);	//Put it in a hashset since we're going to do a bunch of calls to contains()
			
			for(int i = 0; i < filterOptions.size(); i++)
			{
				if(optionsToSelect.contains(filterOptions.get(i).getKey()))
					filterList.setItemChecked(i, true);
			}
		}
		
		filterList.setOnItemClickListener(new OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id)
			{
				if(filterList.getChoiceMode() == ListView.CHOICE_MODE_NONE)
				{
					ArrayList<String> newFilter = new ArrayList<String>();
					String key = filterOptions.get(position).getKey();
					
					if(!key.equalsIgnoreCase("All Styles") && !key.equalsIgnoreCase("All Breweries") && !key.equalsIgnoreCase("Any Container") && !key.equalsIgnoreCase(BeerFilter.Landed.ALL.text))
						newFilter.add(key.equalsIgnoreCase("Other") ? null : key);
					
					if(filterType == FilterType.STYLES && callback != null)
						callback.onStyleFilterChanged(newFilter);
					else if(filterType == FilterType.BREWERIES && callback != null)
						callback.onBreweryFilterChanged(newFilter);
					else if(filterType == FilterType.CONTAINER && callback != null)
						callback.onContainerFilterChanged(newFilter);
					else if(filterType == FilterType.LANDED && callback != null)
						callback.onLandedFilterChanged(newFilter);
				}
				else if(filterList.getChoiceMode() == ListView.CHOICE_MODE_MULTIPLE)
					filterList.setItemChecked(position, filterList.isItemChecked(position));	//No idea what's going on here. Seems like this line of code would do nothing, but has to be like this to work for some reason...
			}
		});
		filterList.setOnItemLongClickListener(new OnItemLongClickListener()
		{
			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id)
			{
				if(multiselect)
				{
					if(filterList.getChoiceMode() != ListView.CHOICE_MODE_MULTIPLE)
					{
						filterList.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);		
						findViewById(R.id.beerMenuFilterOptionButtons).setVisibility(View.VISIBLE);
					}
					
					filterList.setItemChecked(position, !filterList.isItemChecked(position));
					return true;
				}
				
				return false;
			}
		});
		
		//Apply Button (for multi-select only)
		findViewById(R.id.beerMenuFilterOptionApplyButton).setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				ArrayList<String> newFilter = new ArrayList<String>();
				SparseBooleanArray x = filterList.getCheckedItemPositions();
				for(int i = 0; i < filterOptions.size(); i++)
				{
					if(x.get(i))
					{
						String key = filterOptions.get(i).getKey();
						
						if(key.equalsIgnoreCase("All Styles") || key.equalsIgnoreCase("All Breweries") || key.equalsIgnoreCase("Any Container") || key.equalsIgnoreCase(BeerFilter.Landed.ALL.text))
						{
							newFilter = null;
							break;
						}
						
						newFilter.add(key.equalsIgnoreCase("Other") ? null : key);
					}
				}
				
				if(filterType == FilterType.STYLES && callback != null)
					callback.onStyleFilterChanged(newFilter);
				else if(filterType == FilterType.BREWERIES && callback != null)
					callback.onBreweryFilterChanged(newFilter);
				else if(filterType == FilterType.CONTAINER && callback != null)
					callback.onContainerFilterChanged(newFilter);
				else if(filterType == FilterType.LANDED && callback != null)
					callback.onLandedFilterChanged(newFilter);
			}
		});
		
		//Clear Button (for multi-select only)
		findViewById(R.id.beerMenuFilterOptionClearButton).setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				if(filterType == FilterType.STYLES && callback != null)
					callback.onStyleFilterChanged(null);
				else if(filterType == FilterType.BREWERIES && callback != null)
					callback.onBreweryFilterChanged(null);
			}
		});
	}
	
	public interface FilterChangedListener
	{
		public void onStyleFilterChanged(ArrayList<String> newStyleFilter);
		public void onBreweryFilterChanged(ArrayList<String> newBreweryFilter);
		public void onContainerFilterChanged(ArrayList<String> newContainerFilter);
		public void onLandedFilterChanged(ArrayList<String> newLandedFilter);
	}
	public enum FilterType
	{
		STYLES(R.string.styleFilter),
		BREWERIES(R.string.breweryFilter),
		CONTAINER(R.string.containerFilter),
		LANDED(R.string.landedFilter);
		
		public int titleResourceId;
		private FilterType(int titleResourceId)
		{
			this.titleResourceId = titleResourceId;
		}
	}
}
