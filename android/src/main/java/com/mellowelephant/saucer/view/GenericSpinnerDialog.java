package com.mellowelephant.saucer.view;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import com.mellowelephant.saucer.R;

public class GenericSpinnerDialog extends DialogFragment
{
	public static final String ARG_STRINGID = "STRINGID";
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState)
	{
		int stringId = R.string.loading;
		if(getArguments() != null && getArguments().containsKey(ARG_STRINGID))
			stringId = getArguments().getInt(ARG_STRINGID);
		
		setRetainInstance(true);
		
		ProgressDialog spinner = new ProgressDialog(getActivity(), ProgressDialog.STYLE_SPINNER);
		spinner.setMessage(getResources().getString(stringId));
		spinner.setCancelable(false);
		spinner.setCanceledOnTouchOutside(false);
		
		return spinner;
	}
	
	public boolean isShowing()
	{
		return getDialog() != null;
	}
	
	@Override
	public void onDestroyView()	//This fixes some bug with DialogFragment that dismisses it when you rotate
	{
		if(getDialog() != null && getRetainInstance())
			getDialog().setOnDismissListener(null);
		super.onDestroyView();
	}
}
