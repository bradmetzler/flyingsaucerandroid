package com.mellowelephant.saucer.view;

import android.content.Context;
import android.graphics.Typeface;
import android.text.TextUtils.TruncateAt;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TextView;

import com.mellowelephant.saucer.R;
import com.mellowelephant.saucer.util.Formatter;

public class TabView extends LinearLayout
{
	public TabView(Context context)
	{
		super(context);
		
		this.setClickable(true);
		this.setBackgroundResource(R.drawable.tab_selector);
		
		int padding = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 9, getResources().getDisplayMetrics());
		this.setPadding(0, padding, 0, padding);
		this.setGravity(Gravity.CENTER);
		this.setOrientation(LinearLayout.VERTICAL);
		
		this.setLayoutParams(new TableLayout.LayoutParams(0, LayoutParams.WRAP_CONTENT, 1));
	}
	
	public void setTabGroup(final TabGroupView tabGroup)
	{
		this.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				tabGroup.tabSelected(TabView.this);
			}
		});
	}
	
	public void setText(String text)
	{
		if(getChildCount() > 0)	//Already a text. Just change the text
			((TextView)getChildAt(0)).setText(text);
		else
		{
			TextView textView = new TextView(this.getContext());
			
			textView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
			textView.setGravity(Gravity.CENTER_HORIZONTAL);
			textView.setTypeface(null, Typeface.BOLD);
			textView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);
            textView.setTextColor(getContext().getResources().getColor(R.color.darkgray));
			textView.setText(text);
			
			this.addView(textView, 0);
		}
	}
	
	public void setSubtext(String subText)
	{
		if(getChildCount() > 1)	//Already a subtext. Just change the text
			((TextView)getChildAt(1)).setText(subText);
		else					//Gotta build the subtext
		{
			TextView textView = new TextView(this.getContext());
			
			textView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
			textView.setGravity(Gravity.CENTER_HORIZONTAL);
			textView.setTypeface(null, Typeface.ITALIC);
			textView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 11);
			textView.setPadding(Formatter.dpToPx(5), 0, Formatter.dpToPx(5), 0);
			textView.setTextColor(getContext().getResources().getColor(R.color.palegray));
			textView.setSingleLine();
			textView.setEllipsize(TruncateAt.END);
			textView.setText(subText);
			
			this.addView(textView, 1);
		}
	}
}
