package com.mellowelephant.saucer.view;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.TextView;

import com.mellowelephant.saucer.R;

public class BuddyCommentsDialog extends DialogFragment
{
	private String beerName;
	private String buddyName;
	private String comments;
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState)
	{		
		if(savedInstanceState != null && savedInstanceState.containsKey("beerName"))
		{
			beerName = savedInstanceState.getString("beerName");
			buddyName = savedInstanceState.getString("buddyName");
			comments = savedInstanceState.getString("comments");
		}
		else if(getArguments() != null && getArguments().containsKey("beerName"))
		{
			beerName = getArguments().getString("beerName");
			buddyName = getArguments().getString("buddyName");
			comments = getArguments().getString("comments");
		}
		
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		
		builder.setPositiveButton("Close", new OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				dismiss();
			}
		});
		
		builder.setView(createView());
		
		return builder.create();
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState)
	{
		super.onSaveInstanceState(outState);
		
		if(beerName != null)
			outState.putString("beerName", beerName);
		if(buddyName != null)
			outState.putString("buddyName", buddyName);
		if(comments != null)
			outState.putString("comments", comments);
	}
	
	private View createView()
	{
		View view = getActivity().getLayoutInflater().inflate(R.layout.buddycomments_layout, null, false);
		
		((TextView)view.findViewById(R.id.buddyCommentsTitle)).setText(beerName != null ? beerName : getResources().getString(R.string.beerdetails_title));
		((TextView)view.findViewById(R.id.buddyCommentsBuddyName)).setText(buddyName != null ? (buddyName + getResources().getString(R.string.buddyCommentsTitleBuddy)) : "");
		((TextView)view.findViewById(R.id.buddyCommentsComments)).setText(comments != null ? comments : "");
		
		return view;
	}
}
