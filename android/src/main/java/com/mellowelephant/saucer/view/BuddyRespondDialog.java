package com.mellowelephant.saucer.view;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.mellowelephant.saucer.R;
import com.mellowelephant.saucer.callback.BeerBuddyActionListener;
import com.mellowelephant.saucer.data.AppData;
import com.mellowelephant.saucer.dto.BuddyDTO;

import java.text.SimpleDateFormat;

public class BuddyRespondDialog extends DialogFragment
{
	private BuddyDTO buddy = null;
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState)
	{
		if(savedInstanceState != null && savedInstanceState.containsKey("buddy"))
			buddy = (BuddyDTO) savedInstanceState.getSerializable("buddy");
		else if(getArguments() != null && getArguments().containsKey("buddy"))
			buddy = (BuddyDTO) getArguments().getSerializable("buddy");
		
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		
		builder.setNegativeButton("Decline", new OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				respondToInvite(false);
			}
		});
		
		builder.setPositiveButton("Accept", new OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				respondToInvite(true);
			}
		});
		
		if(buddy == null)
			builder.setMessage(getResources().getString(R.string.error));
		else
			builder.setView(createView());
		
		AlertDialog x = builder.create();
		x.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		return x;
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState)
	{
		super.onSaveInstanceState(outState);
		
		if(buddy != null)
			outState.putSerializable("buddy", buddy);
	}
	
	private View createView()
	{
		final View view = getActivity().getLayoutInflater().inflate(R.layout.buddyrespond_layout, null, false);
		
		((TextView)view.findViewById(R.id.buddyrespond_title)).setText(buddy != null ? buddy.getFullName() : getResources().getString(R.string.drinkingBuddiesRespondTitle));
		
		((TextView)view.findViewById(R.id.buddyrespond_summary)).setText(buddy.getFullName() + " " + getResources().getString(R.string.drinkingBuddiesRespondSummary));
		((TextView)view.findViewById(R.id.buddyrespond_detail)).setText(getResources().getString(R.string.drinkingBuddiesRespondDetail) + " " + buddy.getUfoNumber());
		
		return view;
	}
	
	private void respondToInvite(boolean accepted)
	{
		final GenericSpinnerDialog respondingBuddyDialog = new GenericSpinnerDialog();
		Bundle args = new Bundle();
		args.putInt(GenericSpinnerDialog.ARG_STRINGID, accepted ? R.string.drinkingBuddiesInviteAccepting : R.string.drinkingBuddiesInviteDeclining);
		respondingBuddyDialog.setArguments(args);
		respondingBuddyDialog.show(getFragmentManager(), "respondingBuddyDialog");
		
		if(buddy == null)
			return;
		
		final BeerBuddyActionListener respondingBuddyCallback = new BeerBuddyActionListener()
		{
			@Override
			public void onBeerBuddyActionSuccess()
			{
                respondingBuddyDialog.dismiss();

                if(getTargetFragment() != null && getTargetFragment() instanceof BeerBuddyActionListener)
                    ((BeerBuddyActionListener)getTargetFragment()).onBeerBuddyActionSuccess();
			}

			@Override
			public void onBeerBuddyActionError()
			{
                respondingBuddyDialog.dismiss();

                if(getTargetFragment() != null && getTargetFragment() instanceof BeerBuddyActionListener)
                    ((BeerBuddyActionListener)getTargetFragment()).onBeerBuddyActionError();
			}
			
			@Override
			public void onInvalidCredentials()
			{
                respondingBuddyDialog.dismiss();

                if(getTargetFragment() != null && getTargetFragment() instanceof BeerBuddyActionListener)
                    ((BeerBuddyActionListener)getTargetFragment()).onInvalidCredentials();
			}
		};
		
		if(accepted)
			AppData.inviteBuddy(buddy.getUfoNumber().toString(), getFormattedDob(), respondingBuddyCallback);
		else
			AppData.removeBuddy(buddy.getMemberId(), respondingBuddyCallback);
	}
	
	@SuppressLint("SimpleDateFormat")
	private String getFormattedDob()
	{
		if(buddy == null)
			return null;
		
		return new SimpleDateFormat("yyyyMMdd").format(buddy.getBirthDate());
	}
}
