package com.mellowelephant.saucer.view;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.Html;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.mellowelephant.saucer.R;
import com.mellowelephant.saucer.dto.BeerDTO;
import com.mellowelephant.saucer.dto.ContainerDTO;
import com.mellowelephant.saucer.util.AppState;
import com.mellowelephant.saucer.util.Formatter;

public class BeerDetailsDialog extends DialogFragment
{
	private BeerDTO beer = null;
	boolean isLandscape = false;
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState)
	{
		if(savedInstanceState != null && savedInstanceState.containsKey("beer"))
			beer = (BeerDTO) savedInstanceState.getSerializable("beer");
		else if(getArguments() != null && getArguments().containsKey("beer"))
			beer = (BeerDTO) getArguments().getSerializable("beer");
		
		isLandscape = getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE;
		
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		
		//builder.setTitle(beer != null ? beer.getName() : getResources().getString(R.string.beerdetails_title));
		builder.setNegativeButton("Close", new OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				BeerDetailsDialog.this.dismiss();
			}
		});
		
		if(AppState.isUserLoggedIn())
		{
			builder.setPositiveButton(beer.hasBeenConsumed() ? "(tap to edit)" : "Drank it", new OnClickListener()
			{
				@Override
				public void onClick(DialogInterface dialog, int which)
				{
					Bundle arguments = new Bundle();
					arguments.putSerializable("beer", beer);
					
					BeerCommentRatingDialog editDialog = new BeerCommentRatingDialog();
					editDialog.setTargetFragment(getTargetFragment(), 0);
					editDialog.setArguments(arguments);
					editDialog.show(getFragmentManager(), "EditBeer");
				}
			});
		}
		
		if(beer == null)
			builder.setMessage(getResources().getString(R.string.error));
		else
			builder.setView(createView());
		
		AlertDialog x = builder.create();
		x.requestWindowFeature(Window.FEATURE_NO_TITLE);
		x.show();	//Have to call this before we can get a handle on the positive button to set the drawable
		if(beer.hasBeenConsumed())
		{
			int starDrawable = R.drawable.stars_medium_0;
			if(beer.getRating() != null)
			{
				switch(beer.getRating())
				{
					case 1:
						starDrawable = R.drawable.stars_medium_1;
						break;
					case 2:
						starDrawable = R.drawable.stars_medium_2;
						break;
					case 3:
						starDrawable = R.drawable.stars_medium_3;
						break;
					case 4:
						starDrawable = R.drawable.stars_medium_4;
						break;
					case 5:
						starDrawable = R.drawable.stars_medium_5;
						break;
				}
			}
			
			Button button = x.getButton(AlertDialog.BUTTON_POSITIVE);
			button.setTextSize(TypedValue.COMPLEX_UNIT_SP, 10);
			button.setCompoundDrawablesWithIntrinsicBounds(0, starDrawable, 0, 0);
			button.setCompoundDrawablePadding(Formatter.dpToPx(-10));
			button.setPadding(0, Formatter.dpToPx(8), 0, 0);
		}
		
		return x;
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState)
	{
		super.onSaveInstanceState(outState);
		
		if(beer != null)
			outState.putSerializable("beer", beer);
	}
	
	private View createView()
	{
		final View view = getActivity().getLayoutInflater().inflate(R.layout.beerdetail_layout, null, false);
		
		((TextView)view.findViewById(R.id.beerdetail_title)).setText(beer != null ? beer.getName() : getResources().getString(R.string.beerdetails_title));
		
		((TextView)view.findViewById(R.id.beerdetail_availableAtLoc)).setText(beer.isAtCurrentLocation() ? "Available at " + AppState.currentLocation.displayName : "Not Available at " + AppState.currentLocation.displayName);
		((TextView)view.findViewById(R.id.beerdetail_style)).setText(Html.fromHtml("<b>Style:</b> " + beer.getStyle()));
		((TextView)view.findViewById(R.id.beerdetail_brewery)).setText(Html.fromHtml("<b>Brewery:</b> " + beer.getBrewery()));
		((TextView)view.findViewById(R.id.beerdetail_cityCountryPadding)).setText(Html.fromHtml("<b>Brewery:</b> "));
		((TextView)view.findViewById(R.id.beerdetail_container)).setText(" ");
		((TextView)view.findViewById(R.id.beerdetail_cityCountry)).setText(" ");
		((TextView)view.findViewById(R.id.beerdetail_description)).setText("");
		
		if(AppState.isUserLoggedIn())
		{
			int consumedIcon = (beer.getDateConsumed() != null) ? R.drawable.icon_check : R.drawable.icon_blank;
			
			((TextView)view.findViewById(R.id.beerdetail_consumedBeer)).setText(beer.hasBeenConsumed() ? "You had this beer on " + Formatter.formatForDisplay(beer.getDateConsumed()) : "You haven't had this beer");
			((TextView)view.findViewById(R.id.beerdetail_consumedBeer)).setCompoundDrawablesWithIntrinsicBounds(isLandscape ? 0 : consumedIcon, 0, isLandscape ? consumedIcon : 0, 0);
		}
		else
			view.findViewById(R.id.beerdetail_consumedBeer).setVisibility(View.GONE);
		
		if(beer.getStyle() == null)
			view.findViewById(R.id.beerdetail_style).setVisibility(isLandscape ? View.INVISIBLE : View.GONE); //In landscape mode, we need this to still calculate layout so bottle/draught is aligned properly. If bottle/draught is also null, we'll hide it later.

		if(beer.getBrewery() == null)
		{
			view.findViewById(R.id.beerdetail_brewery).setVisibility(isLandscape ? View.INVISIBLE : View.GONE);	//In landscape mode, we need this to still calculate layout so city/country is aligned properly. If city/country is also null, we'll hide it later.
			((TextView)view.findViewById(R.id.beerdetail_cityCountryPadding)).setText("");
		}
		
		int availabilityIcon = beer.isAtCurrentLocation() ? R.drawable.icon_location : R.drawable.icon_blank;
		
		((TextView)view.findViewById(R.id.beerdetail_availableAtLoc)).setCompoundDrawablesWithIntrinsicBounds(availabilityIcon, 0, 0, 0);
		((TextView)view.findViewById(R.id.beerdetail_container)).setCompoundDrawablesWithIntrinsicBounds(isLandscape ? 0 : R.drawable.icon_blank, 0, isLandscape ? R.drawable.icon_blank : 0, 0);
		
		if(beer.getContainer() != null)
		{
			int containerIcon = (beer.getContainer() == ContainerDTO.BOTTLE) ? R.drawable.icon_beer_on : R.drawable.icon_mybeers_on;
			((TextView)view.findViewById(R.id.beerdetail_container)).setCompoundDrawablesWithIntrinsicBounds(isLandscape ? 0 : containerIcon, 0, isLandscape ? containerIcon : 0, 0);
			
			((TextView)view.findViewById(R.id.beerdetail_container)).setText(beer.getContainer().displayName);
		}
		else if(beer.getStyle() == null)	//Container type and style are both null. Get rid of both of them
		{
			view.findViewById(R.id.beerdetail_style).setVisibility(View.GONE);
			view.findViewById(R.id.beerdetail_container).setVisibility(View.GONE);
		}
		
/*		if(beer.getDetails() == null)
		{
			Log.d(AppState.logTag, "Details were null. Fetching them");
			showDetailsAsLoading(view);
			AppData.retrieveBeerDetails(beer.getId(), new BeerDetailsListener()
			{
				@Override
				public void onBeerDetailsRetrieved(Beer beerWithDetails)
				{
					if(beerWithDetails.getId() == beer.getId())
					{
						Log.d(AppState.logTag, "Got details for " + beerWithDetails.getId() + "!");
						beer.setDetails(beerWithDetails.getDetails());
						showDetailsData(view);
					}
					else
					{
						Log.e(AppState.logTag, "Was expecting beerDetails with ID " + beer.getId() + ", but got ID " + beerWithDetails.getId());
						showDetailsError(view);
					}
				}

				@Override
				public void onBeerDetailsError()
				{
					showDetailsError(view);
				}
			});
		}
		else
		{*/
			Log.d(AppState.logTag, "Details were not null.");
			showDetailsData(view);
		//}
		
		return view;
	}
	
//	private void showDetailsAsLoading(View view)
//	{
//		view.findViewById(R.id.beerdetail_description_scroller).setVisibility(View.GONE);
//		view.findViewById(R.id.errorText).setVisibility(View.GONE);
//		view.findViewById(R.id.progressBar).setVisibility(View.VISIBLE);
//	}
	
	private void showDetailsData(View view)
	{
		String cityCountry = "";
		if(beer.getCity() != null)
			cityCountry += beer.getCity();
		if(beer.getCountry() != null)
		{
			if(cityCountry.length() > 0)
				cityCountry += ", ";
			
			cityCountry += beer.getCountry();
		}
		
		view.findViewById(R.id.progressBar).setVisibility(View.GONE);
		view.findViewById(R.id.errorText).setVisibility(View.GONE);
		view.findViewById(R.id.beerdetail_description_scroller).setVisibility(View.VISIBLE);
		
		if(cityCountry.length() == 0)
		{
			((TextView)view.findViewById(R.id.beerdetail_cityCountryPadding)).setVisibility(View.GONE);
			((TextView)view.findViewById(R.id.beerdetail_cityCountry)).setVisibility(View.GONE);
			
			if(beer.getBrewery() == null)
				((TextView)view.findViewById(R.id.beerdetail_brewery)).setVisibility(View.GONE);	//Brewery would have already been invisible, but now we want it completely gone since cityCountry is also empty
		}
			
		((TextView)view.findViewById(R.id.beerdetail_cityCountry)).setText(cityCountry);
		((TextView)view.findViewById(R.id.beerdetail_description)).setText(beer.getDescription());
	}
	
	private void showDetailsError(View view)
	{
		view.findViewById(R.id.progressBar).setVisibility(View.GONE);
		view.findViewById(R.id.beerdetail_description_scroller).setVisibility(View.GONE);
		
		TextView errorText = (TextView)view.findViewById(R.id.errorText);
		errorText.setText(getResources().getString(R.string.beerDetailNetworkError));
		errorText.setVisibility(View.VISIBLE);
	}
}
