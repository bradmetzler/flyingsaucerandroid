package com.mellowelephant.saucer.view;

import java.util.Date;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.mellowelephant.saucer.R;
import com.mellowelephant.saucer.callback.BeerModifiedListener;
import com.mellowelephant.saucer.dto.BeerDTO;
import com.mellowelephant.saucer.service.BeerNetworkQueueService;
import com.mellowelephant.saucer.util.AppState;

public class BeerCommentRatingDialog extends DialogFragment
{
	private BeerDTO beer = null;
	private int newRating = 0;
	private String newComment = "";
	
	private Button saveButton = null;
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState)
	{
		if(savedInstanceState != null && savedInstanceState.containsKey("beer"))
		{
			beer = (BeerDTO) savedInstanceState.getSerializable("beer");
			newRating = savedInstanceState.getInt("newRating");
			newComment = savedInstanceState.getString("newComment");
		}
		else if(getArguments() != null && getArguments().containsKey("beer"))
		{
			beer = (BeerDTO) getArguments().getSerializable("beer");
			newRating = beer.getRating() == null ? 0 : beer.getRating();
			newComment = beer.getComments();
		}
		
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		
		builder.setNegativeButton("Cancel", new OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				BeerCommentRatingDialog.this.dismiss();
			}
		});
		builder.setPositiveButton("Save", new OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				boolean alreadyConsumed = beer.hasBeenConsumed();
				
				beer.setRating(newRating);
				beer.setComments(newComment);
				if(!alreadyConsumed)
				{
					beer.setPendingLocation(AppState.currentLocation);
					beer.setDateConsumed(new Date());
				}
				
				Intent queueIntent = new Intent(getActivity(), BeerNetworkQueueService.class);
				queueIntent.putExtra("beer", beer);
				queueIntent.putExtra("action", alreadyConsumed ? BeerNetworkQueueService.COMMENTRATING : BeerNetworkQueueService.ADDPENDING);
				
				getActivity().startService(queueIntent);
				
				if(getTargetFragment() != null && getTargetFragment() instanceof BeerModifiedListener)
					((BeerModifiedListener)getTargetFragment()).onBeerModified(beer);
			}
		});
		
		if(beer == null)
			builder.setMessage(getResources().getString(R.string.error));
		else
			builder.setView(createView());
		
		AlertDialog x = builder.create();
		x.requestWindowFeature(Window.FEATURE_NO_TITLE);
		x.show();	//Have to call this before we can get a handle on the positive button to enable/disable it
		saveButton = x.getButton(AlertDialog.BUTTON_POSITIVE);
		saveButton.setEnabled(beerHasBeenModified());
		
		return x;
	}
	
	private boolean beerHasBeenModified()
	{
		if(!beer.hasBeenConsumed())	//If the beer has not already been tasted, user should be able to save even without setting a comment/rating.
			return true;
		
		int currentRating = beer.getRating() == null ? 0 : beer.getRating();
		if(currentRating != newRating)
			return true;
		
		if(!(((newComment == null || newComment.trim().isEmpty()) && (beer.getComments() == null || beer.getComments().trim().isEmpty())) || (newComment.trim().equals(beer.getComments()))))
			return true;
		
		return false;
	}
	
	private void setStarRating(int newRating, View view)
	{
		this.newRating = newRating;
		
		((ImageView)view.findViewById(R.id.beercommentrating_star1)).setImageResource(newRating >= 1 ? R.drawable.star_large_on : R.drawable.star_large_off);
		((ImageView)view.findViewById(R.id.beercommentrating_star2)).setImageResource(newRating >= 2 ? R.drawable.star_large_on : R.drawable.star_large_off);
		((ImageView)view.findViewById(R.id.beercommentrating_star3)).setImageResource(newRating >= 3 ? R.drawable.star_large_on : R.drawable.star_large_off);
		((ImageView)view.findViewById(R.id.beercommentrating_star4)).setImageResource(newRating >= 4 ? R.drawable.star_large_on : R.drawable.star_large_off);
		((ImageView)view.findViewById(R.id.beercommentrating_star5)).setImageResource(newRating >= 5 ? R.drawable.star_large_on : R.drawable.star_large_off);
		
		if(saveButton != null)	//On orientation change, saveButton will be null here when the view is being re-created. The button's about to be enabled/disabled in this case anyway, so we can just skip this.
			saveButton.setEnabled(beerHasBeenModified());
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState)
	{
		super.onSaveInstanceState(outState);
		
		if(beer != null)
			outState.putSerializable("beer", beer);
		
		outState.putInt("newRating", newRating);
		outState.putString("newComment", newComment);
	}
	
	private View createView()
	{
		final View view = getActivity().getLayoutInflater().inflate(R.layout.beercommentrating_layout, null, false);
		
		((TextView)view.findViewById(R.id.beercommentrating_title)).setText(beer != null ? beer.getName() : getResources().getString(R.string.beerCommentRatingTitle));
		
		//Star click handlers
		view.findViewById(R.id.beercommentrating_hitbox1).setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				setStarRating(1, view);
			}
		});
		view.findViewById(R.id.beercommentrating_hitbox2).setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				setStarRating(2, view);
			}
		});
		view.findViewById(R.id.beercommentrating_hitbox3).setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				setStarRating(3, view);
			}
		});
		view.findViewById(R.id.beercommentrating_hitbox4).setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				setStarRating(4, view);
			}
		});
		view.findViewById(R.id.beercommentrating_hitbox5).setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				setStarRating(5, view);
			}
		});
		
		EditText commentTextBox = ((EditText)view.findViewById(R.id.beercommentrating_comment));
		commentTextBox.addTextChangedListener(new TextWatcher()
		{
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after){}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count){}

			@Override
			public void afterTextChanged(Editable s)
			{
				Log.w(AppState.logTag, "afterTextChanged()");
				newComment = s.toString();
				
				if(saveButton != null)	//On orientation change, saveButton will be null here when the view is being re-created. The button's about to be enabled/disabled in this case anyway, so we can just skip this.
					saveButton.setEnabled(beerHasBeenModified());
			}
		});
		
		if(newComment != null)
			commentTextBox.setText(newComment);
		
		if(newRating != 0)
			setStarRating(newRating, view);
		
		return view;
	}
}
