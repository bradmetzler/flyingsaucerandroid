package com.mellowelephant.saucer.view;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.mellowelephant.saucer.R;
import com.mellowelephant.saucer.callback.BeerBuddyActionListener;
import com.mellowelephant.saucer.data.AppData;
import com.mellowelephant.saucer.dto.BuddyDTO;

public class BuddyLongPressDialog extends DialogFragment
{
	private BuddyDTO buddy = null;
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState)
	{
		if(savedInstanceState != null && savedInstanceState.containsKey("buddy"))
			buddy = (BuddyDTO) savedInstanceState.getSerializable("buddy");
		else if(getArguments() != null && getArguments().containsKey("buddy"))
			buddy = (BuddyDTO) getArguments().getSerializable("buddy");
		
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		if(buddy == null)
			builder.setMessage(getResources().getString(R.string.error));
		else
			builder.setView(createView());
		
		return builder.create();
	}
	
	private View createView()
	{
		final View view = getActivity().getLayoutInflater().inflate(R.layout.genericlongpress_layout, null, false);
		
		//Title
		((TextView)view.findViewById(R.id.genericlongpress_title)).setText(buddy != null ? buddy.getFullName() : getResources().getString(R.string.drinkingBuddiesLongPressTitle));
		
		//Options
		final String OPTION_REMOVE = getResources().getString(R.string.drinkingBuddiesLongPressRemove);
		final String[] options = new String[]{OPTION_REMOVE};
		
		ListView optionsList = ((ListView)view.findViewById(R.id.genericlongpress_optionsList));
		optionsList.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, options));
		optionsList.setOnItemClickListener(new OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id)
			{
				String selectedOption = options[position];
				
				if(selectedOption.equals(OPTION_REMOVE))
					removeBuddy();
				
				dismiss();
			}
		});
		
		return view;
	}
	
	private void removeBuddy()
	{
		final GenericSpinnerDialog invitingBuddyDialog = new GenericSpinnerDialog();
		Bundle args = new Bundle();
		args.putInt(GenericSpinnerDialog.ARG_STRINGID, R.string.drinkingBuddiesInviteRemoving);
		invitingBuddyDialog.setArguments(args);
		invitingBuddyDialog.show(getFragmentManager(), "invitingBuddyDialog");
		
		if(buddy == null)
			return;
		
		AppData.removeBuddy(buddy.getMemberId(), new BeerBuddyActionListener()
		{
			@Override
			public void onBeerBuddyActionSuccess()
			{
                invitingBuddyDialog.dismiss();

                if(getTargetFragment() != null && getTargetFragment() instanceof BeerBuddyActionListener)
                    ((BeerBuddyActionListener)getTargetFragment()).onBeerBuddyActionSuccess();
			}

			@Override
			public void onBeerBuddyActionError()
			{
                invitingBuddyDialog.dismiss();

                if(getTargetFragment() != null && getTargetFragment() instanceof BeerBuddyActionListener)
                    ((BeerBuddyActionListener)getTargetFragment()).onBeerBuddyActionError();
			}
			
			@Override
			public void onInvalidCredentials()
			{
                invitingBuddyDialog.dismiss();

                if(getTargetFragment() != null && getTargetFragment() instanceof BeerBuddyActionListener)
                    ((BeerBuddyActionListener)getTargetFragment()).onInvalidCredentials();
			}
		});
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState)
	{
		super.onSaveInstanceState(outState);
		
		if(buddy != null)
			outState.putSerializable("buddy", buddy);
	}
}
