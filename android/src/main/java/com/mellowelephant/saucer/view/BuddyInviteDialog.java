package com.mellowelephant.saucer.view;

import java.util.Calendar;
import java.util.GregorianCalendar;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.DatePicker.OnDateChangedListener;
import android.widget.EditText;

import com.mellowelephant.saucer.R;
import com.mellowelephant.saucer.callback.BeerBuddyActionListener;
import com.mellowelephant.saucer.data.AppData;

public class BuddyInviteDialog extends DialogFragment
{
	private Button saveButton;
	private EditText cardNumberInput;
	private DatePicker dobPicker;
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState)
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		
		builder.setNegativeButton("Cancel", new OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				BuddyInviteDialog.this.dismiss();
			}
		});
		
		builder.setPositiveButton("Invite", new OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				sendInvite();
			}
		});
		
		builder.setView(createView());
		
		AlertDialog x = builder.create();
		x.requestWindowFeature(Window.FEATURE_NO_TITLE);
		x.show(); 	//Have to call this before we can get a handle on the positive button to enable/disable it
		saveButton = x.getButton(AlertDialog.BUTTON_POSITIVE);
		saveButton.setEnabled(isFormValid());
		
		return x;
	}
	
	private View createView()
	{
		final View view = getActivity().getLayoutInflater().inflate(R.layout.buddyinvite_layout, null, false);
		
		cardNumberInput = (EditText)view.findViewById(R.id.buddyinvite_cardNumberInput);
		cardNumberInput.addTextChangedListener(new TextWatcher()
		{
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after){}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count){}

			@Override
			public void afterTextChanged(Editable s)
			{
				if(saveButton != null)	//On orientation change, saveButton will be null here when the view is being re-created. The button's about to be enabled/disabled in this case anyway, so we can just skip this.
					saveButton.setEnabled(isFormValid());
			}
		});
		
		GregorianCalendar today = new GregorianCalendar();
		dobPicker = (DatePicker)view.findViewById(R.id.buddyinvite_dobPicker);
		dobPicker.init(today.get(Calendar.YEAR) - 21, today.get(Calendar.MONTH), today.get(Calendar.DAY_OF_MONTH), new OnDateChangedListener()
		{
			@Override
			public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth)
			{
				InputMethodManager imm = (InputMethodManager)getActivity().getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(cardNumberInput.getWindowToken(), 0);
			}
		});
		
		return view;
	}
	
	private boolean isFormValid()
	{
		if(cardNumberInput == null)
			return false;
		
		return !cardNumberInput.getText().toString().trim().isEmpty();
	}
	
	private void sendInvite()
	{
		final GenericSpinnerDialog invitingBuddyDialog = new GenericSpinnerDialog();
		Bundle args = new Bundle();
		args.putInt(GenericSpinnerDialog.ARG_STRINGID, R.string.drinkingBuddiesInviteInviting);
		invitingBuddyDialog.setArguments(args);
		invitingBuddyDialog.show(getFragmentManager(), "invitingBuddyDialog");
		
		if(cardNumberInput == null || dobPicker == null)
			return;
		
		AppData.inviteBuddy(cardNumberInput.getText().toString(), getFormattedDob(), new BeerBuddyActionListener()
		{
			private static final long serialVersionUID = -7494155653799881124L;

			@Override
			public void onBeerBuddyActionSuccess()
			{
				if(invitingBuddyDialog != null)
				{
					invitingBuddyDialog.dismiss();

                    if(getTargetFragment() != null && getTargetFragment() instanceof BeerBuddyActionListener)
                        ((BeerBuddyActionListener)getTargetFragment()).onBeerBuddyActionSuccess();
				}
			}

			@Override
			public void onBeerBuddyActionError()
			{
				if(invitingBuddyDialog != null)
				{
					invitingBuddyDialog.dismiss();

                    if(getTargetFragment() != null && getTargetFragment() instanceof BeerBuddyActionListener)
                        ((BeerBuddyActionListener)getTargetFragment()).onBeerBuddyActionError();
				}
			}
			
			@Override
			public void onInvalidCredentials()
			{
				if(invitingBuddyDialog != null)
				{
					invitingBuddyDialog.dismiss();

                    if(getTargetFragment() != null && getTargetFragment() instanceof BeerBuddyActionListener)
                        ((BeerBuddyActionListener)getTargetFragment()).onInvalidCredentials();
				}
			}
		});
	}
	
	private String getFormattedDob()
	{
		if(dobPicker == null)
			return null;
		
		StringBuilder formattedDob = new StringBuilder();
		
		formattedDob.append(dobPicker.getYear());
		if(dobPicker.getMonth() < 9)
			formattedDob.append("0");
		formattedDob.append(dobPicker.getMonth() + 1);
		if(dobPicker.getDayOfMonth() < 10)
			formattedDob.append("0");
		formattedDob.append(dobPicker.getDayOfMonth());
		
		return formattedDob.toString();
	}
}
