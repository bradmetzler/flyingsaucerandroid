package com.mellowelephant.saucer.view;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.mellowelephant.saucer.R;
import com.mellowelephant.saucer.adapter.BuddyRatingArrayAdapter;
import com.mellowelephant.saucer.adapter.BuddyRatingArrayAdapter.Container;
import com.mellowelephant.saucer.dto.BeerDTO;
import com.mellowelephant.saucer.dto.BuddyBeerDTO;
import com.mellowelephant.saucer.dto.BuddyDTO;

import java.util.ArrayList;
import java.util.List;

public class BuddyRatingDialog extends DialogFragment
{
	private BeerDTO beer = null;
	private ArrayList<BuddyDTO> buddies = null;
	
	@SuppressWarnings("unchecked")
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState)
	{
		if(savedInstanceState != null && savedInstanceState.containsKey("beer"))
		{
			beer = (BeerDTO) savedInstanceState.getSerializable("beer");
			buddies = (ArrayList<BuddyDTO>)savedInstanceState.getSerializable("buddies");
		}
		else if(getArguments() != null && getArguments().containsKey("beer"))
		{
			beer = (BeerDTO) getArguments().getSerializable("beer");
			buddies = (ArrayList<BuddyDTO>)getArguments().getSerializable("buddies");
		}
		
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		
		builder.setNegativeButton("Close", new OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				BuddyRatingDialog.this.dismiss();
			}
		});
		
		builder.setPositiveButton("Beer Details", new OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				Bundle arguments = new Bundle();
				arguments.putSerializable("beer", beer);
				
				BeerDetailsDialog detailsDialog = new BeerDetailsDialog();
				detailsDialog.setTargetFragment(getTargetFragment(), 0);
				detailsDialog.setArguments(arguments);
				detailsDialog.show(getFragmentManager(), "BeerDetails");
			}
		});
		
		if(beer == null)
			builder.setMessage(getResources().getString(R.string.error));
		else
			builder.setView(createView());
		
		return builder.create();
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState)
	{
		super.onSaveInstanceState(outState);
		
		if(beer != null)
			outState.putSerializable("beer", beer);
		if(buddies != null)
			outState.putSerializable("buddies", buddies);
	}
	
	private View createView()
	{
		final View view = getActivity().getLayoutInflater().inflate(R.layout.buddyrating_layout, null, false);
		
		((TextView)view.findViewById(R.id.buddyrating_title)).setText(beer != null ? beer.getName() : getResources().getString(R.string.beerdetails_title));
		
		final List<Container> adapterItems = new ArrayList<Container>();
		Container meContainer = new Container();
		if(beer.hasBeenConsumed())
		{
			BuddyBeerDTO meBuddyBeer = new BuddyBeerDTO();
			meBuddyBeer.setBuddyRating(beer.getRating());
			meBuddyBeer.setDateConsumed(beer.getDateConsumed());
			meBuddyBeer.setComments(beer.getComments());
			meContainer.buddyBeer = meBuddyBeer;
		}
		adapterItems.add(meContainer);
		
		for(BuddyDTO buddy : buddies)
		{
			Container buddyContainer = new Container();
			buddyContainer.buddy = buddy;
			
			List<BuddyBeerDTO> buddyBeers = beer.getBuddies();
			if(buddyBeers != null)
			{
				for(BuddyBeerDTO buddyBeer : buddyBeers)
				{
					if(buddyBeer.getMemberId().longValue() == buddy.getMemberId().longValue())
					{
						buddyContainer.buddyBeer = buddyBeer;
						break;
					}
				}
			}
			
			adapterItems.add(buddyContainer);
		}
		
		ListView buddyList = (ListView)view.findViewById(R.id.buddyrating_buddyList);
		buddyList.setAdapter(new BuddyRatingArrayAdapter(getActivity().getApplicationContext(), adapterItems));
		buddyList.setOnItemClickListener(new OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id)
			{
				Container container = adapterItems.get(position);
				if(container != null && container.buddy == null && beer.hasBeenConsumed())
				{
					//User row was selected, and user has consumed this beer. Take them to edit their rating/comments
					Bundle arguments = new Bundle();
					arguments.putSerializable("beer", beer);
					
					BeerCommentRatingDialog editDialog = new BeerCommentRatingDialog();
					editDialog.setTargetFragment(getTargetFragment(), 0);
					editDialog.setArguments(arguments);
					editDialog.show(getFragmentManager(), "EditBeer");
				}
				else if(container != null && container.buddyBeer != null && container.buddyBeer.getComments() != null && !container.buddyBeer.getComments().isEmpty())
				{
					//Buddy row was selected, and buddy commented on this beer. Show user the buddy's comments.
					Bundle arguments = new Bundle();
					arguments.putString("beerName", beer.getName());
					arguments.putString("buddyName", container.buddy == null ? null : container.buddy.getFullName());
					arguments.putString("comments", container.buddyBeer.getComments());
					
					BuddyCommentsDialog dialog = new BuddyCommentsDialog();
					dialog.setTargetFragment(getTargetFragment(), 0);
					dialog.setArguments(arguments);
					dialog.show(getFragmentManager(), "BuddyComments");
				}
			}
		});
		
		return view;
	}
}
