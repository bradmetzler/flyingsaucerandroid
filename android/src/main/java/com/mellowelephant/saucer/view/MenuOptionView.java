package com.mellowelephant.saucer.view;

import android.content.Context;
import android.graphics.Typeface;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mellowelephant.saucer.R;
import com.mellowelephant.saucer.util.MenuOption;

public class MenuOptionView extends LinearLayout
{
	private MenuOption menuOption;
	
	private ImageView icon;
	private TextView text;
	
	public MenuOptionView(Context context)
	{
		super(context);
	}

	public MenuOptionView(Context context, MenuOption menuOption)
	{
		super(context);
		
		this.menuOption = menuOption;
		
		int padding = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 7, getResources().getDisplayMetrics());
		int height = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 48, getResources().getDisplayMetrics());
		
		icon = new ImageView(context);
		icon.setImageResource(menuOption.iconGrayResource);
		icon.setPadding(padding, padding, padding, padding);
		
		text = new TextView(context);
		text.setText(menuOption.menuText);
		text.setGravity(Gravity.CENTER_VERTICAL);
		text.setHeight(height);
		
		this.addView(icon);
		this.addView(text);
		
		this.setClickable(true);
		this.setOrientation(LinearLayout.HORIZONTAL);
		this.setGravity(Gravity.CENTER_VERTICAL);
		this.setBackgroundResource(R.drawable.menuitem_selector);
	}
	
	public MenuOption getMenuOption()
	{
		return this.menuOption;
	}
	
	public void toggleHighlight(boolean on)
	{
		icon.setImageResource(on ? menuOption.iconResource : menuOption.iconGrayResource);
		text.setTypeface(null, on ? Typeface.BOLD : Typeface.NORMAL);
		text.setTextColor(on ? getResources().getColor(R.color.darkgray) : getResources().getColor(R.color.white));
		//this.setBackgroundColor(on ? getResources().getColor(R.color.gray) : 0x00000000);
		this.setSelected(on);
	}
}