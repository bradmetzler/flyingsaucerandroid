package com.mellowelephant.saucer.view;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;

import com.mellowelephant.saucer.R;
import com.mellowelephant.saucer.util.AppState;

public class TabGroupView extends LinearLayout
{
	private List<TabView> tabList = new ArrayList<TabView>();
	private Map<TabView, OnClickListener> tabMap = new HashMap<TabView, OnClickListener>();
	private int selectedTabIndex;
	private boolean enabled = false;
	
	public TabGroupView(Context context)
	{
		this(context, null);
	}
	
	public TabGroupView(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		
		this.setOrientation(LinearLayout.HORIZONTAL);
		this.setGravity(Gravity.CENTER_VERTICAL);
		this.setBackgroundResource(R.drawable.tabgroup_background);
		this.selectedTabIndex = -1;
	}
	
	public TabView addTab(String tabText, OnClickListener clickHandler)
	{
		return addTab(tabText, null, clickHandler);
	}
	
	public TabView addTab(String tabText, String subText, OnClickListener clickHandler)
	{
		TabView newTab = new TabView(getContext());
		newTab.setTabGroup(this);
		newTab.setText(tabText);
		newTab.setSubtext(subText);
		
		tabList.add(newTab);
		tabMap.put(newTab, clickHandler);
		
		this.setWeightSum(tabList.size());
		
		if(tabList.size() > 1)
		{
			int dividerHeight = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 28, getResources().getDisplayMetrics());
			View ruler = new View(getContext());
			ruler.setBackgroundColor(getResources().getColor(R.color.contentdarkgray));
			addView(ruler, new LayoutParams(1, dividerHeight));
		}
		
		addView(newTab);
		return newTab;
	}
	
	public void tabSelected(TabView tab)
	{
		if(!enabled)
			return;
		
		for(int i = 0; i < tabList.size(); i++)
		{
			TabView tabView = tabList.get(i);
			
			tabView.setSelected(tab == tabView);
			
			if(tab == tabView)
				selectedTabIndex = i;
		}
		
		OnClickListener clickHandler = tabMap.get(tab);
		if(clickHandler != null)
			clickHandler.onClick(tab);
	}
	
	public boolean unselectTab()
	{
		if(selectedTabIndex >= 0)
		{
			selectedTabIndex = -1;
			
			for(TabView tab : tabList)
				tab.setSelected(false);
			
			return true;
		}
		
		return false;
	}
	public void setSelectedTab(int whichTab)
	{
		Log.w(AppState.logTag, "setSelectedTab(" + whichTab + ") tabList size: " + tabList.size());
		
		if(whichTab < 0 || tabList.size() <= whichTab)
			return;
		
		tabSelected(tabList.get(whichTab));
	}
	
	public int getSelectedTabIndex()
	{
		return selectedTabIndex;
	}
	
	public void enable()
	{
		enabled = true;
	}
	
	public void disable()
	{
		enabled = false;
	}
}
