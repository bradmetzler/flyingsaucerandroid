package com.mellowelephant.saucer.view;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.Window;

import com.mellowelephant.saucer.R;

public class BuddyNotFoundDialog extends DialogFragment
{
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState)
	{		
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		
		builder.setView(getActivity().getLayoutInflater().inflate(R.layout.buddynotfound_layout, null, false));
		builder.setPositiveButton(getResources().getString(R.string.drinkingBuddiesInviteBuddyNotFoundButton), new OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				dismiss();
			}
		});
		
		AlertDialog x = builder.create();
		x.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		return x;
	}
}
