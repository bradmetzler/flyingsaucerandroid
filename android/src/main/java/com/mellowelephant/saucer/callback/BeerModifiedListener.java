package com.mellowelephant.saucer.callback;

import com.mellowelephant.saucer.dto.BeerDTO;


public interface BeerModifiedListener
{
	public void onBeerModified(BeerDTO beer);
}
