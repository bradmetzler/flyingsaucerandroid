package com.mellowelephant.saucer.callback;

import com.mellowelephant.saucer.dto.UserDTO;

public interface SignInListener
{
	public void onLoginSuccessful(UserDTO loggedInUser);
	public void onLoginFailure();
}
