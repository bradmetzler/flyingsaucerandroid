package com.mellowelephant.saucer.callback;

import com.mellowelephant.saucer.dto.BuddyDTO;

import java.util.List;

public interface BeerBuddyListListener
{
	public void onBeerBuddyListRetrieved(List<BuddyDTO> buddyList);
	public void onBeerBuddyListError();
	public void onInvalidCredentials();
}
