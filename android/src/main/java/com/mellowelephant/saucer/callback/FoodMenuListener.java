package com.mellowelephant.saucer.callback;

import android.graphics.Bitmap;

public interface FoodMenuListener
{
	public void onFoodMenuRetrieved(Bitmap foodMenu);
	public void onFoodMenuError();
}
