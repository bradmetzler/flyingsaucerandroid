package com.mellowelephant.saucer.callback;

public interface BeerBuddyActionListener
{
	public void onBeerBuddyActionSuccess();
	public void onBeerBuddyActionError();
	public void onInvalidCredentials();
}
