package com.mellowelephant.saucer.callback;

import com.mellowelephant.saucer.dto.BeerDTO;

import java.util.List;

public interface BeerListListener
{
	public void onBeerListRetrieved(List<BeerDTO> beerList);
	public void onBeerListError();
	public void onInvalidCredentials();
}
