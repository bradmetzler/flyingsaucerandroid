package com.mellowelephant.saucer.callback;

import com.mellowelephant.saucer.dto.BeerDTO;

public interface BeerDetailsListener
{
	public void onBeerDetailsRetrieved(BeerDTO beer);
	public void onBeerDetailsError();
}
