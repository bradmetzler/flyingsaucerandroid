package com.mellowelephant.saucer.util;

import java.util.List;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class ArrayAdapterWithIcon extends ArrayAdapter<String>
{
	private List<Integer> images = null;
	
	public ArrayAdapterWithIcon(Context context, List<String> items, List<Integer> images, boolean forDialog)
	{
		super(context, forDialog ? android.R.layout.select_dialog_item : android.R.layout.simple_list_item_1, items);
		this.images = images;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		View view = super.getView(position, convertView, parent);
		TextView textView = (TextView) view.findViewById(android.R.id.text1);
		
		Drawable drawable = null;
		if(images != null && images.get(position) != null)
			drawable = getContext().getResources().getDrawable(images.get(position));

		textView.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
		textView.setCompoundDrawablePadding((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 12, getContext().getResources().getDisplayMetrics()));
		
		return view;
	}
}