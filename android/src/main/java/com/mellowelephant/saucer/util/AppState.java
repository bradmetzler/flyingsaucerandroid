package com.mellowelephant.saucer.util;

import android.app.Dialog;
import android.content.Context;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.mellowelephant.saucer.activity.MainActivity;
import com.mellowelephant.saucer.data.AppData;
import com.mellowelephant.saucer.dto.SaucerLocationDTO;
import com.mellowelephant.saucer.dto.UserDTO;
import com.mellowelephant.saucer.fragment.content.ContentFragment;
import com.mellowelephant.saucer.fragment.menu.AppMenu;
import com.mellowelephant.saucer.inappbilling.util.IabHelper;
import com.mellowelephant.saucer.inappbilling.util.IabResult;
import com.mellowelephant.saucer.inappbilling.util.Inventory;
import com.mellowelephant.saucer.inappbilling.util.Purchase;
import com.mellowelephant.saucer.location.LocationAware;
import com.mellowelephant.saucer.service.UILocalBroadcastReceiver;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

public class AppState
{
	//Keep track of history ourselves (mostly so we can keep menu in sync)
	public static final Stack<MenuOption> historyStack = new Stack<MenuOption>();
	
	//Misc
	public static final String logTag = "MellowElephant-FS";
	public static MainActivity activity = null;
	public static Context context = null;
	private static final UILocalBroadcastReceiver uiLocalBroadcastReceiver = new UILocalBroadcastReceiver();
	
	//UI State
	public static boolean twoPaneMode = false;
	public static MenuOption selectedMenuOption = null;
	public static AppMenu menuFragment = null;
	public static ContentFragment contentFragment = null;	//If calling methods on this contentFragment and you get some weird NPEs after orientation change, need to set this in the onCreate() of the content fragments
	
	//Location
	public static SaucerLocationDTO currentLocation = null;
	private static Map<String, LocationAware> locationAwares = new HashMap<String, LocationAware>();

    //Billing
    public static boolean hasDonated = false;
    public static String billingKey1 = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAkXracz+XeTf2Ui8zRQCzYI75iIQ/vWVDqe0pkzI3HgiSr8W3Fn6uFdE4FAX80WPJATGQ7mKg68fUnGTeJmDYvL8vrBL";
    public static String billingKey3 = "schrW15+N1uQSs30xE4cc0+akQemlHW0BD3Y40xNe02eTSHlBgHEBGhVc1QBLJ0tKwnH6YIliEM2WKl+3EeGj5HMt3Ry3TuQCtJoXbAe3wcRIEXxjjB/qty/R9O/k5VZQIDAQAB";
    public static String billingKey2 = "17kUFacrZKh9u0Ws2vYRLl/iazlrMMzSF8vvr0kMt2zfUfap0i3Lqp9dJBbGkOF/WFqV0F3/4rh1kNtwijjQ872h7KBnTa+tiMu4q5mTJNGT9O7i2LFMPCtl47";
    public static String[] sku_unlock = new String[]{"com.mellowelephant.saucer.product.unlock.one", "com.mellowelephant.saucer.product.unlock.two", "com.mellowelephant.saucer.product.unlock.three", "com.mellowelephant.saucer.product.unlock.four", "com.mellowelephant.saucer.product.unlock.five"};
    public static String[] sku_donate = new String[]{"com.mellowelephant.saucer.product.donate.one", "com.mellowelephant.saucer.product.donate.two", "com.mellowelephant.saucer.product.donate.three", "com.mellowelephant.saucer.product.donate.four", "com.mellowelephant.saucer.product.donate.five"};
    public static final int purchaseRequestCode = 1234321;

	//Login
	public static UserDTO user = null;
	
	public static void initializeAppState(MainActivity activity, MenuOption startingMenuOption)	//When app starts, set default menu option etc...
	{
		AppState.activity = activity;
		AppState.context = activity.getApplicationContext();
		AppState.historyStack.clear();
		
		LocalBroadcastManager.getInstance(context).registerReceiver(uiLocalBroadcastReceiver, new IntentFilter(UILocalBroadcastReceiver.ACTION_NAME));
		
		if(startingMenuOption != null)
		{
			selectedMenuOption = startingMenuOption;
			try{contentFragment = selectedMenuOption.contentFragmentClass.newInstance();}catch(Exception e){}
		}
		if(selectedMenuOption == null)
		{
			selectedMenuOption = MenuOption.defaultMenuOption;
			try{contentFragment = selectedMenuOption.contentFragmentClass.newInstance();}catch(Exception e){}
		}
		
		if(contentFragment == null)
			{try{contentFragment = selectedMenuOption.contentFragmentClass.newInstance();}catch(Exception e){}}
		if(menuFragment == null)
			menuFragment = new AppMenu();
		
		user = AppData.getLoggedInUser();
		
		AppData.initializeNotifications();
	}
	
	public static void shutdown()
	{
		Log.d(AppState.logTag, "AppState.shutdown()");
		LocalBroadcastManager.getInstance(context).unregisterReceiver(uiLocalBroadcastReceiver);
	}
	
	public static void registerAsLocationAware(String key, LocationAware locationAware)
	{
		locationAwares.put(key, locationAware);
	}
	public static void unregisterAsLocationAware(String key)
	{
		locationAwares.remove(key);
	}
	public static void propagateNewLocation(SaucerLocationDTO newLocation, boolean wasSetAutomatically)
	{
		currentLocation = newLocation;
		
		List<LocationAware> awares = new ArrayList<LocationAware>(locationAwares.values());	//Create a new list of the current values in the map of locationAwares. This avoids a ConcurrentModificationException which was occurring when one of the locationAwares would instantiate and register a new locationAware before this loop was completed.
		for(LocationAware aware : awares)
			aware.onNewLocation(newLocation, wasSetAutomatically);
	}
	
	public static void setMenuTouchMode(boolean canSlide)
	{
		if(!twoPaneMode && activity != null)
			activity.getSlidingMenu().setTouchModeAbove(canSlide ? SlidingMenu.TOUCHMODE_FULLSCREEN : SlidingMenu.TOUCHMODE_NONE);
	}
	
	public static boolean isUserLoggedIn()
	{
		return user != null;
	}
	
	public static void logUserIn(UserDTO loggedInUser)
	{
		Log.d(AppState.logTag, "Logging user in with memberId: " + user);
		
		AppState.user = loggedInUser;
		AppData.setLoggedInUser(user);
		AppData.clearBeerListCache();
		AppData.clearBeerBuddyListCache();
		AppData.initializeNotifications();
		
		menuFragment.populateMenuOptions();
		AppState.goTo(MenuOption.BeerMenu);
		
		//menuFragment.onMenuItemSelected(MenuOption.BeerMenu);
	}
	public static void logUserOut(boolean sendToSignIn)
	{
		Log.d(AppState.logTag, "Logging user out.");
		
		AppState.user = null;
		AppData.setLoggedInUser(null);
		AppData.clearBeerListCache();
		AppData.clearBeerBuddyListCache();
		AppData.initializeNotifications();
		
		menuFragment.populateMenuOptions();
		if(sendToSignIn)
			AppState.goTo(MenuOption.SignIn);
		else
			menuFragment.highlightMenuItem(AppState.selectedMenuOption);
	}
	public static void refreshSpecificFragment(MenuOption fragment)
	{
		if(fragment == selectedMenuOption)
			refreshCurrentFragment();
	}
	public static void refreshCurrentFragment()
	{
		if(menuFragment != null)
			menuFragment.onMenuItemSelected(AppState.selectedMenuOption, true);
	}
	public static void goTo(MenuOption menuOption)
	{
		goTo(menuOption, true);
	}
	public static void goTo(MenuOption menuOption, boolean addToHistory)
	{
		if(addToHistory && menuOption != AppState.selectedMenuOption)
			historyStack.push(selectedMenuOption);
		
		menuFragment.onMenuItemSelected(menuOption);
	}
	
	//Check for Google Play Services
	public static boolean isGooglePlayAvailable(boolean allowPlayPopup)
	{
		int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);
		
		if(ConnectionResult.SUCCESS == resultCode)
		{
			Log.d(AppState.logTag, "Play Services is available.");
			return true;
		}
		else
		{
			Log.w(AppState.logTag, "Play Services is _NOT_ available!");
			
			if(allowPlayPopup)
			{
				Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(resultCode, activity, 1234);
				if(errorDialog != null)
                    errorDialog.show();
			}
			
			return false;
		}
	}

    //Load in-app billing inventory from Google Play Services
    public static void loadIabInventory()
    {
        Log.d(AppState.logTag, "Starting IAB");

        //Google recommends building this string on the fly for security
        final IabHelper helper = new IabHelper(context, billingKey1 + billingKey2 + billingKey3);
        helper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            @Override
            public void onIabSetupFinished(IabResult result) {
                Log.d(AppState.logTag, "Setup finished");

                if (!result.isSuccess()) {
                    Log.w(AppState.logTag, "IAB error! " + result);
                    return;
                }

                Log.d(AppState.logTag, "Asking for inventory...");
                helper.queryInventoryAsync(new IabHelper.QueryInventoryFinishedListener()
                {
                    @Override
                    public void onQueryInventoryFinished(IabResult result, Inventory inventory)
                    {
                        if(result.isFailure())
                        {
                            Log.w(AppState.logTag, "Failed to retrieve inventory! " + result);
                            return;
                        }

                        Log.d(AppState.logTag, "Inventory retrieved.");

                        boolean newDonated = false;
                        for(String sku : sku_unlock)
                        {
                            if(inventory.hasPurchase(sku))
                            {
                                newDonated = true;
                                break;
                            }
                        }

                        boolean dispose = true;    //If we need to consume something, don't dispose of the helper until after we are done consuming it.
                        for(String sku : sku_donate)
                        {
                            if(inventory.hasPurchase(sku))
                            {
                                dispose = false;
                                Log.d(AppState.logTag, "Found non-consumed donation. Consuming.");
                                helper.consumeAsync(inventory.getPurchase(sku), new IabHelper.OnConsumeFinishedListener()
                                {
                                    @Override
                                    public void onConsumeFinished(Purchase purchase, IabResult result)
                                    {
                                        helper.dispose();
                                    }
                                });
                            }
                        }

                        Log.d(AppState.logTag, "User has donated? " + newDonated);

                        if(newDonated != hasDonated)
                        {
                            refreshSpecificFragment(MenuOption.Buddies);    //If user is on the buddies screen and this value has changed, re-load that screen.
                            refreshSpecificFragment(MenuOption.About);
                        }

                        hasDonated = newDonated;

                        if(dispose)
                            helper.dispose();
                    }
                });
            }
        });
    }
}
