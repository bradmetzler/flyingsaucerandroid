package com.mellowelephant.saucer.util;

import com.mellowelephant.saucer.R;
import com.mellowelephant.saucer.fragment.content.AboutFragment;
import com.mellowelephant.saucer.fragment.content.BeerMenuFragment;
import com.mellowelephant.saucer.fragment.content.ContentFragment;
import com.mellowelephant.saucer.fragment.content.DrinkingBuddiesFragment;
import com.mellowelephant.saucer.fragment.content.FoodMenuFragment;
import com.mellowelephant.saucer.fragment.content.MyBeersFragment;
import com.mellowelephant.saucer.fragment.content.SettingsFragment;
import com.mellowelephant.saucer.fragment.content.SignInFragment;

public enum MenuOption
{
	MyBrews("My Beers", R.drawable.icon_mybeers_on, R.drawable.icon_mybeers_off, MyBeersFragment.class, true, false),
	Buddies("Drinking Buddies", R.drawable.icon_buddies_on, R.drawable.icon_buddies_off, DrinkingBuddiesFragment.class, true, false),
    //BuddiesColorTest("Color Test", R.drawable.icon_buddies_on, R.drawable.icon_buddies_off, DrinkingBuddiesColorTestFragment.class, true, false),
	BeerMenu("Beer Menu", R.drawable.icon_beer_on, R.drawable.icon_beer_off, BeerMenuFragment.class, true, true),
	FoodMenu("Food Menu", R.drawable.icon_food_on, R.drawable.icon_food_off, FoodMenuFragment.class, true, true),
	Settings("Settings", R.drawable.icon_settings_on, R.drawable.icon_settings_off, SettingsFragment.class, true, false),	//Currently, the only settings we have are for logged in users
	About("About", R.drawable.icon_about_on, R.drawable.icon_about_off, AboutFragment.class, true, true),
	SignIn("Sign In", R.drawable.icon_signin_on, R.drawable.icon_signin_off, SignInFragment.class, false, true);

	public static MenuOption defaultMenuOption = BeerMenu;
	
	public String menuText;
	public int iconResource;
	public int iconGrayResource;
	public Class<? extends ContentFragment> contentFragmentClass;
	public boolean visibleWhenLoggedIn;
	public boolean visibleWhenNotLoggedIn;

	MenuOption(String menuText, int iconResource, int iconGrayResource, Class<? extends ContentFragment> contentFragment, boolean visibleWhenLoggedIn, boolean visibleWhenNotLoggedIn)
	{
		this.menuText = menuText;
		this.iconGrayResource = iconGrayResource;
		this.iconResource = iconResource;
		this.contentFragmentClass = contentFragment;
		this.visibleWhenLoggedIn = visibleWhenLoggedIn;
		this.visibleWhenNotLoggedIn = visibleWhenNotLoggedIn;
	}
}