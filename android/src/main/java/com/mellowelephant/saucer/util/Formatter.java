package com.mellowelephant.saucer.util;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public abstract class Formatter
{
	public static String formatForDisplay(Date date)
	{
		if(date == null)
			return "";
		return SimpleDateFormat.getDateInstance().format(date);
	}
	
	public static String formatForDisplay(float number, int decimals)
	{
		String formatString = "#.";
		for(int i = 0; i < decimals; i++)
			formatString += "#";
		
		DecimalFormat format = new DecimalFormat(formatString);
		format.setRoundingMode(RoundingMode.HALF_UP);
		return format.format(number);
	}
	
	public static int dpToPx(int dp)
	{
		return (int)(dp * AppState.context.getResources().getDisplayMetrics().density + 0.5f);
	}
}
