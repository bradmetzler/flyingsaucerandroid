package com.mellowelephant.saucer.util;

import android.util.Log;

import com.mellowelephant.saucer.dto.BeerDTO;
import com.mellowelephant.saucer.dto.ContainerDTO;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

public class BeerFilter
{
	public boolean hideBeersIveHad;
	public String selectedNameFilter;
	public ArrayList<String> styleFilter;
	public ArrayList<String> breweryFilter;
	public ContainerDTO containerFilter;
	public Integer landedFilter;
	
	public enum Landed
	{
		//TODO String resources
		ALL(-1, "Any Time"),
		ZERO(0, "Today"),
		ONE(1, "Yesterday"),
		TWO(2, "2 days ago"),
		THREE(3, "3 days ago"),
		SEVEN(7, "A week ago"),
		FOURTEEN(14, "2 weeks ago"),
		THIRTYONE(31, "A month ago");
		
		public int number;
		public String text;
		
		private Landed(int number, String text)
		{
			this.number = number;
			this.text = text;
		}
		
		public static Landed byNumber(int number)
		{
			for(Landed value : values())
			{
				if(value.number == number)
					return value;
			}
			return ALL;
		}
		
		public static Landed byText(String text)
		{
			for(Landed value : values())
			{
				if(value.text == text)
					return value;
			}
			return ALL;
		}
	}
	
	public List<BeerDTO> applyFilters(List<BeerDTO> beers)
	{
		List<BeerDTO> filteredBeers = new ArrayList<BeerDTO>();
		if(beers == null || beers.isEmpty())
			return filteredBeers;
		
		Log.d(AppState.logTag, "Applying filters[" + hideBeersIveHad + ", " + selectedNameFilter + ", " + (styleFilter == null ? 0 : styleFilter.size()) + ", " + (breweryFilter == null ? 0 : breweryFilter.size()) + "]...");

		String nameFilter = (selectedNameFilter != null && !selectedNameFilter.trim().isEmpty()) ? selectedNameFilter.toUpperCase(Locale.ENGLISH) : null;
		
		Calendar today = new GregorianCalendar();
		today.set(Calendar.AM_PM, 0);
		today.set(Calendar.HOUR_OF_DAY, 0);
		today.set(Calendar.HOUR, 0);
		today.set(Calendar.MINUTE, 0);
		today.set(Calendar.SECOND, 0);
		today.set(Calendar.MILLISECOND, 0);
		
		for(BeerDTO beer : beers)
		{
			if(hideBeersIveHad && beer.hasBeenConsumed())
				continue;
			else if(nameFilter != null && !beer.getName().toUpperCase(Locale.ENGLISH).contains(nameFilter))
				continue;
			else if(styleFilter != null && !styleFilter.isEmpty() && !styleFilter.contains(beer.getStyle()))
				continue;
			else if(breweryFilter != null && !breweryFilter.isEmpty() && !breweryFilter.contains(beer.getBrewery()))
				continue;
			else if(containerFilter != null && containerFilter != beer.getContainer())
				continue;
			else if(landedFilter != null)
			{
				Calendar beerDay = new GregorianCalendar();
				beerDay.setTime(beer.getStatusSwapDate());
				beerDay.set(Calendar.AM_PM, 0);
				beerDay.set(Calendar.HOUR_OF_DAY, 0);
				beerDay.set(Calendar.HOUR, 0);
				beerDay.set(Calendar.MINUTE, 0);
				beerDay.set(Calendar.SECOND, 0);
				beerDay.set(Calendar.MILLISECOND, 0);
				
				int daysAgo = (int)((today.getTime().getTime() - beerDay.getTime().getTime()) / (1000 * 60 * 60 * 24));	//Approximate days. Only a couple of weird scenarios to worry about (DST changes) since we aren't using JodaTime
				
				if(daysAgo > landedFilter)
					continue;
			}
			
			filteredBeers.add(beer);
		}
		
		return filteredBeers;
	}
}
