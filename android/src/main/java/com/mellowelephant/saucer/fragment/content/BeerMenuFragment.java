package com.mellowelephant.saucer.fragment.content;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.mellowelephant.saucer.R;
import com.mellowelephant.saucer.adapter.BeerMenuArrayAdapter;
import com.mellowelephant.saucer.callback.BeerListListener;
import com.mellowelephant.saucer.callback.BeerModifiedListener;
import com.mellowelephant.saucer.data.AppData;
import com.mellowelephant.saucer.dto.BeerDTO;
import com.mellowelephant.saucer.dto.ContainerDTO;
import com.mellowelephant.saucer.dto.SaucerLocationDTO;
import com.mellowelephant.saucer.location.LocationAware;
import com.mellowelephant.saucer.util.AppState;
import com.mellowelephant.saucer.util.BeerFilter;
import com.mellowelephant.saucer.util.BeerFilter.Landed;
import com.mellowelephant.saucer.view.BeerDetailsDialog;
import com.mellowelephant.saucer.view.BeerFilterOptionsView;
import com.mellowelephant.saucer.view.BeerFilterOptionsView.FilterChangedListener;
import com.mellowelephant.saucer.view.BeerLongPressDialog;
import com.mellowelephant.saucer.view.BeerMeInstructionsDialog;
import com.mellowelephant.saucer.view.TabGroupView;
import com.mellowelephant.saucer.view.TabView;

public class BeerMenuFragment extends ContentFragment implements LocationAware, BeerModifiedListener
{
	private View view;
	private boolean isRunning;
	
	private TabGroupView tabs;
	private BeerFilter beerFilter = new BeerFilter();
	
	private List<BeerDTO> beers;
	private Map<String, Integer> styles;	//Style to number of beers with that style
	private Map<String, Integer> breweries;	//Brewery to number of beers from that brewery
	private Map<String, Integer> containers;//Container to number of beers with that container
	private Map<String, Integer> landeds;	//Time frames to number of beers with that time frame
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, final Bundle savedInstanceState)
	{
		AppState.registerAsLocationAware("Content", this);
		AppState.contentFragment = this;
		view = inflater.inflate(R.layout.beermenu_layout, null);
		
		if(!AppState.isUserLoggedIn())
			view.findViewById(R.id.hideBeersCheckbox).setVisibility(View.GONE);
		
		final BeerFilterOptionsView beerMenuFilterOptions = (BeerFilterOptionsView)view.findViewById(R.id.beerMenuFilterOptions);
		tabs = (TabGroupView)view.findViewById(R.id.beerMenuTabs);
		final TabView styleFilterTab = tabs.addTab(getResources().getString(R.string.filter_styles), getResources().getString(R.string.filter_all), new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				view.findViewById(R.id.beerMenuMainView).setVisibility(View.GONE);
				
				beerMenuFilterOptions.setVisibility(View.VISIBLE);
				beerMenuFilterOptions.setFilterOptions(BeerFilterOptionsView.FilterType.STYLES, styles, beerFilter.styleFilter, true);
			}
		});
		final TabView breweryFilterTab = tabs.addTab(getResources().getString(R.string.filter_breweries), getResources().getString(R.string.filter_all), new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				view.findViewById(R.id.beerMenuMainView).setVisibility(View.GONE);

				beerMenuFilterOptions.setVisibility(View.VISIBLE);
				beerMenuFilterOptions.setFilterOptions(BeerFilterOptionsView.FilterType.BREWERIES, breweries, beerFilter.breweryFilter, true);
			}
		});
		final TabView containerFilterTab = tabs.addTab(getResources().getString(R.string.filter_container), getResources().getString(R.string.filter_any), new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				view.findViewById(R.id.beerMenuMainView).setVisibility(View.GONE);
				
				beerMenuFilterOptions.setVisibility(View.VISIBLE);
				beerMenuFilterOptions.setFilterOptions(BeerFilterOptionsView.FilterType.CONTAINER, containers, null, false);
			}
		});
		final TabView landedFilterTab = tabs.addTab(getResources().getString(R.string.filter_landed), getResources().getString(R.string.filter_all), new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				view.findViewById(R.id.beerMenuMainView).setVisibility(View.GONE);
				
				beerMenuFilterOptions.setVisibility(View.VISIBLE);
				beerMenuFilterOptions.setFilterOptions(BeerFilterOptionsView.FilterType.LANDED, landeds, null, false);
			}
		});
		styleFilterTab.setOnLongClickListener(new OnLongClickListener()
		{
			@Override
			public boolean onLongClick(View v)
			{
				updateStyleFilter(null, styleFilterTab, beerMenuFilterOptions);
				return true;
			}
		});
		breweryFilterTab.setOnLongClickListener(new OnLongClickListener()
		{
			@Override
			public boolean onLongClick(View v)
			{
				updateBreweryFilter(null, breweryFilterTab, beerMenuFilterOptions);
				return true;
			}
		});
		containerFilterTab.setOnLongClickListener(new OnLongClickListener()
		{
			@Override
			public boolean onLongClick(View v)
			{
				updateContainerFilter(null, containerFilterTab, beerMenuFilterOptions);
				return true;
			}
		});
		landedFilterTab.setOnLongClickListener(new OnLongClickListener()
		{
			@Override
			public boolean onLongClick(View v)
			{
				updateLandedFilter(null, landedFilterTab, beerMenuFilterOptions);
				return true;
			}
		});

		CheckBox hideBeersCheckbox = (CheckBox)view.findViewById(R.id.hideBeersCheckbox);
		hideBeersCheckbox.setChecked(beerFilter.hideBeersIveHad);
		hideBeersCheckbox.setOnCheckedChangeListener(new OnCheckedChangeListener()
		{
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
			{
				beerFilter.hideBeersIveHad = isChecked;
				setBeers(beerFilter.applyFilters(beers));
			}
		});
		
		EditText beerFilterTextBox = (EditText)view.findViewById(R.id.beerFilterTextBox);
		if(beerFilter.selectedNameFilter != null)
			beerFilterTextBox.setText(beerFilter.selectedNameFilter);
		beerFilterTextBox.addTextChangedListener(new TextWatcher()
		{
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after){}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count){}

			@Override
			public void afterTextChanged(Editable s)
			{
				Log.w(AppState.logTag, "afterTextChanged()");
				if(getActivity() != null && beers != null)
				{
					String newValue = s.toString();
					if(!newValue.equals(beerFilter.selectedNameFilter))
					{
						beerFilter.selectedNameFilter = s.toString();
						setBeers(beerFilter.applyFilters(beers));
					}
				}
			}
		});
		
		beerMenuFilterOptions.setFilterChangedListener(new FilterChangedListener()
		{
			@Override
			public void onStyleFilterChanged(ArrayList<String> newStyleFilter)
			{
				updateStyleFilter(newStyleFilter, styleFilterTab, beerMenuFilterOptions);
			}

			@Override
			public void onBreweryFilterChanged(ArrayList<String> newBreweryFilter)
			{
				updateBreweryFilter(newBreweryFilter, breweryFilterTab, beerMenuFilterOptions);
			}

			@Override
			public void onContainerFilterChanged(ArrayList<String> newContainerFilter)
			{
				updateContainerFilter(newContainerFilter, containerFilterTab, beerMenuFilterOptions);
			}

			@Override
			public void onLandedFilterChanged(ArrayList<String> newLandedFilter)
			{
				updateLandedFilter(newLandedFilter, landedFilterTab, beerMenuFilterOptions);
			}	
		});
		
		applyTabText(styleFilterTab, beerFilter.styleFilter, R.string.filter_style, R.string.filter_styles);
		applyTabText(breweryFilterTab, beerFilter.breweryFilter, R.string.filter_brewery, R.string.filter_breweries);
		applyTabText(containerFilterTab, beerFilter.containerFilter == null ? null : Arrays.asList(new String[]{beerFilter.containerFilter.displayName}), R.string.filter_container, R.string.filter_container);
		applyTabText(landedFilterTab, beerFilter.landedFilter == null ? null : Arrays.asList(new String[]{BeerFilter.Landed.byNumber(beerFilter.landedFilter == null ? 0 : beerFilter.landedFilter).text}), R.string.filter_landed, R.string.filter_landed);
		
		return view;
	}
	
	private void updateStyleFilter(ArrayList<String> newStyleFilter, TabView styleFilterTab, BeerFilterOptionsView beerMenuFilterOptions)
	{
		beerFilter.styleFilter = (newStyleFilter == null || newStyleFilter.isEmpty()) ? null : newStyleFilter;
		
		tabs.unselectTab();
		view.findViewById(R.id.beerMenuMainView).setVisibility(View.VISIBLE);
		beerMenuFilterOptions.setVisibility(View.GONE);
		
		applyTabText(styleFilterTab, beerFilter.styleFilter, R.string.filter_style, R.string.filter_styles);
		setBeers(beerFilter.applyFilters(beers));
	}
	
	private void updateBreweryFilter(ArrayList<String> newBreweryFilter, TabView breweryFilterTab, BeerFilterOptionsView beerMenuFilterOptions)
	{
		beerFilter.breweryFilter = (newBreweryFilter == null || newBreweryFilter.isEmpty()) ? null : newBreweryFilter;
		
		tabs.unselectTab();
		view.findViewById(R.id.beerMenuMainView).setVisibility(View.VISIBLE);
		beerMenuFilterOptions.setVisibility(View.GONE);
		
		applyTabText(breweryFilterTab, beerFilter.breweryFilter, R.string.filter_brewery, R.string.filter_breweries);
		setBeers(beerFilter.applyFilters(beers));
	}
	
	private void updateContainerFilter(ArrayList<String> newContainerFilter, TabView containerFilterTab, BeerFilterOptionsView beerMenuFilterOptions)
	{
		beerFilter.containerFilter = (newContainerFilter == null || newContainerFilter.isEmpty()) ? null : ContainerDTO.fromDisplayName(newContainerFilter.get(0));
		
		tabs.unselectTab();
		view.findViewById(R.id.beerMenuMainView).setVisibility(View.VISIBLE);
		beerMenuFilterOptions.setVisibility(View.GONE);
		
		applyTabText(containerFilterTab, newContainerFilter, R.string.filter_container, R.string.filter_container);
		setBeers(beerFilter.applyFilters(beers));
	}
	
	private void updateLandedFilter(ArrayList<String> newLandedFilter, TabView landedFilterTab, BeerFilterOptionsView beerMenuFilterOptions)
	{
		beerFilter.landedFilter = (newLandedFilter == null || newLandedFilter.isEmpty()) ? null : BeerFilter.Landed.byText(newLandedFilter.get(0)).number;
		
		tabs.unselectTab();
		view.findViewById(R.id.beerMenuMainView).setVisibility(View.VISIBLE);
		beerMenuFilterOptions.setVisibility(View.GONE);
		
		applyTabText(landedFilterTab, newLandedFilter, R.string.filter_landed, R.string.filter_landed);
		setBeers(beerFilter.applyFilters(beers));
	}
	
	private void applyTabText(TabView whichTab, List<String> subTextItems, int singularTitle, int pluralTitle)
	{
		StringBuilder subtext = new StringBuilder();
		if(subTextItems == null || subTextItems.isEmpty())
			subtext.append(getResources().getString(R.string.filter_all));
		else
		{
			String subTextItem = subTextItems.get(0);
			subtext.append(subTextItem == null ? getResources().getString(R.string.filter_other) : subTextItem);
			if(subTextItems.size() > 1)
				subtext.append(" " + getResources().getString(R.string.filter_moreStart) + (subTextItems.size() - 1) + " " + getResources().getString(R.string.filter_moreEnd));
		}
		
		whichTab.setText((subTextItems == null || subTextItems.isEmpty() || subTextItems.size() > 1) ? getResources().getString(pluralTitle) : getResources().getString(singularTitle));
		whichTab.setSubtext(subtext.toString());
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
	{
		if(!AppState.isUserLoggedIn())
			return;
		
		MenuInflater menuInflater = getActivity().getMenuInflater();
		menuInflater.inflate(R.menu.beermenu_menu, menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		if(item.getItemId() == R.id.beerMenuMenuBeerMe)
		{
			beerMe();
			return true;
		}
		
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onStart()
	{
		super.onStart();
		setHasOptionsMenu(AppState.isUserLoggedIn());
		
		isRunning = true;
		
		
		if(AppState.currentLocation == null)
			clearBeers();
		else
			fetchBeers();
	}
	
	@Override
	public void onCreate(Bundle bundle)
	{
		super.onCreate(bundle);
		beerFilter = AppData.loadBeerFilter(this.getActivity().getApplicationContext());
		Log.w(AppState.logTag, "Load beer filter");
	}
	
	@Override
	public void onStop()
	{
		super.onDestroy();
		AppData.saveBeerFilter(beerFilter);
		Log.w(AppState.logTag, "Save beer filter");
	}
	
	@Override
	public void onPause()
	{
		super.onPause();
		isRunning = false;
	}
	
	@Override
	public void onResume()
	{
		super.onResume();
		isRunning = true;
	}
	
	@Override
	public void onDestroyView()
	{
		super.onDestroyView();
		AppState.unregisterAsLocationAware("Content");
	}
	
	private void beerMe()
	{
		if(AppData.beerMeInstructionsViewed())
		{
			if(beers != null && !beers.isEmpty())
			{
				//Suggest a beer
				BeerDTO recommendedBeer = null;
				for(BeerDTO beer : beers)
				{
					if(beer.hasBeenConsumed() || Boolean.TRUE.equals(beer.getPassedBeerMe()))
						continue;
					
					if(beer.getBeerMeWeight() != null)
					{
						if(recommendedBeer == null || recommendedBeer.getBeerMeWeight() == null || beer.getBeerMeWeight() > recommendedBeer.getBeerMeWeight())
							recommendedBeer = beer;
					}
				}
				
				if(recommendedBeer != null)
				{
					recommendedBeer.setPassedBeerMe(Boolean.TRUE);
					showBeerDetails(recommendedBeer);
				}
			}
		}
		else
		{
			//Show Explanation
			BeerMeInstructionsDialog instructions = new BeerMeInstructionsDialog();
			instructions.setTargetFragment(BeerMenuFragment.this, 0);
			instructions.setCallback(new BeerMeInstructionsDialog.Callback()
			{
				@Override
				public void onComplete()
				{
					//User closed the instructions. Go ahead and give a recommendation.
					beerMe();
				}
			});
			instructions.show(getFragmentManager(), "BeerMe");
		}
	}
	
	private void populateData()	//Called when beers are first retrieved
	{
		if(beers == null || beers.isEmpty())
			return;

		setBeers(beerFilter.applyFilters(beers));
	}
		
	private void setBeers(final List<BeerDTO> beers)
	{
		Log.d(AppState.logTag, "Setting " + beers.size() + " beers.");
		
		tabs.enable();
		
		ListView beerListView = (ListView)view.findViewById(R.id.beerList);
		view.findViewById(R.id.progressBar).setVisibility(View.GONE);
		
		if(beers.size() == 0)
		{
			//Nothing matches the filters. Show a message
			beerListView.setVisibility(View.GONE);
			
			TextView errorText = (TextView)view.findViewById(R.id.errorText); 
			errorText.setVisibility(View.VISIBLE);
			errorText.setText(getResources().getString(R.string.beerMenuNoMatches));
			
			return;
		}
		else
		{
			view.findViewById(R.id.errorText).setVisibility(View.GONE);
			beerListView.setVisibility(View.VISIBLE);
		}
		
		beerListView.setAdapter(new BeerMenuArrayAdapter(getActivity(), beers));
		beerListView.setOnItemClickListener(new OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int pos, long id)
			{
				showBeerDetails(beers.get(pos));
			}
		});
		beerListView.setOnItemLongClickListener(new OnItemLongClickListener()
		{
			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view, int pos, long id)
			{
				Bundle arguments = new Bundle();
				arguments.putSerializable("beer", beers.get(pos));
				
				BeerLongPressDialog dialog = new BeerLongPressDialog();
				dialog.setTargetFragment(BeerMenuFragment.this, 0);
				dialog.setArguments(arguments);
				dialog.show(getFragmentManager(), "BeerLongPress");
				
				return true;
			}
		});
	}
	private void showBeerDetails(BeerDTO beer)
	{
		Bundle arguments = new Bundle();
		arguments.putSerializable("beer", beer);
		
		BeerDetailsDialog dialog = new BeerDetailsDialog();
		dialog.setTargetFragment(BeerMenuFragment.this, 0);
		dialog.setArguments(arguments);
		dialog.show(getFragmentManager(), "BeerDetails");
	}
	private void clearBeers()
	{
		Log.d(AppState.logTag, "Clearing existing beers on the UI.");
		ListView beerListView = (ListView)view.findViewById(R.id.beerList);
		beerListView.setAdapter(null);
		beerListView.setVisibility(View.GONE);
		
		view.findViewById(R.id.beerMenuMainView).setVisibility(View.VISIBLE);
		view.findViewById(R.id.beerMenuFilterOptions).setVisibility(View.GONE);
		view.findViewById(R.id.errorText).setVisibility(View.GONE);
		view.findViewById(R.id.progressBar).setVisibility(View.VISIBLE);
		tabs.unselectTab();
		tabs.disable();
	}
	
	private void fetchBeers()
	{
		clearBeers();
		
		Log.d(AppState.logTag, "Asking for beers...");
		AppData.retrieveBeerList(new BeerListListener()
		{
			@Override
			public void onBeerListRetrieved(List<BeerDTO> beerList)
			{
				if(!isRunning)
				{
					Log.d(AppState.logTag, "Ignoring call to onBeerListRetrieved() because this fragment is no longer visible");
					return;
				}
				
				Log.d(AppState.logTag, "Retrieved " + beerList.size() + " beers.");
				int nullStyles = 0;
				int nullBreweries = 0;
				int nullContainers = 0;
				int nullLandeds = 0;
				
				styles = new TreeMap<String, Integer>(new FilterComparator());
				breweries = new TreeMap<String, Integer>(new FilterComparator());
				containers = new TreeMap<String, Integer>(new FilterComparator());
				landeds = new TreeMap<String, Integer>(new FilterComparatorLandeds());
				
				for(Landed value : BeerFilter.Landed.values())
					landeds.put(value.text, 0);
				
				Calendar today = new GregorianCalendar();
				today.set(Calendar.AM_PM, 0);
				today.set(Calendar.HOUR_OF_DAY, 0);
				today.set(Calendar.HOUR, 0);
				today.set(Calendar.MINUTE, 0);
				today.set(Calendar.SECOND, 0);
				today.set(Calendar.MILLISECOND, 0);
				Iterator<BeerDTO> beerIter = beerList.iterator();
				while(beerIter.hasNext())
				{
					BeerDTO beer = beerIter.next();
					if(!beer.isAtCurrentLocation())
					{
						beerIter.remove();	//For the beer menu, only want to show beers that are available at this location
						continue;
					}
					
					if(beer.getStyle() != null)
						styles.put(beer.getStyle(), styles.containsKey(beer.getStyle()) ? styles.get(beer.getStyle()) + 1 : 1);
					else
						nullStyles++;
					
					if(beer.getBrewery() != null)
						breweries.put(beer.getBrewery(), breweries.containsKey(beer.getBrewery()) ? breweries.get(beer.getBrewery()) + 1 : 1);
					else
						nullBreweries++;
					
					if(beer.getContainer() != null)
						containers.put(beer.getContainer().displayName, containers.containsKey(beer.getContainer().displayName) ? containers.get(beer.getContainer().displayName) + 1 : 1);
					else
						nullContainers++;
					
					if(beer.getStatusSwapDate() != null)
					{
						Calendar beerDay = new GregorianCalendar();
						beerDay.setTime(beer.getStatusSwapDate());
						beerDay.set(Calendar.AM_PM, 0);
						beerDay.set(Calendar.HOUR_OF_DAY, 0);
						beerDay.set(Calendar.HOUR, 0);
						beerDay.set(Calendar.MINUTE, 0);
						beerDay.set(Calendar.SECOND, 0);
						beerDay.set(Calendar.MILLISECOND, 0);
						
						int daysAgo = (int)((today.getTime().getTime() - beerDay.getTime().getTime()) / (1000 * 60 * 60 * 24));	//Approximate days. Only a couple of weird scenarios to worry about (DST changes) since we aren't using JodaTime
						
						for(Landed value : BeerFilter.Landed.values())
						{
							if(daysAgo <= value.number)
								landeds.put(value.text, landeds.containsKey(value.text) ? landeds.get(value.text) + 1 : 1);
						}
					}
					else
						nullLandeds++;
				}
				
				beers = beerList;
				
				styles.put(getResources().getString(R.string.filter_allStyles), beerList.size());
				breweries.put(getResources().getString(R.string.filter_allBreweries), beerList.size());
				containers.put(getResources().getString(R.string.filter_allContainers), beerList.size());
				landeds.put(BeerFilter.Landed.ALL.text, beerList.size());
				
				if(nullStyles > 0)
					styles.put(getResources().getString(R.string.filter_other), nullStyles);
				if(nullBreweries > 0)
					breweries.put(getResources().getString(R.string.filter_other), nullBreweries);
				if(nullContainers > 0)
					containers.put(getResources().getString(R.string.filter_other), nullContainers);
				if(nullLandeds > 0)
					landeds.put(getResources().getString(R.string.filter_other), nullLandeds);
				
				populateData();
			}

			@Override
			public void onBeerListError()
			{
				if(!isRunning)
				{
					Log.d(AppState.logTag, "Ignoring call to onBeerListError() because this fragment is no longer visible");
					return;
				}
				
				beers = null;
				styles = null;
				breweries = null;
				clearBeers();
				
				view.findViewById(R.id.beerList).setVisibility(View.GONE);
				view.findViewById(R.id.progressBar).setVisibility(View.GONE);
				
				TextView errorText = (TextView)view.findViewById(R.id.errorText);
				errorText.setText(getResources().getString(R.string.beerMenuNetworkError));
				errorText.setVisibility(View.VISIBLE);
			}
			
			@Override
			public void onInvalidCredentials()
			{
				if(!isRunning)
				{
					Log.d(AppState.logTag, "Ignoring call to onInvalidCredentials() because this fragment is no longer visible");
					return;
				}
				
				promptForNewCredentials();
			}
		});
	}

	@Override
	public void onNewLocation(SaucerLocationDTO location, boolean wasSetAutomatically)
	{
		if(location == null)
			clearBeers();
		else
			fetchBeers();
	}

	@Override
	public void onBeerModified(BeerDTO beer)	//Called when a beer is modified via a dialog (removed, added as pending, comment/rating changed). Need to re-draw the beer list.
	{
		if(!isRunning)
			return;
		
		ListView beerListView = (ListView)view.findViewById(R.id.beerList);
		if(beerListView == null)
			return;
		
		//Only thing that can change on this screen will be adding/removing a checkmark for pending beer. Just try to find the modified beer in the visible list, and update the view if we find it.
		int firstVisiblePosition = beerListView.getFirstVisiblePosition();
		for(int i = firstVisiblePosition, j = beerListView.getLastVisiblePosition(); i<=j; i++)
		{
			if(beer == beerListView.getItemAtPosition(i))	//Yea, pointer comparison. Since we pass it into the edit dialog, should be the same object.
			{
				Log.d(AppState.logTag, "Found modified beer at index " + i);
				beerListView.getAdapter().getView(i, beerListView.getChildAt(i - firstVisiblePosition), beerListView);
				break;
			}
		}
	}

	@Override
	public boolean handleBackButton()
	{
		if(tabs.unselectTab())	//Attempt to unselect the filter tabs. If this returns true, it means we were on a filter screen previously. Therefore, we can handle the back button.
		{
			view.findViewById(R.id.beerMenuMainView).setVisibility(View.VISIBLE);
			view.findViewById(R.id.beerMenuFilterOptions).setVisibility(View.GONE);
			return true;
		}
		
		return false;
	}
	
	private class FilterComparator implements Comparator<String>
	{
		@Override
		public int compare(String lhs, String rhs)
		{
			//TODO String resources
			if(lhs.equals("All Styles") || lhs.equals("All Breweries") || lhs.equals("Any Container") || lhs.equals(BeerFilter.Landed.ALL.text))
				return -1;
			if(rhs.equals("Other"))
				return -1;
			if(rhs.equals("All Styles") || rhs.equals("All Breweries") || rhs.equals("Any Container") || rhs.equals(BeerFilter.Landed.ALL.text))
				return 1;
			if(lhs.equals("Other"))
				return 1;
			
			return lhs.compareTo(rhs);
		}
	}
	private class FilterComparatorLandeds implements Comparator<String>
	{
		@Override
		public int compare(String lhs, String rhs)
		{
			return BeerFilter.Landed.byText(lhs).number - BeerFilter.Landed.byText(rhs).number;
		}
	}
}