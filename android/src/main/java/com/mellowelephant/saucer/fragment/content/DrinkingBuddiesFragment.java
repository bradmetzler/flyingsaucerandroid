package com.mellowelephant.saucer.fragment.content;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.mellowelephant.saucer.R;
import com.mellowelephant.saucer.adapter.DrinkingBuddyArrayAdapter;
import com.mellowelephant.saucer.callback.BeerBuddyActionListener;
import com.mellowelephant.saucer.callback.BeerBuddyListListener;
import com.mellowelephant.saucer.data.AppData;
import com.mellowelephant.saucer.dto.BuddyDTO;
import com.mellowelephant.saucer.dto.BuddyStatusDTO;
import com.mellowelephant.saucer.util.AppState;
import com.mellowelephant.saucer.util.BuddyComparator;
import com.mellowelephant.saucer.view.BuddyInviteDialog;
import com.mellowelephant.saucer.view.BuddyLongPressDialog;
import com.mellowelephant.saucer.view.BuddyNotFoundDialog;
import com.mellowelephant.saucer.view.BuddyRespondDialog;

import java.util.Collections;
import java.util.List;

public class DrinkingBuddiesFragment extends ContentFragment implements BeerBuddyActionListener
{
	private static final int MANUALREFRESHES_MAX = 10;
	private static final int MANUALREFRESHES_WARN = 7;
	
	private View view;
	private boolean isRunning;
	
	private List<BuddyDTO> buddies;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		AppState.contentFragment = this;
		view = inflater.inflate(R.layout.drinkingbuddies_layout, null);
		
		((Button)view.findViewById(R.id.drinkingBuddiesCompareBeersButton)).setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				AppState.contentFragment = new DrinkingBuddiesCompareFragment();
				getFragmentManager().beginTransaction().replace(R.id.main_content_frame, AppState.contentFragment).commit();
			}
		});
		
		return view;
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
	{
		MenuInflater menuInflater = getActivity().getMenuInflater();
		menuInflater.inflate(R.menu.drinkingbuddies_menu, menu);
	}
	
	@Override
	public void onPrepareOptionsMenu(Menu menu)
	{
		int items = menu.size();
		for(int i = 0; i < items; i++)
		{
			MenuItem item = menu.getItem(i);
			if(item.getItemId() == R.id.drinkingBuddiesMenuRefreshBuddy)
			{
				boolean enabled = (AppData.getNumberOfManualBuddyListRefreshes() < MANUALREFRESHES_MAX);
				
				item.setEnabled(enabled);
				item.setVisible(enabled);
				break;
			}
		}
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		if(item.getItemId() == R.id.drinkingBuddiesMenuInviteBuddy)
		{
			inviteBuddy();
			return true;
		}
		if(item.getItemId() == R.id.drinkingBuddiesMenuRefreshBuddy)
		{
			refreshBuddyList();
			return true;
		}
		
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onStart()
	{
		super.onStart();
		setHasOptionsMenu(true);
		
		isRunning = true;
		
		fetchBuddies();
	}
	
	@Override
	public void onPause()
	{
		super.onPause();

		Log.w(AppState.logTag, "onPause");
		
		isRunning = false;
	}
	
	@Override
	public void onResume()
	{
		super.onResume();
		
		Log.w(AppState.logTag, "onResume");
		
		isRunning = true;
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState)
	{
		super.onSaveInstanceState(outState);
	}
	
	private void inviteBuddy()
	{
		BuddyInviteDialog dialog = new BuddyInviteDialog();
		dialog.setTargetFragment(DrinkingBuddiesFragment.this, 0);
		dialog.show(getFragmentManager(), "BuddyInvite");
	}
	
	private void refreshBuddyList()
	{
		int numRefreshes = AppData.getNumberOfManualBuddyListRefreshes();
		if(numRefreshes < MANUALREFRESHES_MAX)
		{
			AppData.setNumberOfManualBuddyListRefreshes(++numRefreshes);
			AppData.clearBeerBuddyListCache();
			
			if(numRefreshes >= MANUALREFRESHES_WARN && numRefreshes < MANUALREFRESHES_MAX)
				Toast.makeText(getActivity(), getResources().getString(R.string.drinkingBuddiesTooManyRefreshesWarn), Toast.LENGTH_LONG).show();
			
			fetchBuddies();
		}
		
		if(numRefreshes >= MANUALREFRESHES_MAX)	//Not else, b/c we still want to hit this if we incremented it to max above
			Toast.makeText(getActivity(), getResources().getString(R.string.drinkingBuddiesTooManyRefreshes), Toast.LENGTH_LONG).show();
	}
	
	private void clearBuddies()
	{
		Log.d(AppState.logTag, "Clearing existing buddies on the UI.");
		ListView buddyListView = (ListView)view.findViewById(R.id.drinkingBuddiesBuddyList);
		buddyListView.setAdapter(null);
		buddyListView.setVisibility(View.GONE);
		
		view.findViewById(R.id.drinkingBuddiesSadPanda).setVisibility(View.GONE);
		view.findViewById(R.id.errorText).setVisibility(View.GONE);
		view.findViewById(R.id.progressBar).setVisibility(View.VISIBLE);
		((Button)view.findViewById(R.id.drinkingBuddiesCompareBeersButton)).setEnabled(false);
	}
	
	private void fetchBuddies()
	{
		clearBuddies();
		//enableDisableRefreshButton();
		
		getActivity().invalidateOptionsMenu();	//Re-create options menu (to disable the refresh button if necessary)
		
		Log.d(AppState.logTag, "Asking for buddies...");
		AppData.retrieveBeerBuddyList(new BeerBuddyListListener()
		{
			@Override
			public void onBeerBuddyListRetrieved(List<BuddyDTO> buddyList)
			{
				if(!isRunning)
				{
					Log.d(AppState.logTag, "Ignoring call to onBeerBuddyListRetrieved() because this fragment is no longer visible");
					return;
				}
				
				Log.d(AppState.logTag, "Retrieved " + buddyList.size() + " buddies.");
				
				buddies = buddyList;
				
				populateData();
			}

			@Override
			public void onBeerBuddyListError()
			{
				if(!isRunning)
				{
					Log.d(AppState.logTag, "Ignoring call to onBeerBuddyListError() because this fragment is no longer visible");
					return;
				}
				
				buddies = null;
				clearBuddies();
				
				view.findViewById(R.id.drinkingBuddiesSadPanda).setVisibility(View.GONE);
				view.findViewById(R.id.drinkingBuddiesBuddyList).setVisibility(View.GONE);
				view.findViewById(R.id.progressBar).setVisibility(View.GONE);
				
				TextView errorText = (TextView)view.findViewById(R.id.errorText);
				errorText.setText(getResources().getString(R.string.drinkingBuddiesNetworkError));
				errorText.setVisibility(View.VISIBLE);
			}
			
			@Override
			public void onInvalidCredentials()
			{
				if(!isRunning)
				{
					Log.d(AppState.logTag, "Ignoring call to onInvalidCredentials() because this fragment is no longer visible");
					return;
				}
				
				promptForNewCredentials();
			}
		});
	}
	
	private void populateData()	//Called when buddies are first retrieved
	{
		if(buddies == null)
			return;

		setBuddies(buddies);
	}
	
	private void setBuddies(final List<BuddyDTO> buddies)
	{
		Log.d(AppState.logTag, "Setting " + buddies.size() + " buddies.");
		
		ListView buddyListView = (ListView)view.findViewById(R.id.drinkingBuddiesBuddyList);
		view.findViewById(R.id.progressBar).setVisibility(View.GONE);
		view.findViewById(R.id.errorText).setVisibility(View.GONE);
		
		if(buddies.size() == 0)
		{
			//No buddies. Show sad panda
			buddyListView.setVisibility(View.GONE);
			view.findViewById(R.id.drinkingBuddiesSadPanda).setVisibility(View.VISIBLE);

			return;
		}
		else
		{
			view.findViewById(R.id.drinkingBuddiesSadPanda).setVisibility(View.GONE);
			view.findViewById(R.id.errorText).setVisibility(View.GONE);
			buddyListView.setVisibility(View.VISIBLE);
			
			((Button)view.findViewById(R.id.drinkingBuddiesCompareBeersButton)).setEnabled(numAcceptedBuddies(buddies) > 0);
		}
		
		Collections.sort(buddies, new BuddyComparator());
		
		buddyListView.setAdapter(new DrinkingBuddyArrayAdapter(getActivity(), buddies));
		buddyListView.setOnItemClickListener(new OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int pos, long id)
			{
				BuddyDTO buddy = buddies.get(pos);
				
				if(buddy.getStatus() == BuddyStatusDTO.PENDING)
				{
					Bundle arguments = new Bundle();
					arguments.putSerializable("buddy", buddy);
					
					BuddyRespondDialog dialog = new BuddyRespondDialog();
					dialog.setTargetFragment(DrinkingBuddiesFragment.this, 0);
					dialog.setArguments(arguments);
					dialog.show(getFragmentManager(), "BuddyRespond");
				}
			}
		});
		buddyListView.setOnItemLongClickListener(new OnItemLongClickListener()
		{
			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view, int pos, long id)
			{
				BuddyDTO buddy = buddies.get(pos);
				
				if(buddy.getStatus() == BuddyStatusDTO.ACCEPTED || buddy.getStatus() == BuddyStatusDTO.REQUESTED)
				{
					Bundle arguments = new Bundle();
					arguments.putSerializable("buddy", buddy);

					BuddyLongPressDialog dialog = new BuddyLongPressDialog();
					dialog.setTargetFragment(DrinkingBuddiesFragment.this, 0);
					dialog.setArguments(arguments);
					dialog.show(getFragmentManager(), "BuddyLongPress");
					
					return true;
				}
				
				return false;
			}
		});
	}
	
	private int numAcceptedBuddies(List<BuddyDTO> buddies)
	{
		if(buddies == null || buddies.isEmpty())
			return 0;
		
		int counter = 0;
		for(BuddyDTO buddy : buddies)
		{
			if(buddy.getStatus() == BuddyStatusDTO.ACCEPTED)
				counter++;
		}
		
		return counter;
	}

	@Override
	public boolean handleBackButton()
	{
		return false;
	}

    @Override
    public void onBeerBuddyActionSuccess()
    {
        if(!isRunning)
        {
            Log.d(AppState.logTag, "Ignoring call to onBeerBuddyActionSuccess() because this fragment is no longer visible");
            return;
        }

        fetchBuddies();	//Refresh buddy list to display the newly-invited buddy
    }

    @Override
    public void onBeerBuddyActionError()
    {
        if(!isRunning)
        {
            Log.d(AppState.logTag, "Ignoring call to onBeerBuddyActionError() because this fragment is no longer visible");
            return;
        }

        Log.d(AppState.logTag, "onBeerBuddyActionError(): Displaying error dialog...");
        BuddyNotFoundDialog instructions = new BuddyNotFoundDialog();
        instructions.setTargetFragment(DrinkingBuddiesFragment.this, 0);
        instructions.show(getFragmentManager(), "BuddyNotFound");
    }

    @Override
    public void onInvalidCredentials()
    {
        if(!isRunning)
        {
            Log.d(AppState.logTag, "Ignoring call to onInvalidCredentials() because this fragment is no longer visible");
            return;
        }

        promptForNewCredentials();
    }
}
