package com.mellowelephant.saucer.fragment.content;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.FileProvider;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.ShareActionProvider;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.mellowelephant.saucer.MEG;
import com.mellowelephant.saucer.R;
import com.mellowelephant.saucer.adapter.MyBeersArrayAdapter;
import com.mellowelephant.saucer.callback.BeerListListener;
import com.mellowelephant.saucer.callback.BeerModifiedListener;
import com.mellowelephant.saucer.data.AppData;
import com.mellowelephant.saucer.dto.BeerDTO;
import com.mellowelephant.saucer.dto.SaucerLocationDTO;
import com.mellowelephant.saucer.location.LocationAware;
import com.mellowelephant.saucer.util.AppState;
import com.mellowelephant.saucer.util.BeerComparatorDateAscending;
import com.mellowelephant.saucer.util.BeerComparatorDateDescending;
import com.mellowelephant.saucer.util.BeerComparatorNameAscending;
import com.mellowelephant.saucer.util.BeerComparatorRatingDescending;
import com.mellowelephant.saucer.util.Formatter;
import com.mellowelephant.saucer.view.BeerDetailsDialog;
import com.mellowelephant.saucer.view.BeerLongPressDialog;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

public class MyBeersFragment extends ContentFragment implements LocationAware, BeerModifiedListener
{
	private View view;
    private MenuItem shareMenuItem;
	private boolean isRunning;
	
	private List<BeerDTO> beers;
	private float avgRating;
	private Date projectedFinish;
	private final int targetBeers = 200;
	
	private int orderByIndex = 2;	//Default to "Newest First"
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, final Bundle savedInstanceState)
	{
		AppState.registerAsLocationAware("Content", this);
		AppState.contentFragment = this;
		view = inflater.inflate(R.layout.mybeers_layout, null);

		if(savedInstanceState != null)
			orderByIndex = savedInstanceState.getInt("orderByIndex", orderByIndex);
		
		Spinner orderOptionsSpinner = (Spinner)view.findViewById(R.id.myBeersOrderBySpinner);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, new String[]{"Alphabetical", "Highest Rated First", "Newest First", "Oldest First"});
		orderOptionsSpinner.setAdapter(adapter);
		orderOptionsSpinner.setOnItemSelectedListener(new OnItemSelectedListener()
		{
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int pos, long id)
			{
				if(orderByIndex != pos)
				{
					orderByIndex = pos;
					sortBeers();
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent)
			{
				onItemSelected(parent, null, 0, 0);
			}
		});
		orderOptionsSpinner.setSelection(orderByIndex);

		return view;
	}
	
	@Override
	public void onStart()
	{
		super.onStart();
		setHasOptionsMenu(true);
		
		isRunning = true;
		
		if(AppState.currentLocation == null)
			clearBeers();
		else
			fetchBeers();
	}
	
	@Override
	public void onPause()
	{
		super.onPause();
		
		isRunning = false;
	}
	
	@Override
	public void onResume()
	{
		super.onResume();
		
		isRunning = true;
	}
	
	@Override
	public void onDestroyView()
	{
		super.onDestroyView();
		AppState.unregisterAsLocationAware("Content");
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState)
	{
		super.onSaveInstanceState(outState);
		
		outState.putInt("orderByIndex", orderByIndex);
	}
	
	private void populateData()	//Called when beers are first retrieved
	{
		if(beers == null || beers.isEmpty())
		{
			view.findViewById(R.id.beerList).setVisibility(View.GONE);
			view.findViewById(R.id.progressBar).setVisibility(View.GONE);
			
			TextView errorText = (TextView)view.findViewById(R.id.errorText);
			errorText.setText(getResources().getString(R.string.myBeersNoBeers));
			errorText.setVisibility(View.VISIBLE);
			
			return;
		}

        if(shareMenuItem != null)
        {
            shareMenuItem.setEnabled(true);
            getActivity().invalidateOptionsMenu();
        }

		calculateStats();
		sortBeers();
	}
	
	private void calculateStats()
	{
        if(beers == null || beers.isEmpty())
		{
			//Clear and hide stats
			avgRating = 0;
			projectedFinish = null;
			
			view.findViewById(R.id.myBeersBeerStatsNumBeers).setVisibility(View.INVISIBLE);
			view.findViewById(R.id.myBeersBeerStatsProgress).setVisibility(View.INVISIBLE);
			view.findViewById(R.id.myBeersBeerStatsAvgRating).setVisibility(View.INVISIBLE);
			view.findViewById(R.id.myBeersBeerStatsProjectedFinish).setVisibility(View.INVISIBLE);
			
			((TextView)view.findViewById(R.id.myBeersBeerStatsNumBeers)).setText("0");
			((TextView)view.findViewById(R.id.myBeersBeerStatsProgress)).setText("0%");
			((TextView)view.findViewById(R.id.myBeersBeerStatsAvgRating)).setText(0);
			((TextView)view.findViewById(R.id.myBeersBeerStatsProjectedFinish)).setText("");
			
			return;
		}
		
		Date earliestBeer = null;
		int ratingSum = 0;
		int numRatedBeers = 0;
		for(BeerDTO beer : beers)
		{
			if(earliestBeer == null || beer.getDateConsumed().before(earliestBeer))
				earliestBeer = beer.getDateConsumed();
			
			if(beer.getRating() != null)
			{
				ratingSum += beer.getRating();
				numRatedBeers++;
			}
		}
		
		//Calculate avg rating
		avgRating = (float)ratingSum / numRatedBeers;
		
		//Calculate projected finish date
		if(beers.size() == targetBeers)
		{
			((TextView)view.findViewById(R.id.myBeersStatsProjectedFinishHeader)).setText(getResources().getString(R.string.myBeersBeerStatsFinish));
			
			//User already reached target. Find the last beer they drank and call it the finish date
			BeerDTO lastBeer = null;
			for(BeerDTO beer : beers)
			{
				if(lastBeer == null || beer.getDateConsumed().after(lastBeer.getDateConsumed()))
					lastBeer = beer;
			}
			projectedFinish = lastBeer.getDateConsumed();
		}
		else
		{
			((TextView)view.findViewById(R.id.myBeersStatsProjectedFinishHeader)).setText(getResources().getString(R.string.myBeersBeerStatsProjectedFinish));
			
			int numDaysSinceStart = (int)Math.ceil((new Date().getTime() - earliestBeer.getTime()) / (1000 * 60 * 60 * 24));
			if(numDaysSinceStart == 0)
				numDaysSinceStart = 1;	//Without this, on day 1, Projected Finish Date was showing as current day.
			
			float beersPerDay = (float)beers.size() / numDaysSinceStart;
			int days0To200 = (int)Math.ceil(targetBeers / beersPerDay);
			GregorianCalendar gonnaBeDone = new GregorianCalendar();
			gonnaBeDone.setTime(earliestBeer);
			gonnaBeDone.add(Calendar.DAY_OF_YEAR, days0To200);
			projectedFinish = gonnaBeDone.getTime();
		}
		
		//Show stats
		view.findViewById(R.id.myBeersBeerStatsNumBeers).setVisibility(View.VISIBLE);
		view.findViewById(R.id.myBeersBeerStatsProgress).setVisibility(View.VISIBLE);
		view.findViewById(R.id.myBeersBeerStatsAvgRating).setVisibility(View.VISIBLE);
		view.findViewById(R.id.myBeersBeerStatsProjectedFinish).setVisibility(View.VISIBLE);
		
		((TextView)view.findViewById(R.id.myBeersBeerStatsNumBeers)).setText(String.valueOf(beers.size()));
		((TextView)view.findViewById(R.id.myBeersBeerStatsProgress)).setText(Formatter.formatForDisplay(((beers.size() / (float)targetBeers) * 100), 1) + "%");
		((TextView)view.findViewById(R.id.myBeersBeerStatsAvgRating)).setText(Formatter.formatForDisplay(avgRating, 3));
		((TextView)view.findViewById(R.id.myBeersBeerStatsProjectedFinish)).setText(Formatter.formatForDisplay(projectedFinish));
	}
	
	private void sortBeers()
	{
		if(beers == null || beers.isEmpty())
			return;
		
		Log.d(AppState.logTag, "Sorting beers[" + orderByIndex + "]...");
		
		Comparator<BeerDTO> comparator = null;
		
		switch(orderByIndex)
		{
			case 0:
				comparator = new BeerComparatorNameAscending();
				break;
			case 1:
				comparator = new BeerComparatorRatingDescending();
				break;
			case 2:
				comparator = new BeerComparatorDateDescending();
				break;
			case 3:
				comparator = new BeerComparatorDateAscending();
				break;
		}
		
		List<BeerDTO> sortedBeers = new ArrayList<BeerDTO>(beers);
		Collections.sort(sortedBeers, comparator);
		
		setBeers(sortedBeers);
	}
	
	private void setBeers(final List<BeerDTO> beers)
	{
		Log.d(AppState.logTag, "Setting " + beers.size() + " beers.");
		
		ListView beerListView = (ListView)view.findViewById(R.id.beerList);
		view.findViewById(R.id.progressBar).setVisibility(View.GONE);
		view.findViewById(R.id.errorText).setVisibility(View.GONE);
		beerListView.setVisibility(View.VISIBLE);

		MyBeersArrayAdapter adapter = new MyBeersArrayAdapter(getActivity(), beers);
		beerListView.setAdapter(adapter);
		beerListView.setOnItemClickListener(new OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int pos, long id)
			{
				Bundle arguments = new Bundle();
				arguments.putSerializable("beer", beers.get(pos));
				
				BeerDetailsDialog dialog = new BeerDetailsDialog();
				dialog.setTargetFragment(MyBeersFragment.this, 0);
				dialog.setArguments(arguments);
				dialog.show(getFragmentManager(), "BeerDetails");
			}
		});
		beerListView.setOnItemLongClickListener(new OnItemLongClickListener()
		{
			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view, int pos, long id)
			{
				Bundle arguments = new Bundle();
				arguments.putSerializable("beer", beers.get(pos));
				
				BeerLongPressDialog dialog = new BeerLongPressDialog();
				dialog.setTargetFragment(MyBeersFragment.this, 0);
				dialog.setArguments(arguments);
				dialog.show(getFragmentManager(), "BeerRemove");
				
				return true;
			}
		});
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
	{
        MenuInflater menuInflater = getActivity().getMenuInflater();
        menuInflater.inflate(R.menu.mybeers_menu, menu);

        shareMenuItem = menu.findItem(R.id.myBeersMenuShare);
        shareMenuItem.setEnabled(beers != null && !beers.isEmpty());

        ShareActionProvider shareProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(shareMenuItem);
        shareProvider.setShareIntent(createExportShareIntent());
        shareProvider.setOnShareTargetSelectedListener(new ShareActionProvider.OnShareTargetSelectedListener()
        {
            @Override
            public boolean onShareTargetSelected(ShareActionProvider source, Intent intent)
            {
                createExportFile();
                return false;
            }
        });
	}
	
	private Intent createExportShareIntent()
	{
		Intent shareIntent = null;
		
		try
		{
			File exportPath = new File(getActivity().getFilesDir(), "export");
			File exportFile = new File(exportPath, "BeerListExport.html");
			exportPath.mkdirs();
			exportFile.createNewFile();
			
			Uri contentUri = FileProvider.getUriForFile(getActivity(), "com.mellowelephant.saucer.fileprovider", exportFile);
			
			shareIntent = new Intent();
			shareIntent.setAction(Intent.ACTION_SEND);
			shareIntent.putExtra(Intent.EXTRA_STREAM, contentUri);
			shareIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
			shareIntent.setType("text/html");
		}
		catch(Exception e)
		{
			Log.e(AppState.logTag, "Exception in createExportShareIntent()", e);
		}
		
		return shareIntent;
	}
	
	private void createExportFile()
	{
		try
		{
			//TODO Test this. Javascript may need updating because of object changes.
			File exportPath = new File(getActivity().getFilesDir(), "export");
			File exportFile = new File(exportPath, "BeerListExport.html");
			exportPath.mkdirs();
			PrintWriter exportWriter = new PrintWriter(exportFile);
			
			BufferedReader templatePart1 = new BufferedReader(new InputStreamReader(getResources().openRawResource(R.raw.beerlistexporttemplate_part1)));
			BufferedReader templatePart2 = new BufferedReader(new InputStreamReader(getResources().openRawResource(R.raw.beerlistexporttemplate_part2)));
			
			String line;
			while((line = templatePart1.readLine()) != null)
				exportWriter.append(line);
			
			exportWriter.append("\nvar beers = " + MEG.toJson(beers) + ";\n");
			exportWriter.append("\nvar fullname = \"" + AppState.user.getFullName() + "\";\n");
			exportWriter.append("\nvar numBeers = " + (beers == null ? 0 : beers.size()) + ";\n");
			exportWriter.append("\nvar progress = " + Formatter.formatForDisplay(((beers.size() / (float)targetBeers) * 100), 1) + ";\n");
			exportWriter.append("\nvar avgRating = " + Formatter.formatForDisplay(avgRating, 3) + ";\n");
			exportWriter.append("\nvar projFinish = \"" + Formatter.formatForDisplay(projectedFinish) + "\";\n");
			
			while((line = templatePart2.readLine()) != null)
				exportWriter.append(line);
			
			templatePart1.close();
			templatePart2.close();
			exportWriter.close();
		}
		catch(Exception e)
		{
			Log.e(AppState.logTag, "Exception in buildExportFile()", e);
		}
	}
	
	private void clearBeers()
	{
		beers = null;
        if(shareMenuItem != null)
        {
            shareMenuItem.setEnabled(false);
            getActivity().invalidateOptionsMenu();
        }
		
		Log.d(AppState.logTag, "Clearing existing beers on the UI.");
		ListView beerListView = (ListView)view.findViewById(R.id.beerList);
		beerListView.setAdapter(null);
		beerListView.setVisibility(View.GONE);
		
		view.findViewById(R.id.myBeersBeerStatsNumBeers).setVisibility(View.INVISIBLE);
		view.findViewById(R.id.myBeersBeerStatsProgress).setVisibility(View.INVISIBLE);
		view.findViewById(R.id.myBeersBeerStatsAvgRating).setVisibility(View.INVISIBLE);
		view.findViewById(R.id.myBeersBeerStatsProjectedFinish).setVisibility(View.INVISIBLE);
		
		view.findViewById(R.id.errorText).setVisibility(View.GONE);
		view.findViewById(R.id.progressBar).setVisibility(View.VISIBLE);
	}
	
	private void fetchBeers()
	{
		clearBeers();
		
		Log.d(AppState.logTag, "Asking for beers...");
		AppData.retrieveBeerList(new BeerListListener()
		{
			@Override
			public void onBeerListRetrieved(List<BeerDTO> beerList)
			{
				if(!isRunning)
				{
					Log.d(AppState.logTag, "Ignoring call to onBeerListRetrieved() because this fragment is no longer visible");
					return;
				}
				
				Log.d(AppState.logTag, "Retrieved " + beerList.size() + " beers.");
				
				Iterator<BeerDTO> beerIter = beerList.iterator();
				while(beerIter.hasNext())
				{
					BeerDTO beer = beerIter.next();
					if(!beer.hasBeenConsumed())
						beerIter.remove();	//For My Beers, only want to show beers that the user has tasted
				}
					
				beers = beerList;
				populateData();
			}

			@Override
			public void onBeerListError()
			{
				if(!isRunning)
				{
					Log.d(AppState.logTag, "Ignoring call to onBeerListError() because this fragment is no longer visible");
					return;
				}
				
				beers = null;
				clearBeers();
				
				view.findViewById(R.id.beerList).setVisibility(View.GONE);
				view.findViewById(R.id.progressBar).setVisibility(View.GONE);
				
				TextView errorText = (TextView)view.findViewById(R.id.errorText);
				errorText.setText(getResources().getString(R.string.beerMenuNetworkError));
				errorText.setVisibility(View.VISIBLE);
			}
			
			@Override
			public void onInvalidCredentials()
			{
				if(!isRunning)
				{
					Log.d(AppState.logTag, "Ignoring call to onInvalidCredentials() because this fragment is no longer visible");
					return;
				}
				
				promptForNewCredentials();
			}
		});
	}

	@Override
	public void onNewLocation(SaucerLocationDTO location, boolean wasSetAutomatically)
	{
		if(location == null)
			clearBeers();
		else
			fetchBeers();
	}

	@Override
	public void onBeerModified(BeerDTO beer)	//Called when a beer is modified via a dialog (removed, added as pending, comment/rating changed). Need to re-draw the beer list.
	{
		if(!isRunning)
			return;
		
		ListView beerListView = (ListView)view.findViewById(R.id.beerList);
		if(beerListView == null)
			return;
		
		if(!beer.hasBeenConsumed())
		{
			//The modified beer used to be pending and now isn't. Remove it from the adapter, and the listview will be notified to remove it from the UI.
			beers.remove(beer);
			((MyBeersArrayAdapter)beerListView.getAdapter()).remove(beer);
		}
		else
		{
			//Pending didn't change; it must have been a rating change. Loop through the visible rows in beerListView. If one of them is the modified beer, need to trigger it to re-draw
			int firstVisiblePosition = beerListView.getFirstVisiblePosition();
			for(int i = firstVisiblePosition, j = beerListView.getLastVisiblePosition(); i<=j; i++)
			{
				if(beer == beerListView.getItemAtPosition(i))	//Yea, pointer comparison. Since we pass it into the edit dialog, should be the same object.
				{
					Log.d(AppState.logTag, "Found modified beer at index " + i);
					beerListView.getAdapter().getView(i, beerListView.getChildAt(i - firstVisiblePosition), beerListView);
					break;
				}
			}
		}
		
		calculateStats();
	}

	@Override
	public boolean handleBackButton()
	{
		return false;
	}
}