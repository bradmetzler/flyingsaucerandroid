package com.mellowelephant.saucer.fragment.content;

import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;

import com.mellowelephant.saucer.R;
import com.mellowelephant.saucer.adapter.BeerCompareArrayAdapter;
import com.mellowelephant.saucer.callback.BeerBuddyListListener;
import com.mellowelephant.saucer.callback.BeerListListener;
import com.mellowelephant.saucer.callback.BeerModifiedListener;
import com.mellowelephant.saucer.data.AppData;
import com.mellowelephant.saucer.dto.BeerDTO;
import com.mellowelephant.saucer.dto.BuddyDTO;
import com.mellowelephant.saucer.dto.BuddyStatusDTO;
import com.mellowelephant.saucer.dto.SaucerLocationDTO;
import com.mellowelephant.saucer.location.LocationAware;
import com.mellowelephant.saucer.util.AppState;
import com.mellowelephant.saucer.util.BeerComparatorBuddyDateAscending;
import com.mellowelephant.saucer.util.BeerComparatorBuddyDateDescending;
import com.mellowelephant.saucer.util.BeerComparatorBuddyRatingDescending;
import com.mellowelephant.saucer.util.BeerComparatorNameAscending;
import com.mellowelephant.saucer.util.BuddyComparator;
import com.mellowelephant.saucer.util.BuddyUtil;
import com.mellowelephant.saucer.view.BeerLongPressDialog;
import com.mellowelephant.saucer.view.BuddyRatingDialog;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public class DrinkingBuddiesCompareFragment extends ContentFragment implements LocationAware, BeerModifiedListener
{
	private View view;
	private boolean isRunning;
	
	private boolean hideBeersIveHad;
	private boolean onlyShowAvailableBeers;
	
	private List<BuddyDTO> buddies;
	private List<BeerDTO> beers;
	
	private int orderByIndex = 1;	//Default to "Highest Rated First"
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		AppState.registerAsLocationAware("Content", this);
		AppState.contentFragment = this;
		view = inflater.inflate(R.layout.beercompare_layout, null);
		
		if(savedInstanceState != null)
			orderByIndex = savedInstanceState.getInt("orderByIndex", orderByIndex);
		
		Spinner orderOptionsSpinner = (Spinner)view.findViewById(R.id.myBeersOrderBySpinner);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, new String[]{"Alphabetical", "Highest Rated First", "Newest First", "Oldest First"});
		orderOptionsSpinner.setAdapter(adapter);
		orderOptionsSpinner.setOnItemSelectedListener(new OnItemSelectedListener()
		{
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int pos, long id)
			{
				if(orderByIndex != pos)
				{
					orderByIndex = pos;
					sortBeers();
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent)
			{
				onItemSelected(parent, null, 0, 0);
			}
		});
		orderOptionsSpinner.setSelection(orderByIndex);
		
		CheckBox onlyAvailableCheckbox = (CheckBox)view.findViewById(R.id.onlyAvailableCheckbox); 
		onlyAvailableCheckbox.setText(onlyAvailableCheckbox.getText() + " " + (AppState.currentLocation != null ? AppState.currentLocation.displayName : ""));
		onlyAvailableCheckbox.setOnCheckedChangeListener(new OnCheckedChangeListener()
		{
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
			{
				Log.w(AppState.logTag, "onCheckedChanged()");
				onlyShowAvailableBeers = isChecked;
				applyFilters();
			}
		});
		
		((CheckBox)view.findViewById(R.id.hideBeersCheckboxTwo)).setOnCheckedChangeListener(new OnCheckedChangeListener()
		{
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
			{
				Log.w(AppState.logTag, "onCheckedChanged()");
				hideBeersIveHad = isChecked;
				applyFilters();
			}
		});
		
		return view;
	}
	
	@Override
	public void onStart()
	{
		super.onStart();
		setHasOptionsMenu(true);
		
		isRunning = true;
		
		if(AppState.currentLocation == null)
			clearBeers();
		else
			fetchBuddies();
	}
	
	@Override
	public void onPause()
	{
		super.onPause();
		
		Log.w(AppState.logTag, "onPause");
		
		isRunning = false;
	}
	
	@Override
	public void onResume()
	{
		super.onResume();
		
		Log.w(AppState.logTag, "onResume");
		
		isRunning = true;
	}
	
	private void fetchBeers()
	{
		Log.d(AppState.logTag, "Asking for buddy beers...");
		
		AppData.retrieveBeerBuddyBeerList(new BeerListListener()
		{

			@Override
			public void onBeerListRetrieved(List<BeerDTO> beerList)
			{
				if(!isRunning)
				{
					Log.d(AppState.logTag, "Ignoring call to onBeerListRetrieved() because this fragment is no longer visible");
					return;
				}
				
				Log.d(AppState.logTag, "Retrieved " + beerList.size() + " buddy beers.");
				
				beers = beerList;
				populateData();
			}

			@Override
			public void onBeerListError()
			{
				if(!isRunning)
				{
					Log.d(AppState.logTag, "Ignoring call to onBeerListError() because this fragment is no longer visible");
					return;
				}
				
				beers = null;
				clearBeers();
				
				view.findViewById(R.id.beerList).setVisibility(View.GONE);
				view.findViewById(R.id.progressBar).setVisibility(View.GONE);
				
				TextView errorText = (TextView)view.findViewById(R.id.errorText);
				errorText.setText(getResources().getString(R.string.beerMenuNetworkError));
				errorText.setVisibility(View.VISIBLE);
			}

			@Override
			public void onInvalidCredentials()
			{
				if(!isRunning)
				{
					Log.d(AppState.logTag, "Ignoring call to onInvalidCredentials() because this fragment is no longer visible");
					return;
				}
				
				promptForNewCredentials();
			}
		});
	}
	
	private void fetchBuddies()
	{
		clearBeers();
		
		Log.d(AppState.logTag, "Asking for buddies...");
		AppData.retrieveBeerBuddyList(new BeerBuddyListListener()
		{
			@Override
			public void onBeerBuddyListRetrieved(List<BuddyDTO> buddyList)
			{
				if(!isRunning)
				{
					Log.d(AppState.logTag, "Ignoring call to onBeerBuddyListRetrieved() because this fragment is no longer visible");
					return;
				}
				
				Log.d(AppState.logTag, "Retrieved " + buddyList.size() + " buddies.");
				
				Iterator<BuddyDTO> buddyIter = buddyList.iterator();
				while(buddyIter.hasNext())
				{
					BuddyDTO buddy = buddyIter.next();
					if(buddy.getStatus() != BuddyStatusDTO.ACCEPTED)
						buddyIter.remove();
				}
				Collections.sort(buddyList, new BuddyComparator());
				
				buddies = buddyList;
				
				fetchBeers();
			}

			@Override
			public void onBeerBuddyListError()
			{
				if(!isRunning)
				{
					Log.d(AppState.logTag, "Ignoring call to onBeerBuddyListError() because this fragment is no longer visible");
					return;
				}
				
				buddies = null;
				clearBeers();
				
				view.findViewById(R.id.beerList).setVisibility(View.GONE);
				view.findViewById(R.id.progressBar).setVisibility(View.GONE);
				
				TextView errorText = (TextView)view.findViewById(R.id.errorText);
				errorText.setText(getResources().getString(R.string.beerMenuNetworkError));
				errorText.setVisibility(View.VISIBLE);
			}
			
			@Override
			public void onInvalidCredentials()
			{
				if(!isRunning)
				{
					Log.d(AppState.logTag, "Ignoring call to onInvalidCredentials() because this fragment is no longer visible");
					return;
				}
				
				promptForNewCredentials();
			}
		});
	}
	
	private void clearBeers()
	{
		buddies = null;
		beers = null;
		
		Log.d(AppState.logTag, "Clearing existing buddies/beers on the UI.");
		ListView beerListView = (ListView)view.findViewById(R.id.beerList);
		beerListView.setAdapter(null);
		beerListView.setVisibility(View.GONE);

		view.findViewById(R.id.beerCompareBuddyHeader).setVisibility(View.INVISIBLE);
		view.findViewById(R.id.errorText).setVisibility(View.GONE);
		view.findViewById(R.id.progressBar).setVisibility(View.VISIBLE);
	}
	
	@Override
	public void onDestroyView()
	{
		super.onDestroyView();
		AppState.unregisterAsLocationAware("Content");
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState)
	{
		super.onSaveInstanceState(outState);
		
		outState.putInt("orderByIndex", orderByIndex);
	}
	
	private void populateData()	//Called when beers are first retrieved
	{
		if(beers == null || beers.isEmpty())
		{
			view.findViewById(R.id.beerList).setVisibility(View.GONE);
			view.findViewById(R.id.progressBar).setVisibility(View.GONE);
			
			TextView errorText = (TextView)view.findViewById(R.id.errorText);
			errorText.setText(getResources().getString(R.string.myBeersNoBeers));
			errorText.setVisibility(View.VISIBLE);
			
			return;
		}
		
		buildBuddyHeader();
		sortBeers();
	}
	
	private void sortBeers()
	{
		if(beers == null || beers.isEmpty())
			return;
		
		Log.d(AppState.logTag, "Sorting beers[" + orderByIndex + "]...");
		
		Comparator<BeerDTO> comparator = null;
		
		switch(orderByIndex)
		{
			case 0:
				comparator = new BeerComparatorNameAscending();
				break;
			case 1:
				comparator = new BeerComparatorBuddyRatingDescending();
				break;
			case 2:
				comparator = new BeerComparatorBuddyDateDescending();
				break;
			case 3:
				comparator = new BeerComparatorBuddyDateAscending();
				break;
		}
		
		Collections.sort(beers, comparator);
		applyFilters();
	}
	
	private void applyFilters()
	{
		if(beers == null || beers.isEmpty())
			return;
		
		List<BeerDTO> filteredBeers = new ArrayList<BeerDTO>();
		
		for(BeerDTO beer : beers)
		{
			if(hideBeersIveHad && beer.hasBeenConsumed())
				continue;
			if(onlyShowAvailableBeers && !beer.isAtCurrentLocation())
				continue;
			
			filteredBeers.add(beer);
		}
		
		setBeers(filteredBeers);
	}
	
	private void setBeers(final List<BeerDTO> beers)
	{
		Log.d(AppState.logTag, "Setting " + buddies.size() + " buddies and " + beers.size() + " beers.");
		
		ListView beerListView = (ListView)view.findViewById(R.id.beerList);
		view.findViewById(R.id.progressBar).setVisibility(View.GONE);
		view.findViewById(R.id.errorText).setVisibility(View.GONE);
		beerListView.setVisibility(View.VISIBLE);
		
		BeerCompareArrayAdapter adapter = new BeerCompareArrayAdapter(getActivity(), beers, buddies);
		beerListView.setAdapter(adapter);
		beerListView.setOnItemClickListener(new OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int pos, long id)
			{
				Bundle arguments = new Bundle();
				arguments.putSerializable("beer", beers.get(pos));
				arguments.putSerializable("buddies", (ArrayList<BuddyDTO>)buddies);
				
				BuddyRatingDialog dialog = new BuddyRatingDialog();
				dialog.setTargetFragment(DrinkingBuddiesCompareFragment.this, 0);
				dialog.setArguments(arguments);
				dialog.show(getFragmentManager(), "BeerCompare");
			}
		});
		beerListView.setOnItemLongClickListener(new OnItemLongClickListener()
		{
			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view, int pos, long id)
			{
				Bundle arguments = new Bundle();
				arguments.putSerializable("beer", beers.get(pos));
				
				BeerLongPressDialog dialog = new BeerLongPressDialog();
				dialog.setTargetFragment(DrinkingBuddiesCompareFragment.this, 0);
				dialog.setArguments(arguments);
				dialog.show(getFragmentManager(), "BeerRemove");
				
				return true;
			}
		});
	}
	
	private void buildBuddyHeader()
	{
		LinearLayout buddyHeaderContainer = (LinearLayout)view.findViewById(R.id.beerCompareBuddyHeader);
		buddyHeaderContainer.removeAllViews();
		buddyHeaderContainer.setWeightSum(buddies.size() + 1);
		
		buddyHeaderContainer.addView(buildBuddyHeaderView("Me", getResources().getColor(R.color.lightgray)));
		for(BuddyDTO buddy : buddies)
			buddyHeaderContainer.addView(buildBuddyHeaderView(BuddyUtil.getFirstName(buddy), BuddyUtil.getColorSquareColor(buddy)));
		
		view.findViewById(R.id.beerCompareBuddyHeader).setVisibility(View.VISIBLE);
	}
	
	private View buildBuddyHeaderView(String name, int color)
	{
		TextView textView = new TextView(this.getActivity());
		textView.setLayoutParams(new TableLayout.LayoutParams(0, LayoutParams.MATCH_PARENT, 1));
		textView.setText(name);
		textView.setBackgroundColor(color);
		textView.setTextColor(0xFFFFFFFF);
		textView.setTypeface(null, Typeface.BOLD);
		textView.setGravity(Gravity.CENTER);

		return textView;
	}
	
	@Override
	public void onBeerModified(BeerDTO beer)	//Called when a beer is modified via a dialog (removed, added as pending, comment/rating changed). Need to re-draw the beer list.
	{
		if(!isRunning)
			return;
		
		ListView beerListView = (ListView)view.findViewById(R.id.beerList);
		if(beerListView == null)
			return;
		
		if(!beer.hasBeenConsumed() && (beer.getBuddies() == null || beer.getBuddies().isEmpty()))
		{
			//The modified beer used to be pending and now isn't. It also has no buddy ratings, so remove it from the adapter, and the listview will be notified to remove it from the UI.
			beers.remove(beer);
			((BeerCompareArrayAdapter)beerListView.getAdapter()).remove(beer);
		}
		else
		{
			//It must have been a rating change, or we removed the beer but it still has buddy ratings. Loop through the visible rows in beerListView. If one of them is the modified beer, need to trigger it to re-draw
			int firstVisiblePosition = beerListView.getFirstVisiblePosition();
			for(int i = firstVisiblePosition, j = beerListView.getLastVisiblePosition(); i<=j; i++)
			{
				if(beer == beerListView.getItemAtPosition(i))	//Yea, pointer comparison. Since we pass it into the edit dialog, should be the same object.
				{
					Log.d(AppState.logTag, "Found modified beer at index " + i);
					beerListView.getAdapter().getView(i, beerListView.getChildAt(i - firstVisiblePosition), beerListView);
					break;
				}
			}
		}
	}

	@Override
	public boolean handleBackButton()
	{
		AppState.contentFragment = new DrinkingBuddiesFragment();
		getFragmentManager().beginTransaction().replace(R.id.main_content_frame, AppState.contentFragment).commit();
		return true;
	}

	@Override
	public void onNewLocation(SaucerLocationDTO location, boolean wasSetAutomatically)
	{
		if(location == null)
			clearBeers();
		else
			fetchBuddies();
	}
}
