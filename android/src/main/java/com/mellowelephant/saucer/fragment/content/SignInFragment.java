package com.mellowelephant.saucer.fragment.content;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.mellowelephant.saucer.R;
import com.mellowelephant.saucer.callback.SignInListener;
import com.mellowelephant.saucer.data.AppData;
import com.mellowelephant.saucer.dto.SaucerLocationDTO;
import com.mellowelephant.saucer.dto.UserDTO;
import com.mellowelephant.saucer.util.AppState;
import com.mellowelephant.saucer.view.GenericSpinnerDialog;

public class SignInFragment extends ContentFragment
{
	private View view;
	
	//Form data
	private String ufoCardNumber = null;
	private String password = null;
	private SaucerLocationDTO location = null;
	private boolean isMou = false;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		view = inflater.inflate(R.layout.signin_layout, null);
		
		//UFO Card Number
		((EditText)view.findViewById(R.id.signInUfoNumber)).addTextChangedListener(new TextWatcher()
		{
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after){}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count){}

			@Override
			public void afterTextChanged(Editable s)
			{
				ufoCardNumber = s.toString();
				validateForm();
			}
		});
		
		//Password
		((EditText)view.findViewById(R.id.signInPassword)).addTextChangedListener(new TextWatcher()
		{
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after){}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count){}

			@Override
			public void afterTextChanged(Editable s)
			{
				password = s.toString();
				validateForm();
			}
		});
		
		//Location
		final Spinner optionsSpinner = (Spinner)view.findViewById(R.id.signInLocation);
		final SaucerLocationDTO[] locations = SaucerLocationDTO.values();
		String[] spinnerOptions = new String[locations.length + 1];
		
		spinnerOptions[0] = "-- Choose a location --";
		for(int i = 0; i < locations.length; i++)
			spinnerOptions[i + 1] = locations[i].displayName;
		
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, spinnerOptions);
		optionsSpinner.setAdapter(adapter);
		optionsSpinner.setOnItemSelectedListener(new OnItemSelectedListener()
		{
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int pos, long id)
			{
				if(pos > 0)
					location = locations[pos - 1];
				else
					location = null;
				
				validateForm();
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent)
			{
				location = null;
				validateForm();
			}
		});

		//MOU
		((TextView)view.findViewById(R.id.signInMouLabel)).setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				((CheckBox)view.findViewById(R.id.signInMou)).toggle();
			}
		});
		
		((CheckBox)view.findViewById(R.id.signInMou)).setOnCheckedChangeListener(new OnCheckedChangeListener()
		{
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
			{
				isMou = isChecked;
				validateForm();
			}
		});
		
		//Sign In Button
		((Button)view.findViewById(R.id.signInButton)).setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				final GenericSpinnerDialog signingInDialog = new GenericSpinnerDialog();
				Bundle args = new Bundle();
				args.putInt(GenericSpinnerDialog.ARG_STRINGID, R.string.signInSpinner);
				signingInDialog.setArguments(args);
				signingInDialog.show(getFragmentManager(), "signingInDialog");
				
				AppData.loginUser(ufoCardNumber, password, location, isMou, new SignInListener()
				{
					@Override
					public void onLoginSuccessful(UserDTO loggedInUser)
					{
						AppState.logUserIn(loggedInUser);
						
						if(signingInDialog != null)
							signingInDialog.dismiss();
					}

					@Override
					public void onLoginFailure()
					{
						if(signingInDialog != null)
							signingInDialog.dismiss();
						
						new LoginFailedDialog().show(getFragmentManager(), "signInFailedDialog");
					}
				});
			}
		});
		
		validateForm();
		
		return view;
	}
	
	private void validateForm()
	{
		boolean isValid = true;
		
		if(ufoCardNumber == null || ufoCardNumber.length() == 0)
			isValid = false;
		else if(password == null || password.length() == 0)
			isValid = false;
		else if(location == null)
			isValid = false;
		
		((Button)view.findViewById(R.id.signInButton)).setEnabled(isValid);
	}
	
	public static class LoginFailedDialog extends DialogFragment
	{
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState)
		{
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			builder.setTitle("Login Failed");
			builder.setNeutralButton("Close", new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(DialogInterface dialog, int which)
				{
					LoginFailedDialog.this.dismiss();
				}
			});
			
			builder.setMessage("Login failed. Please check your login information and try again.");
			
			return builder.create();
		}
	}

	@Override
	public boolean handleBackButton()
	{
		return false;
	}
}
