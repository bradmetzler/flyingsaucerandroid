package com.mellowelephant.saucer.fragment.menu;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mellowelephant.saucer.R;
import com.mellowelephant.saucer.activity.MainActivity;
import com.mellowelephant.saucer.data.AppData;
import com.mellowelephant.saucer.dto.SaucerLocationDTO;
import com.mellowelephant.saucer.location.LocationAware;
import com.mellowelephant.saucer.location.LocationPrompt;
import com.mellowelephant.saucer.util.AppState;
import com.mellowelephant.saucer.util.MenuOption;
import com.mellowelephant.saucer.view.MenuOptionView;

public class AppMenu extends Fragment implements LocationAware
{
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		AppState.menuFragment = this;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		Log.e(AppState.logTag, "onCreateView");
		return inflater.inflate(R.layout.main_menu, null);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
		super.onActivityCreated(savedInstanceState);
		AppState.registerAsLocationAware("Menu", this);	//Get a notification when the selected location has changed.
		
		LinearLayout locationSelector = (LinearLayout)getView().findViewById(R.id.location_selector);
		locationSelector.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				new LocationPrompt(getActivity()).show();
			}
		});
		
		populateMenuOptions();
		
		highlightMenuItem(AppState.selectedMenuOption);
		
		if(AppState.currentLocation != null)
			onNewLocation(AppState.currentLocation, AppData.isSavedLocationAutomatic());
	}
	
	public void highlightMenuItem(MenuOption menuOption)
	{
		LinearLayout menuButtons = (LinearLayout)getView().findViewById(R.id.main_menu_options);
		for(int i = 0; i < menuButtons.getChildCount(); i++)
		{
			if(menuButtons.getChildAt(i) instanceof MenuOptionView)
			{
				MenuOptionView view = (MenuOptionView)menuButtons.getChildAt(i);
				
				view.toggleHighlight(menuOption == view.getMenuOption());
			}
		}
	}
	
	public void onMenuItemSelected(MenuOption menuOption)
	{
		onMenuItemSelected(menuOption, false);
	}
	public void onMenuItemSelected(MenuOption menuOption, boolean refreshIfSelected)
	{
		if(getActivity() == null)
			return;
		
		highlightMenuItem(menuOption);
			
		if(getActivity() instanceof MainActivity)
			((MainActivity) getActivity()).switchContent(menuOption, refreshIfSelected);
		
		AppState.selectedMenuOption = menuOption;
	}
	
	public void populateMenuOptions()
	{
		LinearLayout menuButtons = (LinearLayout)getView().findViewById(R.id.main_menu_options);
		menuButtons.removeAllViews();
		
		for(final MenuOption menuOption : MenuOption.values())
		{
			if(!((AppState.isUserLoggedIn() && menuOption.visibleWhenLoggedIn) || (!AppState.isUserLoggedIn() && menuOption.visibleWhenNotLoggedIn)))
				continue;
			
			if(menuButtons.getChildCount() > 0)
				getLayoutInflater(null).inflate(R.layout.menu_divider, menuButtons);
			
			MenuOptionView view = new MenuOptionView(getActivity(), menuOption);
			view.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					AppState.goTo(menuOption);
					//onMenuItemSelected(menuOption);
				}
			});
			
			menuButtons.addView(view);
		}
	}

	@Override
	public void onNewLocation(SaucerLocationDTO location, boolean wasSetAutomatically)
	{
		TextView tv = (TextView)getView().findViewById(R.id.location_selector_curlocation);
		
		tv.setText(location == null ? getResources().getText(R.string.locationUnknown) : location.displayName);
		tv.setCompoundDrawablesWithIntrinsicBounds(wasSetAutomatically ? R.drawable.mylocation_small : 0, 0, 0, 0);
	}
}