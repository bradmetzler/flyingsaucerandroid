package com.mellowelephant.saucer.fragment.content;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.mellowelephant.saucer.R;
import com.mellowelephant.saucer.data.AppData;
import com.mellowelephant.saucer.util.AppState;

public class SettingsFragment extends ContentFragment
{
	private View view;
	
	private static final int busyTimerDelay = 1000;
	private static Handler busyTimer = null;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		view = inflater.inflate(R.layout.settings_layout, null);
		
		//Signed In as
		((TextView)view.findViewById(R.id.settingsSignOutName)).setText(getResources().getString(R.string.settingsSignedInAs) + " " + AppData.getLoggedInUser().getFullName());
		
		//Sign out
		((Button)view.findViewById(R.id.settingsSignOutButton)).setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				AppState.logUserOut(true);
			}
		});
		
		//Notifications
		final CheckBox notificationsCheckbox = ((CheckBox)view.findViewById(R.id.settingsPushNotificationsCheckbox));
		if(AppState.isGooglePlayAvailable(false))
		{
			notificationsCheckbox.setChecked(AppData.isNotificationsEnabled());
			notificationsCheckbox.setOnCheckedChangeListener(new OnCheckedChangeListener()
			{
				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
				{
					setBusyViewState(true, notificationsCheckbox, view.findViewById(R.id.settingsPushNotificationsProgressBar));
					notificationsCheckbox.setEnabled(false);
					AppData.setNotificationsEnabled(isChecked);
				}
			});
			
			if(AppData.isNotificationsBusy())
				setBusyViewState(true, notificationsCheckbox, view.findViewById(R.id.settingsPushNotificationsProgressBar));
		}
		else
		{
			notificationsCheckbox.setChecked(false);
			notificationsCheckbox.setEnabled(false);
			notificationsCheckbox.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					Toast.makeText(getActivity(), "Google Play Services unavailable!", Toast.LENGTH_LONG).show();
				}
			});
		}
		
		return view;
	}
	
	@SuppressLint("HandlerLeak")
	private void setBusyViewState(boolean isBusy, final CheckBox checkbox, final View progress)
	{
		checkbox.setEnabled(!isBusy);
		checkbox.setVisibility(isBusy ? View.INVISIBLE : View.VISIBLE);
		progress.setVisibility(isBusy ? View.VISIBLE : View.INVISIBLE);
		
		if(isBusy)
		{
			busyTimer = new Handler()
			{
				@Override
				public void handleMessage(Message msg)
				{
					if(AppData.isNotificationsBusy())
					{
						Log.d(AppState.logTag, "SettingsFragment: Tick");
						sendEmptyMessageDelayed(0, busyTimerDelay);
						return;
					}
					
					setBusyViewState(false, checkbox, progress);
					checkbox.setChecked(AppData.isNotificationsEnabled());
					AppData.isNotificationsEnabled();
				}
			};
			busyTimer.sendEmptyMessageDelayed(0, busyTimerDelay);
		}
		else if(busyTimer != null)
			busyTimer.removeMessages(0);
	}
	
	@Override
	public void onPause()
	{
		super.onPause();
		if(busyTimer != null)
			busyTimer.removeMessages(0);
	}

	@Override
	public boolean handleBackButton()
	{
		return false;
	}
}
