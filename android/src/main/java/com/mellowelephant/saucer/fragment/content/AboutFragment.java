package com.mellowelephant.saucer.fragment.content;


import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mellowelephant.saucer.R;
import com.mellowelephant.saucer.inappbilling.util.IabHelper;
import com.mellowelephant.saucer.inappbilling.util.IabResult;
import com.mellowelephant.saucer.inappbilling.util.Inventory;
import com.mellowelephant.saucer.inappbilling.util.Purchase;
import com.mellowelephant.saucer.inappbilling.util.PurchaseAware;
import com.mellowelephant.saucer.inappbilling.util.SkuDetails;
import com.mellowelephant.saucer.util.AppState;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AboutFragment extends ContentFragment implements PurchaseAware
{
    private View view;
    private IabHelper helper;
    private boolean iabSetup = false;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.about_layout, null);

        try
        {
            ((TextView) view.findViewById(R.id.aboutVersionText)).setText(getResources().getText(R.string.aboutVersion) + " " + getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0).versionName);
        }
        catch (Exception e)
        {
            view.findViewById(R.id.aboutVersionText).setVisibility(View.INVISIBLE);
        }

        WebView aboutUs = (WebView) view.findViewById(R.id.aboutUsView);
        aboutUs.loadData("<html><body><p style='font-size:small;' align='justify'>" + getResources().getText(R.string.aboutUs) + "</p></body></html>", "text/html", "utf-8");
        aboutUs.setBackgroundColor(0x00000000);

        AppState.contentFragment = this;
        return view;
    }

    @Override
    public void onStart()
    {
        super.onStart();

        setupDonateOptions();
    }

    @Override
    public void onDestroyView()
    {
        super.onDestroyView();

        if(helper != null && iabSetup)
            helper.dispose();
    }

	@Override
	public boolean handleBackButton()
	{
		return false;
	}

    private void setupDonateOptions()
    {
        if(helper != null && iabSetup)
            helper.dispose();

        if(AppState.hasDonated)
        {
            ((TextView)view.findViewById(R.id.aboutDonateTipText)).setText(getResources().getText(R.string.aboutDonateThanks));
            ((TextView)view.findViewById(R.id.aboutDonateInstructionsText)).setText(getResources().getText(R.string.aboutDonateAgainInstructions));
        }

        view.findViewById(R.id.skuList).setVisibility(View.GONE);
        view.findViewById(R.id.errorText).setVisibility(View.GONE);
        view.findViewById(R.id.progressBar).setVisibility(View.VISIBLE);

        Log.d(AppState.logTag, "Starting IAB");

        //Google recommends building this string on the fly for security
        final IabHelper helper = new IabHelper(getActivity().getApplicationContext(), AppState.billingKey1 + AppState.billingKey2 + AppState.billingKey3);
        this.helper = helper;
        helper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            @Override
            public void onIabSetupFinished(IabResult result) {
                Log.d(AppState.logTag, "Setup finished");

                if (!result.isSuccess()) {
                    Log.w(AppState.logTag, "IAB error! " + result);
                    showDonateOptionsError();
                    return;
                }

                iabSetup = true;
                Log.d(AppState.logTag, "Asking for sku details...");
                final List<String> skus = AppState.hasDonated ? Arrays.asList(AppState.sku_donate) : Arrays.asList(AppState.sku_unlock);
                helper.queryInventoryAsync(true, skus, new IabHelper.QueryInventoryFinishedListener()
                {
                    @Override
                    public void onQueryInventoryFinished(IabResult result, Inventory inventory)
                    {
                        if(result.isFailure())
                        {
                            Log.w(AppState.logTag, "Failed to retrieve sku details! " + result);
                            showDonateOptionsError();
                            return;
                        }

                        Log.d(AppState.logTag, "SKU details retrieved.");
                        List<SkuDetails> skuDetails = new ArrayList<SkuDetails>();
                        for(String sku : skus)
                        {
                            SkuDetails skuDetail = inventory.getSkuDetails(sku);
                            if(skuDetail != null)
                                skuDetails.add(skuDetail);
                        }

                        displayDonateOptions(skuDetails);
                    }
                });
            }
        });
    }

    private void displayDonateOptions(List<SkuDetails> skuDetails)
    {
        view.findViewById(R.id.errorText).setVisibility(View.GONE);
        view.findViewById(R.id.progressBar).setVisibility(View.GONE);

        LinearLayout skuList = (LinearLayout)view.findViewById(R.id.skuList);
        skuList.removeAllViews();
        for(final SkuDetails skuDetail : skuDetails)
        {
            View skuView = getLayoutInflater(null).inflate(R.layout.donateskulist_list_item, skuList, false);

            ((TextView)skuView.findViewById(R.id.donateSkuListItemDescription)).setText(skuDetail.getDescription());
            Button button = (Button)skuView.findViewById(R.id.donateSkuListItemButton);
            button.setText(skuDetail.getPrice());
            button.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    if(helper != null)
                    {
                        Log.w(AppState.logTag, "Launching purchase flow.");
                        helper.launchPurchaseFlow(getActivity(), skuDetail.getSku(), AppState.purchaseRequestCode, new IabHelper.OnIabPurchaseFinishedListener()
                        {
                            @Override
                            public void onIabPurchaseFinished(IabResult result, Purchase purchase)
                            {
                                if(result.isFailure())
                                {
                                    Log.w(AppState.logTag, "Failed to purchase SKU! " + result);
                                    return;
                                }

                                Log.d(AppState.logTag, "Purchase completed.");

                                if(Arrays.asList(AppState.sku_donate).contains(purchase.getSku()))
                                {
                                    Log.d(AppState.logTag, "Consuming donation.");
                                    helper.consumeAsync(purchase, null);
                                }

                                AppState.hasDonated = true;
                                AppState.refreshCurrentFragment();
                            }
                        });
                    }
                }
            });

            if(skuList.getChildCount() > 0)
                getLayoutInflater(null).inflate(R.layout.menu_divider, skuList);

            skuList.addView(skuView);
        }

        skuList.setVisibility(View.VISIBLE);
    }

    private void showDonateOptionsError()
    {
        view.findViewById(R.id.skuList).setVisibility(View.GONE);
        view.findViewById(R.id.progressBar).setVisibility(View.GONE);

        TextView errorView = (TextView)view.findViewById(R.id.errorText);
        errorView.setText(getResources().getText(R.string.aboutDonateOptionsError));
        errorView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onPurchaseResponse(int requestCode, int resultCode, Intent data)
    {
        Log.d(AppState.logTag, "onPurchaseResponse()");
        if(helper != null && iabSetup)
            helper.handleActivityResult(requestCode, resultCode, data);
    }
}
