package com.mellowelephant.saucer.fragment.content;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mellowelephant.saucer.R;

public class PlaceHolderFragment extends ContentFragment
{
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		return inflater.inflate(R.layout.placeholder_layout, null);
	}

	@Override
	public boolean handleBackButton()
	{
		return false;
	}
}
