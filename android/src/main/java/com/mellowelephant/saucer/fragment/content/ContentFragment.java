package com.mellowelephant.saucer.fragment.content;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.mellowelephant.saucer.util.AppState;

public abstract class ContentFragment extends Fragment
{
	public abstract boolean handleBackButton();
	
	protected void promptForNewCredentials()
	{
		Log.w(AppState.logTag, "Invalid credentials. Prompting to reauthenticate...");
		
		AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
		
		dialog.setTitle("Sign-in Failed");
		dialog.setMessage("Could not authenticate. Did your password change?");
		dialog.setPositiveButton("Sign In Again", new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				AppState.logUserOut(true);
			}
		});
		dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				AppState.logUserOut(false);
				AppState.refreshCurrentFragment();
			}
		});
		
		dialog.create().show();
	}
}
