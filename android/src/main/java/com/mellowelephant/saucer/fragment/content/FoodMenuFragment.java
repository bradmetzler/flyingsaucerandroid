package com.mellowelephant.saucer.fragment.content;


import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mellowelephant.saucer.R;
import com.mellowelephant.saucer.callback.FoodMenuListener;
import com.mellowelephant.saucer.data.AppData;
import com.mellowelephant.saucer.dto.SaucerLocationDTO;
import com.mellowelephant.saucer.location.LocationAware;
import com.mellowelephant.saucer.util.AppState;
import com.mellowelephant.saucer.view.TouchImageView;

public class FoodMenuFragment extends ContentFragment implements LocationAware
{
	private boolean isRunning;
	
	private View view;
	private Bitmap bitmap;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		AppState.registerAsLocationAware("Content", this);
		AppState.contentFragment = this;
		view = inflater.inflate(R.layout.foodmenu_layout, null);

		((TouchImageView)view.findViewById(R.id.foodMenuImageView)).setTouchListener(new TouchImageView.TouchListener()
		{
			@Override
			public void onTouch(Matrix matrix)
			{
				float[] matrixVals = new float[9];
				matrix.getValues(matrixVals);
				AppState.setMenuTouchMode(matrixVals[Matrix.MTRANS_X] >= 0);	//If we are scrolled all the way to the left of the image, sliding will open the menu. Otherwise, sliding will just slide the image.
			}
		});
		
		if(savedInstanceState != null && savedInstanceState.containsKey("foodMenuBitmap"))
		{
			Log.d(AppState.logTag, "Restoring bitmap from bundle.");
			setMenu((Bitmap)savedInstanceState.getParcelable("foodMenuBitmap"));
		}
		else
			fetchMenu();
		
		return view;
	}
	
	@Override
	public void onResume()
	{
		super.onStart();
		
		isRunning = true;
	}
	
	@Override
	public void onPause()
	{
		super.onPause();
		
		isRunning = false;
	}
	
	@Override
	public void onDestroyView()
	{
		super.onDestroyView();
		AppState.unregisterAsLocationAware("Content");
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState)
	{
		super.onSaveInstanceState(outState);
		
		if(this.bitmap != null)
			outState.putParcelable("foodMenuBitmap", bitmap);
	}

	private void fetchMenu()
	{
		clearMenu();
		
		Log.d(AppState.logTag, "Asking for menu...");
		AppData.retrieveFoodMenu(new FoodMenuListener()
		{
			@Override
			public void onFoodMenuRetrieved(Bitmap foodMenu)
			{
				if(!isRunning)
				{
					Log.d(AppState.logTag, "Ignoring call to onFoodMenuRetrieved() because this fragment is no longer visible");
					return;
				}
				
				setMenu(foodMenu);
			}

			@Override
			public void onFoodMenuError()
			{
				if(!isRunning)
				{
					Log.d(AppState.logTag, "Ignoring call to onFoodMenuError() because this fragment is no longer visible");
					return;
				}
				
				view.findViewById(R.id.foodMenuImageView).setVisibility(View.GONE);
				view.findViewById(R.id.progressBar).setVisibility(View.GONE);
				
				TextView errorText = (TextView)view.findViewById(R.id.errorText);
				errorText.setText(getResources().getString(R.string.foodMenuNetworkError));
				errorText.setVisibility(View.VISIBLE);
			}
		});
	}
	private void clearMenu()
	{
		this.bitmap = null;
		
		TouchImageView menuView = (TouchImageView)view.findViewById(R.id.foodMenuImageView);
		menuView.setImageBitmap(null);
		menuView.findViewById(R.id.foodMenuImageView).setVisibility(View.GONE);
		
		view.findViewById(R.id.progressBar).setVisibility(View.VISIBLE);
	}
	private void setMenu(Bitmap bitmap)
	{
		this.bitmap = bitmap;
		
		Log.d(AppState.logTag, "Setting the food menu bitmap to the view.");
		TouchImageView menuView = (TouchImageView)view.findViewById(R.id.foodMenuImageView);
		menuView.setImageBitmap(bitmap);
		
		view.findViewById(R.id.progressBar).setVisibility(View.GONE);
		menuView.setVisibility(View.VISIBLE);
	}
	
	@Override
	public void onNewLocation(SaucerLocationDTO location, boolean wasSetAutomatically)
	{
		Log.d(AppState.logTag, "onNewLocation: " + location);
		
		if(location == null)
			clearMenu();
		else
			fetchMenu();
	}

	@Override
	public boolean handleBackButton()
	{
		return false;
	}
}
