package com.mellowelephant.saucer.fragment.content;

import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.mellowelephant.saucer.R;
import com.mellowelephant.saucer.util.AppState;
import com.mellowelephant.saucer.util.BuddyUtil;
import com.mellowelephant.saucer.util.Formatter;

public class DrinkingBuddiesColorTestFragment extends ContentFragment
{
	private View view;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		AppState.contentFragment = this;
		view = inflater.inflate(R.layout.drinkingbuddies_layout, null);
		
		return view;
	}
	
	@Override
	public void onStart()
	{
		super.onStart();
		
		setupColorTest();
	}
	
	@Override
	public void onPause()
	{
		super.onPause();

		Log.w(AppState.logTag, "onPause");
	}
	
	@Override
	public void onResume()
	{
		super.onResume();
		
		Log.w(AppState.logTag, "onResume");
	}
	
	private void setupColorTest()
	{
		Log.d(AppState.logTag, "Setting up the color test");
		
		ListView buddyListView = (ListView)view.findViewById(R.id.drinkingBuddiesBuddyList);
		view.findViewById(R.id.progressBar).setVisibility(View.GONE);
		view.findViewById(R.id.errorText).setVisibility(View.GONE);
        view.findViewById(R.id.drinkingBuddiesSadPanda).setVisibility(View.GONE);
        view.findViewById(R.id.errorText).setVisibility(View.GONE);
        buddyListView.setVisibility(View.GONE);
        view.findViewById(R.id.drinkingBuddiesCompareBeersButton).setVisibility(View.GONE);

        LinearLayout ll = new LinearLayout(view.getContext());
        ll.setOrientation(LinearLayout.VERTICAL);

        for(int i = 0; i < BuddyUtil.colorSquareColors.length; i++)
        {
            int color = BuddyUtil.colorSquareColors[i];

            LinearLayout colorTestItem = new LinearLayout(view.getContext());
            colorTestItem.setOrientation(LinearLayout.HORIZONTAL);
            colorTestItem.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));

            TextView colorSquare = new TextView(view.getContext());
            if(Build.VERSION.SDK_INT >= 17)
                colorSquare.setTextAlignment(TextView.TEXT_ALIGNMENT_CENTER);
            colorSquare.setGravity(Gravity.CENTER);
            colorSquare.setTypeface(null, Typeface.BOLD);
            colorSquare.setTextColor(0xFFFFFFFF);
            colorSquare.setText("AZ");
            colorSquare.setBackgroundColor(color);

            TextView alphaSquare = new TextView(view.getContext());
            if(Build.VERSION.SDK_INT >= 17)
                alphaSquare.setTextAlignment(TextView.TEXT_ALIGNMENT_CENTER);
            alphaSquare.setGravity(Gravity.CENTER);
            alphaSquare.setTypeface(null, Typeface.BOLD);
            alphaSquare.setText("5");
            alphaSquare.setBackgroundColor(color);
            alphaSquare.setAlpha(0.25f);

            TextView fadedSquare = null;
            if(BuddyUtil.fadedSquareColors.length >= (i + 1))
            {
                fadedSquare = new TextView(view.getContext());
                if(Build.VERSION.SDK_INT >= 17)
                    fadedSquare.setTextAlignment(TextView.TEXT_ALIGNMENT_CENTER);
                fadedSquare.setGravity(Gravity.CENTER);
                fadedSquare.setTypeface(null, Typeface.BOLD);
                fadedSquare.setText("5");
                fadedSquare.setBackgroundColor(BuddyUtil.fadedSquareColors[i]);
            }

            colorTestItem.addView(colorSquare, Formatter.dpToPx(40), Formatter.dpToPx(40));
            colorTestItem.addView(alphaSquare, Formatter.dpToPx(40), Formatter.dpToPx(40));
            if(fadedSquare != null)
                colorTestItem.addView(fadedSquare, Formatter.dpToPx(40), Formatter.dpToPx(40));

            ll.addView(colorTestItem);
        }

        ((ViewGroup) view).addView(ll, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
	}

	@Override
	public boolean handleBackButton()
	{
		return false;
	}
}
